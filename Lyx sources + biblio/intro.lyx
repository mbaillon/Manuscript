#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass kaobook
\begin_preamble
\usepackage[backend=bibtex]{kaobiblio}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
% \usepackage[english]{babel}
\usepackage{bussproofs}
\usepackage{tikz-cd}
\usepackage{tipa}
\usepackage{url}
\usepackage{ stmaryrd }
\usepackage{multicol}

\setmathfont{DejaVu Math TeX Gyre}


%-- Configuration
% \newgeometry{vmargin={20mm}, hmargin={15 mm, 20 mm}}

%-- Custom environments

\newtheorem*{notation}{Notation}

%-- Macros

\newcommand{\MLTT}{\mathsf{MLTT}}
\newcommand{\BTT}{\mathsf{BTT}}
\newcommand{\CIC}{\mathsf{CIC}}
\newcommand{\CCO}{\mathsf{CC}_\omega}

\newcommand{\SysT}{\mathtt{T}}

\newcommand{\nat}{\mathbb{N}}
\newcommand{\bool}{\mathbb{B}}
\newcommand{\Type}{\square}
\newcommand{\TYPE}{\square\kern-0.65em\square}
\newcommand{\BTYPE}{\TYPE^b}
\newcommand{\ETYPE}{\TYPE^\varepsilon}

\newcommand{\cstr}[1]{\mathtt{#1}}

\newcommand{\Dial}{\mathfrak{D}}
\newcommand{\deval}{\partial}
\newcommand{\bind}{\mathtt{bind}}

\newcommand{\Pind}[1]{#1_\mathbf{ind}}
\newcommand{\Prect}[1]{#1_\mathbf{rec}}
\newcommand{\Pstr}[1]{#1_\mathbf{str}}
\newcommand{\Pcase}[1]{#1_\mathbf{cse}}
\definecolor{redinria}{RGB}{226,51,26}
\definecolor{blueinria}{RGB}{37,156,255}
\newcommand{\Task}{{\color{redinria}\mathbf{I}}}
\newcommand{\Tans}{{\color{blueinria}\mathbf{O}}}
\newcommand{\Toracle}{{\color{orange}\mathbf{Q}}}

\newcommand{\targetTT}{{\mathsf{T}}}

\newcommand{\Tlist}{\mathsf{list}}

\newcommand{\continuousS}{\mathscr{C}}
\newcommand{\continuous}[1]{\mathscr{C}\ #1}

\newcommand{\bAsk}[1]{\beta_{#1}}
\newcommand{\eAsk}[1]{\beta^\varepsilon_{#1}}

\newcommand{\dummy}{\omega}
\newcommand{\dummyT}{\mho}

\newcommand{\funext}{\mathsf{funext}}

% Axiom translation: 1st arg = oracle, 2nd arg = source term/typecontext
\newcommand*\atransterm[1]{[#1]_a}
\newcommand*\atranstype[1]{\llbracket {#1} \rrbracket_a}

% Branching translation: arg = source term/typecontext
\newcommand*\btransterm[1]{[#1]_b}
\newcommand*\btranstype[1]{\llbracket {#1} \rrbracket_b}

% Parametricity translation: 1st arg = oracle, 2nd arg = source term/typecontext
\newcommand*\etransterm[1]{[#1]_{\varepsilon}}
\newcommand*\etranstype[1]{\llbracket {#1} \rrbracket_{\varepsilon}}

% Global translation: 1st arg = oracle, 2nd arg = source term/typecontext
\newcommand*\gtransterm[2]{[#2]^{#1}_{g}}
\newcommand*\gtranstype[2]{\llbracket {#2} \rrbracket^{#1}_{g}}

\newcommand*\xtransterm[1]{[#1]}
\newcommand*\xtranstype[1]{\llbracket {#1} \rrbracket}

% Global translation: 1st arg = oracle, 2nd arg = source term/typecontext
\newcommand*\utransterm[1]{\langle#1\rangle}
\newcommand*\utranstype[1]{\langle {#1} \rangle}

\newcommand{\algparam}[1]{{#1}_\varepsilon}

\newcommand{\gentree}{\mathfrak{t}}
\newcommand{\genelt}{\gamma}


\let\footnote\marginnote
\let\footfullcite\sidecite

\def\th@plain{%
  \thm@notefont{\bfseries}% same as heading font
  \itshape % body font
}


\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\theoremstyle{plain}
\newtheorem{corollary}{Corollary}
\newtheorem{exemple}{Example}
\newtheorem{traduction}{Traduction}
\theoremstyle{remark}
\newtheorem*{remark}{Remark}


\AtBeginDocument{
\renewenvironment{figure}{\begin{marginfigure}}{\end{marginfigure}}
}
\end_preamble
\use_default_options true
\begin_modules
environments
\end_modules
\maintain_unincluded_children false
\language english
\language_package babel
\inputencoding auto
\fontencoding global
\font_roman "default" "KpRoman"
\font_sans "default" "DejaVu Sans"
\font_typewriter "default" "DejaVu Sans Mono"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts true
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command bibtex
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine biblatex
\cite_engine_type numerical
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Chapter*
Introduction
\end_layout

\begin_layout Standard
This thesis focuses on the interaction between dependent type theory and
 function continuity.
 Chapter 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:MLTT"
plural "false"
caps "false"
noprefix "false"

\end_inset

 describes the different flavours of type theory that we will encounter
 in this thesis.
 There will mainly be four of them:
\end_layout

\begin_layout Itemize

\emph on
System
\emph default
 
\begin_inset ERT
status open

\begin_layout Plain Layout

$
\backslash
SysT$
\end_layout

\end_inset

, a variant of simply-typed 
\begin_inset ERT
status open

\begin_layout Plain Layout

$
\backslash
lambda$
\end_layout

\end_inset

-calculus featuring natural numbers and a recursor operation, displayed
 in Section 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:System-T"
plural "false"
caps "false"
noprefix "false"

\end_inset

.
 It will be our running example in Chapter 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:gardening"
plural "false"
caps "false"
noprefix "false"

\end_inset

 and Chapter 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:digammaTT"
plural "false"
caps "false"
noprefix "false"

\end_inset

;
\end_layout

\begin_layout Itemize

\emph on
Martin-Löf Type Theory
\emph default
 (
\begin_inset ERT
status open

\begin_layout Plain Layout

$
\backslash
MLTT$
\end_layout

\end_inset

), a dependent type theory featuring dependent products, natural numbers
 and one universe, displayed in Section 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:MLTT"
plural "false"
caps "false"
noprefix "false"

\end_inset

.
 It will be our base theory for the proof of normalization we attempt in
 Chapter 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:digammaTT"
plural "false"
caps "false"
noprefix "false"

\end_inset

;
\end_layout

\begin_layout Itemize
the 
\emph on
Calculus of Inductive Constructions 
\emph default
(
\begin_inset ERT
status open

\begin_layout Plain Layout

$
\backslash
CIC$
\end_layout

\end_inset

), an extension of 
\begin_inset ERT
status open

\begin_layout Plain Layout

$
\backslash
MLTT$
\end_layout

\end_inset

 featuring inductive types and an infinite hierarchy of universes.
 It will be our meta-theory for the formalizations done iin Coq for this
 thesis, and is displayed in Section 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:CIC"
plural "false"
caps "false"
noprefix "false"

\end_inset

;
\end_layout

\begin_layout Itemize

\emph on
Baclofen Type Theory 
\emph default
(
\begin_inset ERT
status open

\begin_layout Plain Layout

$
\backslash
BTT$
\end_layout

\end_inset

), a variant of 
\begin_inset ERT
status open

\begin_layout Plain Layout

$
\backslash
CIC$
\end_layout

\end_inset

 where dependent elimination is weakened to accomodate effects, which will
 prove useful in Chapter 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:gardening"
plural "false"
caps "false"
noprefix "false"

\end_inset

.
 It is displayed in Section 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:BTT"
plural "false"
caps "false"
noprefix "false"

\end_inset

.
\end_layout

\begin_layout Standard
The end of Chapter 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:MLTT"
plural "false"
caps "false"
noprefix "false"

\end_inset

 will be devoted to
\emph on
 program translations 
\emph default
as a particular way of building models of type theories, a tool we will
 use in Chapter 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:gardening"
plural "false"
caps "false"
noprefix "false"

\end_inset

.
\end_layout

\begin_layout Standard
Chapter 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:continuity"
plural "false"
caps "false"
noprefix "false"

\end_inset

 focuses on dialogue trees as an inductive representation of 
\emph on
continuous functionals
\emph default
.
 This way of envisioning continuity is one among many others, hence we survey
 the different definitions of continuity that exist in the literature, and
 describe their interactions.
 In particular, we highlight the closeness between the dialogue monad and
 sheafification.
\end_layout

\begin_layout Standard
Chapter 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:gardening"
plural "false"
caps "false"
noprefix "false"

\end_inset

 displays a first attempt at mingling continuity with dependent type theory.
 In this Chapter, we extend a proof by 
\emph on
Escardó 
\emph default

\begin_inset CommandInset citation
LatexCommand footfullcite
key "escardo1"
literal "false"

\end_inset

 that every functional definable in System 
\begin_inset ERT
status open

\begin_layout Plain Layout

$
\backslash
SysT$
\end_layout

\end_inset

 is continuous.
 As the proof is fairly technical, we first go through
\emph on
 Escardó
\emph default
's proof in Section 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Escardó's-model"
plural "false"
caps "false"
noprefix "false"

\end_inset

, albeit rephrasing it as a program translation.
 We finally enhance the proof to 
\begin_inset ERT
status open

\begin_layout Plain Layout

$
\backslash
BTT$
\end_layout

\end_inset

 in Section 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:syntactic-model"
plural "false"
caps "false"
noprefix "false"

\end_inset

.
\end_layout

\begin_layout Standard
Unfortunately, this proof technique does not scale well to 
\begin_inset ERT
status open

\begin_layout Plain Layout

$
\backslash
MLTT$
\end_layout

\end_inset

, so we change tack to try and gain more control over computation by building
 a 
\emph on
normalization model 
\emph default
for an extension of 
\begin_inset ERT
status open

\begin_layout Plain Layout

$
\backslash
MLTT$
\end_layout

\end_inset

.
 This extension, dubbed 
\begin_inset ERT
status open

\begin_layout Plain Layout

$
\backslash
DIGTT$
\end_layout

\end_inset

, features a specific function 
\begin_inset ERT
status open

\begin_layout Plain Layout

$
\backslash
oracle : 
\backslash
nat 
\backslash
to 
\backslash
bool$
\end_layout

\end_inset

 whose calls are recorded in forcing conditions.
 The idea is, to recover a modified 
\emph on
canonicity
\emph default
 theorem for 
\begin_inset ERT
status open

\begin_layout Plain Layout

$
\backslash
DIGTT$
\end_layout

\end_inset

, essentially stating that every term 
\begin_inset ERT
status open

\begin_layout Plain Layout

$t : 
\backslash
nat$
\end_layout

\end_inset

 potentially mentioning 
\begin_inset ERT
status open

\begin_layout Plain Layout

$
\backslash
oracle$
\end_layout

\end_inset

 is continuous.
 This proof is however not fully formalized, and we hope to provide a certified
 proof in the coming months.
 We discus this endeavour in Chapter 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:digammaTT"
plural "false"
caps "false"
noprefix "false"

\end_inset

.
\end_layout

\end_body
\end_document
