#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass kaobook
\begin_preamble
\usepackage[backend=bibtex]{kaobiblio}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
% \usepackage[english]{babel}
\usepackage{bussproofs}
\usepackage{tikz-cd}
\usepackage{tipa}
\usepackage{url}
\usepackage{ stmaryrd }
\usepackage{multicol}

\setmathfont{DejaVu Math TeX Gyre}


%-- Configuration
% \newgeometry{vmargin={20mm}, hmargin={15 mm, 20 mm}}

%-- Custom environments

\newtheorem*{notation}{Notation}

%-- Macros

\newcommand{\MLTT}{\mathsf{MLTT}}
\newcommand{\BTT}{\mathsf{BTT}}
\newcommand{\CIC}{\mathsf{CIC}}
\newcommand{\CCO}{\mathsf{CC}_\omega}

\newcommand{\SysT}{\mathtt{T}}

\newcommand{\nat}{\mathbb{N}}
\newcommand{\bool}{\mathbb{B}}
\newcommand{\Type}{\square}
\newcommand{\TYPE}{\square\kern-0.65em\square}
\newcommand{\BTYPE}{\TYPE^b}
\newcommand{\ETYPE}{\TYPE^\varepsilon}

\newcommand{\cstr}[1]{\mathtt{#1}}

\newcommand{\Dial}{\mathfrak{D}}
\newcommand{\deval}{\partial}
\newcommand{\bind}{\mathtt{bind}}

\newcommand{\Pind}[1]{#1_\mathbf{ind}}
\newcommand{\Prect}[1]{#1_\mathbf{rec}}
\newcommand{\Pstr}[1]{#1_\mathbf{str}}
\newcommand{\Pcase}[1]{#1_\mathbf{cse}}
\definecolor{redinria}{RGB}{226,51,26}
\definecolor{blueinria}{RGB}{37,156,255}
\newcommand{\Task}{{\color{redinria}\mathbf{I}}}
\newcommand{\Tans}{{\color{blueinria}\mathbf{O}}}
\newcommand{\Toracle}{{\color{orange}\mathbf{Q}}}

\newcommand{\targetTT}{{\mathsf{T}}}

\newcommand{\Tlist}{\mathsf{list}}

\newcommand{\continuousS}{\mathscr{C}}
\newcommand{\continuous}[1]{\mathscr{C}\ #1}

\newcommand{\bAsk}[1]{\beta_{#1}}
\newcommand{\eAsk}[1]{\beta^\varepsilon_{#1}}

\newcommand{\dummy}{\omega}
\newcommand{\dummyT}{\mho}

\newcommand{\funext}{\mathsf{funext}}

% Axiom translation: 1st arg = oracle, 2nd arg = source term/typecontext
\newcommand*\atransterm[1]{[#1]_a}
\newcommand*\atranstype[1]{\llbracket {#1} \rrbracket_a}

% Branching translation: arg = source term/typecontext
\newcommand*\btransterm[1]{[#1]_b}
\newcommand*\btranstype[1]{\llbracket {#1} \rrbracket_b}

% Parametricity translation: 1st arg = oracle, 2nd arg = source term/typecontext
\newcommand*\etransterm[1]{[#1]_{\varepsilon}}
\newcommand*\etranstype[1]{\llbracket {#1} \rrbracket_{\varepsilon}}

% Global translation: 1st arg = oracle, 2nd arg = source term/typecontext
\newcommand*\gtransterm[2]{[#2]^{#1}_{g}}
\newcommand*\gtranstype[2]{\llbracket {#2} \rrbracket^{#1}_{g}}

\newcommand*\xtransterm[1]{[#1]}
\newcommand*\xtranstype[1]{\llbracket {#1} \rrbracket}

% Global translation: 1st arg = oracle, 2nd arg = source term/typecontext
\newcommand*\utransterm[1]{\langle#1\rangle}
\newcommand*\utranstype[1]{\langle {#1} \rangle}

\newcommand{\algparam}[1]{{#1}_\varepsilon}

\newcommand{\gentree}{\mathfrak{t}}
\newcommand{\genelt}{\gamma}


\let\footnote\marginnote
\let\footfullcite\sidecite

\def\th@plain{%
  \thm@notefont{\bfseries}% same as heading font
  \itshape % body font
}


\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\theoremstyle{plain}
\newtheorem{corollary}{Corollary}
\newtheorem{exemple}{Example}
\newtheorem{traduction}{Traduction}
\theoremstyle{remark}
\newtheorem*{remark}{Remark}


\AtBeginDocument{
\renewenvironment{figure}{\begin{marginfigure}}{\end{marginfigure}}
}
\end_preamble
\use_default_options true
\begin_modules
environments
\end_modules
\maintain_unincluded_children false
\language english
\language_package babel
\inputencoding auto
\fontencoding global
\font_roman "default" "KpRoman"
\font_sans "default" "DejaVu Sans"
\font_typewriter "default" "DejaVu Sans Mono"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts true
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command bibtex
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine biblatex
\cite_engine_type numerical
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
pagelayout{wide} % No margins
\end_layout

\end_inset


\end_layout

\begin_layout Chapter*
Remerciements
\end_layout

\begin_layout Standard
Proximité géographique faisant foi, je tiens tout d'abord à remercier les
 différentes personnes à avoir partagé mon bureau au cours de ces trois
 années de thèse.
 Au commencement était Meven, solide titulaire de la super-chaise-près-de-la-fen
être, que je me suis empressé de récupérer à son départ.
 Il n'était pas encore devenu le franc colocataire qu'il fut ensuite, mais
 il montrait déjà ses caractéristiques les plus évidentes : un grand sourire,
 une maigreur aussi inquiétante que les slides de Mekaouche, un rire reconnaissa
ble entre tous, un amour incompréhensible pour le typage bidirectionnel
 et un certain manque de suite dans les idées quand il s'agit de faire des
 reprises d'Iron Maiden.
 Merci à toi, 
\emph on
senpai numéro 1.
 
\end_layout

\begin_layout Standard
Merci aussi à Pierre, cothésard éclectique, toujours prêt à proposer de
 nouveaux modèles de calcul Turing-complet, à improviser un jazz au Mélodica
 ou à venir assister à un cours de théâtre.
 J'espère que tu trouveras un lieu où abriter l'incroyable énergie d'apprendre
 et d'enseigner dont tu resplendis.
 Merci à Nils pour son apprentissage parcellaire du français, toi dont les
 
\begin_inset Quotes eld
\end_inset

Bien ou bien ?
\begin_inset Quotes erd
\end_inset

 retentissent encore dans les murs de ce bâtiment ; merci à Yann-le-mangeur-lent
, à Josselin-le-catégoricien-shiny et à Sidney-le-deuxième-mangeur-lent
 pour les discussions sur des vrais langages de programmation concrets de
 la vraie vie véritable.
 La super-chaise-près-de-la-fenêtre sera bientôt libre, ne vous battez pas.
 Merci à Loïc enfin, 
\emph on
senpai numéro 2,
\emph default
 prophète de l'égalité observationnelle, toi plus pâle que la neige et plus
 solide que cette métaphore, acteur d'une journée héroïque passée à écrire
 sous forme de vagues et contempler des canards.
 Clairement la meilleure Ascension possible.
 Hâte de venir te voir dans ton igloo nordique un de ces jours.
 
\end_layout

\begin_layout Standard
Merci aussi à tous les stagiaires et doctorants en visite venus animer ce
 bureau, Arthur, Peio, Thomas, Robin, Tomas, Tomas (le deuxième surtout).
 Merci à Théo de nous avoir laissé te pousser en haut de la colline d'Édimbourg,
 le paysage était sublime et on s'est bien amusé.
\end_layout

\begin_layout Standard
Dans l'enfilade de bureaux adjacents du bâtiment 11, j'appelle Enzo à la
 barre.
 Merci à toi, chair de ma chair, frère de thèse, indépendantiste breton
 diplômé avant moi à la photo finish.
 Ton apparition, café à la main, dans l'encadrement de la porte pour parler
 de tes derniers algorithmes paramétriques était toujours une éclaircie
 dans la journée la plus sombre, et notre périple écossais, en randonnée
 dans les Highlands, debouts dans un train surchauffé ou à te nourrir à
 la becquée covidé dans ta chambre, reste un des meilleurs souvenirs de
 2022.
 Toi aussi tu pars t'enterrer dans le froid et la neige, drôle d'idée à
 mon avis mais qui suis-je pour critiquer les choix d'un docteur ? Merci
 à toi, Hamza, grand connaisseur de musique classique et détenteur d'un
 tapis du plus bel effet ; j'avoue que je n'ai jamais compris ton sujet
 de thèse, merci d'avoir été moins nul et de t'être intéressé au mien.
 Merci à Nicolas, figure tutélaire, chef d'équipe en marcel et tongs, d'une
 disponibilité à toute épreuve, merci à Kazuhiko pour m'avoir montré comment
 faire du vrai café, à Kenji-le-brûleur-de-blattes pour les discussions
 autour d'extensions de Kan dans une direction quelconque et de splits dans
 des théories 
\begin_inset Quotes eld
\end_inset

à poils bleus
\begin_inset Quotes erd
\end_inset

, à Mathieu pour les éclairages mathématico-mathématiques sur la continuité,
 merci à Yannick pour m'inculquer un peu de rigueur dans ma rédaction de
 thèse et pour demander 
\emph on
when is the party? 
\emph default
à la fin du processus, merci à Koen, Yee Jian et Nicolas.
 Merci à Xavier désormais sans bureau pour ton point de vue intéressant
 sur les notations et les débats que tu suscites de temps en temps sur le
 canal doctorants.
\end_layout

\begin_layout Standard
Enfin, dernier cité du bâtiment 34 mais pas le moindre, merci Pierre-Marie
 pour avoir été Pierre-Marie du début à la fin, avec tes imitations d'accents
 indiens aux subtilités inaudibles pour nos oreilles d'européens sourdingues,
 ta théière à piston qui se décompose jour après jour et tes statégies politique
s inaccessibles au tout-venant.
 Merci d'avoir été un mauvais encadrant de thèse qui m'a laissé écrire les
 pires jeux de mots dans mon manuscrit, pour ton amour du Sur Mesure et
 ta chartreuse toujours disponible en séminaire au vert, pour tes histoires
 de voiture perdue en Espagne les répliques de la 
\emph on
Classe Américaine 
\emph default
que tu cries dans les couloirs et les menaces de mort sur ma tête que tu
 échanges avec Andrej.
 Merci pour les mouvements de doigts que tu fais lorsque tu parles des catégorie
s ; rarement les mathématiques auront été aussi proches de la pantomime.
 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
bigskip
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Par la fenêtre, j'aperçois de l'autre côté de la passerelle le bureau d'Assia,
 Guilhem et Guillaume.
 Merci Guillaume pour tes remarques caustiques, pour toutes les fois où
 tu m'as caché ma sacoche de vélo et pour m'avoir montré que certaines personnes
 faisaient n'importe quoi avec OCaml, merci Guilhem pour ta bonne humeur
 et ton courage lorsqu'il a fallu monter au front contre Mekaouche-le-terrible.
 Merci à Assia pour m'avoir donné un sujet de stage de théorie des types
 alors que je t'avais envoyé un mail à propos d'induction sur les réels,
 merci d'avoir permis qu'une thèse commencée en plein confinement se passe
 aussi bien.
 Merci d'avoir répondu à mes questions, d'avoir posé à Pierre-Marie les
 questions nécessaires pour débroussailler sa parole touffue, de m'avoir
 abrité des tempêtes administratives, de m'avoir dit de rédiger quand il
 fallait rédiger et d'arrêter de rédiger quand il fallait arrêter de rédiger,
 merci d'avoir réservé les salles sur GRR et de m'avoir proposé de rester
 comme ingénieur de recherche après ma thèse.
 Merci de ne pas avoir compté tes heures lorsque tu devais gérer à la fois
 ta recherche, des enfants qui s'ingénient à tomber malade au pire moment
 et deux thésards au bord du précipice.
 Visiblement je ne suis pas le premier doctorant que tu encadres qui parte
 faire des bêtises artistiques une fois diplômé ; crois bien que ce n'est
 pas parce que tu nous fait peur.
 
\end_layout

\begin_layout Standard
Toujours au sein du bâtiment 11, merci à Gaëtan pour tes éclairages sur
 le fonctionnement de Coq, merci à Matthieu - le seul vrai rockeur de Gallinette
 - pour les parties de flipper endiablées, les cours de Forró, pour avoir
 nommé ta fille Cléopâtre et pour ta gentillesse, tout simplement.
 Merci à Anne-Claire pour les multiples remboursements de frais de mission
 qui m'ont rendu riche aux dépens du contribuable, merci à Virginie pour
 ses éclairages administratifs sur le fonctionnement complexe de l'université.
\end_layout

\begin_layout Standard
Quittant le labo par le nord, on atteint rapidement le gymnase de la Barboire,
 qui abrite l'une des équipes de volley les plus admirables que j'aie rencontrée
s.
 Merci à Alice et Alex les capicoachs, à Tom qui smashe super bien, Dimitri
 le réparateur de grues, Arnaud le protecteur de biodiversité, Orama l'archéolog
ue, Corentin le sudiste, Monika et Bastien pour avoir formé une équipe de
 RAV4 d'exception.
 Et bien sûr, merci à Audrey la boîte aux lettres, Enora la puncheuse, Ilaria
 
\emph on
vai vai vai
\emph default
, Simon le bourrin, Marine la gagneuse, Paul le contreur fou, Laure l'entorse
 aux règles et Tom le footeux pour avoir repris le flambeau et être devenus
 une si belle équipe de raviolis.
\end_layout

\begin_layout Standard
Merci aux participants de mes autres activités que je ne pourrai bientôt
 plus appeler 
\begin_inset Quotes eld
\end_inset

extra-scolaires
\begin_inset Quotes erd
\end_inset

, Nina, Mathieu, Lucie et les autres d'avoir joué Shakespeare avec moi,
 et merci à Marie, Claire, Carine, Aurore, Cyrille, Tom et Jade pour ces
 soirées danse endiablées.
 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
bigskip
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Ce tour rétrospectif de la ville de Nantes ne serait pas complet sans un
 immense merci aux colocataires qui se sont succédés au sein de La Boulange.
 Merci à Meven, à nouveau, membre fondateur de cette colocation, merci à
 Joanne pour ton intégrité, ta gentillesse, les footings-crêpes-nems du
 jeudi matin et les longues discussions sur le monde merveilleux de la fantasy.
 Ce chapitre 1 est pour toi, bien sûr, tu sauras l'apprécier.
 Merci à Léo pour avoir été le coloc le plus constant, mi-hobbit mi-gobelin,
 pour m'avoir indiqué mille bons plans nantais et m'avoir permis de rencontrer
 moult personnes fascinantes.
 J'espère que la vie en éco-lieu se passera bien.
 Merci à Léa, propriétaire mais toujours révolutionnaire, de te battre pour
 rallumer les étoiles, merci à Élisa d'avoir monté 
\emph on
Les couilles sur la table
\emph default
 au fond de la grotte du Fluffystan, merci à Emma d'avoir apporté sa bonne
 humeur depuis Marseille, à Natan d'avoir tenté de me vendre le SNU et à
 Elsa de t'accrocher bon an mal an à ce service civique aux airs de torture.
 Merci à Lara d'avoir peint le mur en bleu, de m'avoir initié à la course
 et aux gâteaux citron-basilic, d'avoir joué au foot comme on conduit un
 TGV et d'avoir une petite soeur encore plus cool que toi ; merci, enfin,
 pour ces quelques mois passés ensemble et hâte de venir te voir à Dresde.
 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
bigskip
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Nantes n'étant, finalement, qu'une banlieue un peu éloignée de Paris, il
 est temps d'aborder une nouvelle salve de remerciements à tous les parisiens
 que j'ai pu côtoyer durant cette thèse.
 En commençant par les cinéastes, merci d’abord à Pierre, fidèle compagnon
 de route artistique, homme aux multiples talents, auteur, réalisateur,
 acteur, chanteur, guitariste et même développeur web à ses heures perdues.
 Merci de m’avoir attendu ces trois ans, ça y est c’est fini, promis, on
 va pouvoir faire des films.
 Merci à Claire le koala philosophe pour ta production industrielle de mèmes
 qui m’a maintenu à flot lors de la rédaction, pour avoir essayé de lire
 ma thèse et m’avoir fait bénéficier de ton vaste empire immobilier.
 Merci pour avoir cité régulièrement Skoll, le Lillet Tonic et Paul Schräder,
 ta sainte trinité à toi.
 Merci surtout pour avoir été et être toujours la personne la plus stylée
 de la vie en jaune.
 Merci à Clélia, enfin, pour apporter un peu de vie dans ce groupe de penseurs
 dépressifs et pour ne pas m’avoir tué à de multiples reprises lorsque tu
 en as eu l’occasion.
 Merci à Paco d’avoir filmé quand on lui demandait de filmer, à Roman d’avoir
 été notre sherpa tout autant que notre script doctor, à Marie d’être la
 meilleure personne sur terre et à toute l’équipe de L’entorse pour nous
 avoir permis de faire ce film qui nous ressemble.
\end_layout

\begin_layout Standard
Merci à Cécile et Guillaume pour tous les repas dans votre appartement,
 pour le confinement dans un chalet montagnard et pour avoir prévu votre
 mariage après ma soutenance, c’est quand même plus simple pour s’organiser.
 Merci à Grégoire et Nico pour les randos-vélos, à Antoine et Guillaume
 pour m’avoir donné une raison de revenir à Massy-Palaiseau, et plus généralemen
t à toute la section bad de rester ce groupe d’amis sur lesquels compter.
 Merci à FMF, Maverick et toute la lignée pour les Xiao Long Bao et merci
 à Charles de filer du boulot à Persistance sous le SMIC horaire.
 Tu es le destinataire du plus long poème que j’ai écrit, et tu en mérites
 chaque ligne.
 Merci à Lucas de rester la personne la plus musclée que je connaisse, à
 Armand d’être un frère d’armes inamovible, à Louis de nous rappeler chaque
 jour l’absence d’analyse modale chez les ingénieurs italiens, à Meryem
 d’être la meilleure respo archives que le monde ait connu et à tous les
 membres du JTX d’avoir fait des vidéos de qualité discutable mais toujours
 avec enthousiasme.
 Merci à Sylvestre pour ton amour inconditionnel du badminton, pour les
 tournois partout en France et les conseils que tu m’as prodigués.
 Hâte de te voir à Berlin ! Merci aussi à tout le reste de l’EBPS12, Florian,
 Théo, Thomas, Camille et tous les autres de partager votre passion avec
 bonne humeur.
 
\end_layout

\begin_layout Standard
Merci à tous les participants des différents confinements, qu’ils soient
 dans les Alpes ou en Charentes-Maritimes, pour avoir fait de ces mois de
 restrictions des moments hors du temps, à mi-chemin du travail et des vacances.
 
\end_layout

\begin_layout Standard
Merci à Antonin d’avoir fait ma thèse à ma place sans que personne ne s’en
 rende compte, et pour l’achat prochain d’une Switch.
 
\end_layout

\begin_layout Standard
Merci à Sylvie, Violette et Maud pour votre enthousiasme à la vue de la
 moindre brocante et pour avoir tenté de me donner du style.
\end_layout

\begin_layout Standard
Merci à mes parents de m’avoir toujours soutenu, à mon frère de m’encourager
 à attaquer en justice tout ce qui bouge, et à mes grands-parents qui sont,
 ou seraient, j’espère, fiers de moi.
 
\end_layout

\begin_layout Standard
Merci aux Halogen Mushrooms, aux Brews Brothers et à tous les autres.
\end_layout

\begin_layout Standard
Merci à tous les professeurs qui m’ont transmis leur amour du savoir au
 cours de mes années d’études, Mme Giboraud, M.
 Deseez, Mme Plassard, M.
 Combault, Mme Kolago, M.
 Real, M.
 Taïeb, M.
 Bournez et tous les autres.
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
bigskip
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Enfin, je n’oublie pas qu’en tant qu'homme blanc de famille aisée, parisien,
 cisgenre et hétérosexeuel, j'étais dans une position privilégiée pour faire
 cette thèse.
 Espérons qu’un jour il en soit autrement.
 
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
pagelayout{margin}
\end_layout

\end_inset


\end_layout

\end_body
\end_document
