#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass kaobook
\begin_preamble
\usepackage[backend=bibtex]{kaobiblio}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
% \usepackage[english]{babel}
\usepackage{bussproofs}
\usepackage{tikz-cd}
\usepackage{tipa}
\usepackage{url}
\usepackage{ stmaryrd }
\usepackage{multicol}

\setmathfont{DejaVu Math TeX Gyre}


%-- Configuration
% \newgeometry{vmargin={20mm}, hmargin={15 mm, 20 mm}}

%-- Custom environments

\newtheorem*{notation}{Notation}

%-- Macros

\newcommand{\MLTT}{\mathsf{MLTT}}
\newcommand{\BTT}{\mathsf{BTT}}
\newcommand{\CIC}{\mathsf{CIC}}
\newcommand{\CCO}{\mathsf{CC}_\omega}

\newcommand{\SysT}{\mathtt{T}}

\newcommand{\nat}{\mathbb{N}}
\newcommand{\bool}{\mathbb{B}}
\newcommand{\Type}{\square}
\newcommand{\TYPE}{\square\kern-0.65em\square}
\newcommand{\BTYPE}{\TYPE^b}
\newcommand{\ETYPE}{\TYPE^\varepsilon}

\newcommand{\cstr}[1]{\mathtt{#1}}

\newcommand{\Dial}{\mathfrak{D}}
\newcommand{\deval}{\partial}
\newcommand{\bind}{\mathtt{bind}}

\newcommand{\Pind}[1]{#1_\mathbf{ind}}
\newcommand{\Prect}[1]{#1_\mathbf{rec}}
\newcommand{\Pstr}[1]{#1_\mathbf{str}}
\newcommand{\Pcase}[1]{#1_\mathbf{cse}}
\definecolor{redinria}{RGB}{226,51,26}
\definecolor{blueinria}{RGB}{37,156,255}
\newcommand{\Task}{{\color{redinria}\mathbf{I}}}
\newcommand{\Tans}{{\color{blueinria}\mathbf{O}}}
\newcommand{\Toracle}{{\color{orange}\mathbf{Q}}}

\newcommand{\targetTT}{{\mathsf{T}}}

\newcommand{\Tlist}{\mathsf{list}}

\newcommand{\continuousS}{\mathscr{C}}
\newcommand{\continuous}[1]{\mathscr{C}\ #1}

\newcommand{\bAsk}[1]{\beta_{#1}}
\newcommand{\eAsk}[1]{\beta^\varepsilon_{#1}}

\newcommand{\dummy}{\omega}
\newcommand{\dummyT}{\mho}

\newcommand{\funext}{\mathsf{funext}}

% Axiom translation: 1st arg = oracle, 2nd arg = source term/typecontext
\newcommand*\atransterm[1]{[#1]_a}
\newcommand*\atranstype[1]{\llbracket {#1} \rrbracket_a}

% Branching translation: arg = source term/typecontext
\newcommand*\btransterm[1]{[#1]_b}
\newcommand*\btranstype[1]{\llbracket {#1} \rrbracket_b}

% Parametricity translation: 1st arg = oracle, 2nd arg = source term/typecontext
\newcommand*\etransterm[1]{[#1]_{\varepsilon}}
\newcommand*\etranstype[1]{\llbracket {#1} \rrbracket_{\varepsilon}}

% Global translation: 1st arg = oracle, 2nd arg = source term/typecontext
\newcommand*\gtransterm[2]{[#2]^{#1}_{g}}
\newcommand*\gtranstype[2]{\llbracket {#2} \rrbracket^{#1}_{g}}

\newcommand*\xtransterm[1]{[#1]}
\newcommand*\xtranstype[1]{\llbracket {#1} \rrbracket}

% Global translation: 1st arg = oracle, 2nd arg = source term/typecontext
\newcommand*\utransterm[1]{\langle#1\rangle}
\newcommand*\utranstype[1]{\langle {#1} \rangle}

\newcommand{\algparam}[1]{{#1}_\varepsilon}

\newcommand{\gentree}{\mathfrak{t}}
\newcommand{\genelt}{\gamma}


\let\footnote\marginnote
\let\footfullcite\sidecite

\def\th@plain{%
  \thm@notefont{\bfseries}% same as heading font
  \itshape % body font
}


\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\theoremstyle{plain}
\newtheorem{corollary}{Corollary}
\newtheorem{exemple}{Example}
\newtheorem{traduction}{Traduction}
\theoremstyle{remark}
\newtheorem*{remark}{Remark}


\AtBeginDocument{
\renewenvironment{figure}{\begin{marginfigure}}{\end{marginfigure}}
}
\end_preamble
\use_default_options true
\begin_modules
environments
\end_modules
\maintain_unincluded_children false
\language english
\language_package babel
\inputencoding auto
\fontencoding global
\font_roman "default" "KpRoman"
\font_sans "default" "DejaVu Sans"
\font_typewriter "default" "DejaVu Sans Mono"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts true
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command bibtex
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine biblatex
\cite_engine_type numerical
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Chapter
Sheaves
\end_layout

\begin_layout Section
Presheaves and prefascist types
\end_layout

\begin_layout Section
Sheaves and Sh*TT
\end_layout

\end_body
\end_document
