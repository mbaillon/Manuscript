Require Import Setoid Lia Term LContexts Convertibility.

(*Types of our theory*)
Inductive type :=
| arr : type -> type -> type
| bool : type
| nat : type.

(*Typing rules*)
Inductive typing : list type -> term -> type -> Type :=
| typing_var : forall Γ A n, List.nth_error Γ n = Some A -> typing Γ (var n) A
| typing_lam : forall Γ A B t, typing (cons A Γ) t B -> typing Γ (lam t) (arr A B)
| typing_app : forall Γ A B t u, typing Γ t (arr A B) -> typing Γ u A -> typing Γ (app t u) B
| typing_tt : forall Γ, typing Γ tt bool
| typing_ff : forall Γ, typing Γ ff bool
| typing_alpha : forall Γ t, typing Γ t nat -> typing Γ (alpha t) bool
| typing_cse : forall Γ A t u1 u2,
  typing Γ t bool -> typing Γ u1 A -> typing Γ u2 A ->
  typing Γ (cse t u1 u2) A
| typing_z : forall Γ, typing Γ z nat
| typing_succ : forall Γ t, typing Γ t nat -> typing Γ (succ t) nat
| typing_natrec : forall Γ A t u1 u2,
    typing Γ t nat ->
    typing Γ u1 A ->
    typing Γ u2 (arr A A) ->
    typing Γ (natrec t u1 u2) A.

Notation "[ Γ ⊢ t : A ]" := (typing Γ t A) (t at level 0).


(*Well-typed weakenings*)
Inductive typing_lift : lift -> list type -> list type -> Prop :=
| typing_ELID : forall Γ, typing_lift ELID Γ Γ
| typing_ELSHFT : forall el Γ Δ A,
  typing_lift el Γ Δ ->
  typing_lift (ELSHFT el) (cons A Γ) Δ
| typing_ELLFT : forall el Γ Δ A,
  typing_lift el Γ Δ ->
  typing_lift (ELLFT el) (cons A Γ) (cons A Δ)
.

Notation "[ Γ ⊢lift e : Δ ]" := (typing_lift e Γ Δ) (e at level 0).

(*Induction principle for typing*)
Lemma typing_lift_rect :
  forall P : lift -> list type -> list type -> Type,
  (forall Γ : list type, P ELID Γ Γ) ->
  (forall (el : lift) (Γ Δ : list type) (A : type),
   [Γ ⊢lift el : Δ] -> P el Γ Δ -> P (ELSHFT el) (A :: Γ)%list Δ) ->
  (forall (el : lift) (Γ Δ : list type) (A : type),
   [Γ ⊢lift el : Δ] -> P el Γ Δ -> P (ELLFT el) (A :: Γ)%list (A :: Δ)%list) ->
  forall (l : lift) (l0 l1 : list type), [l0 ⊢lift l : l1] -> P l l0 l1.
Proof.
intros P f f0 f1.
fix F 1; intros e Γ Δ He; destruct e.
+ assert (Hrw : Γ = Δ).
  { inversion He; subst; reflexivity. }
  destruct Hrw; apply f.
+ destruct Γ as [|A Γ].
  { exfalso; inversion He. }
  assert (He' : [ Γ ⊢lift e : Δ ]).
  { inversion He; subst; assumption. }
  apply f0, F; apply He'.
+ destruct Γ as [|A Γ].
  { exfalso; inversion He. }
  destruct Δ as [|B Δ].
  { exfalso; inversion He. }
  assert (Hrw : A = B).
  { inversion He; subst; reflexivity. }
  destruct Hrw.
  assert (He' : [ Γ ⊢lift e : Δ ]).
  { inversion He; subst; assumption. }
  apply f1, F; apply He'.
Qed.

Lemma nth_error_app : forall A (l1 l2 : list A) n,
  List.nth_error (l1 ++ l2)%list (length l1 + n) = List.nth_error l2 n.
Proof.
induction l1 as [|x l1]; intros l2 n; cbn.
+ reflexivity.
+ apply IHl1.
Defined.

Lemma typing_lift_reloc : forall Γ Δ A n el,
  List.nth_error Γ n = Some A ->
  typing_lift el Δ Γ -> List.nth_error Δ (reloc n el) = Some A.
Proof.
intros Γ Δ A n el e Hel.
revert n A e.
induction Hel; cbn in *; intros n B e.
+ assumption.
+ apply IHHel; assumption.
+ destruct n; cbn in *.
  - assumption.
  - apply IHHel; assumption.
Qed.

Lemma typing_lift_compat : forall Γ Δ A t el,
  typing Γ t A -> typing_lift el Δ Γ -> typing Δ (lift_term el t) A.
Proof.
intros Γ Δ A t el Ht; revert Δ el; induction Ht; intros Δ el Hel; cbn.
+ apply typing_var.
  apply typing_lift_reloc with (Γ := Γ); assumption.
+ apply typing_lam.
  apply IHHt.
  apply (typing_ELLFT _ _ _ A); assumption.
+ apply typing_app with A.
  - apply IHHt1; assumption.
  - apply IHHt2; assumption.
+ now econstructor.
+ now econstructor. 
+ econstructor.
  now eapply IHHt.
+ apply typing_cse.
  - apply IHHt1; assumption.
  - apply IHHt2; assumption.
  - apply IHHt3; assumption.
+ now econstructor.
+ econstructor.
  now eapply IHHt.
+ apply typing_natrec.
  - apply IHHt1; assumption.
  - apply IHHt2; assumption.
  - apply IHHt3; assumption.
Qed.


Lemma typing_lift_compose : forall Γ Δ Ξ e1 e2,
  [ Γ ⊢lift e1 : Δ ] -> [ Δ ⊢lift e2 : Ξ ] -> [ Γ ⊢lift (lift_compose e1 e2) : Ξ ].
Proof.
intros Γ Δ Ξ e1 e2 He1; revert Ξ e2; induction He1; cbn in *; intros Ξ e2 He2.
+ assumption.
+ constructor; eauto.
+ destruct e2; cbn.
  - inversion He2; subst.
    constructor; assumption.
  - inversion He2; subst.
    constructor; apply IHHe1; assumption.
  - inversion He2; subst.
    constructor; apply IHHe1; assumption.
Qed.

(*Well-typed weakenings*)
Inductive typing_subs : subs term -> list type -> list type -> Type :=
| typing_ESID : forall Γ, typing_subs ESID Γ Γ
| typing_CONS : forall Γ Δ s t A, typing_subs s Γ Δ ->
                                  typing Γ t A ->
                                  typing_subs (CONS t s) Γ (cons A Δ)
| typing_SHFT : forall Γ Δ s A, typing_subs s Γ Δ ->
                                typing_subs (SHFT s) (cons A Γ) Δ
| typing_LIFT : forall Γ Δ s A, typing_subs s Γ Δ ->
                                typing_subs (LIFT s) (cons A Γ) (cons A Δ)
.

Notation "[ Γ ⊢subs e : Δ ]" := (typing_subs e Γ Δ) (e at level 0).

Lemma typing_wkn_n : forall Γ Δ A B t,
    typing (Δ ++ Γ) t A ->
    typing (Δ ++ cons B Γ) (lift_term (ELLFTn (length Δ) (ELSHFT ELID)) t) A.
Proof.
intros Γ Δ A B t Ht.
remember (Δ ++ Γ)%list as Ξ; revert Γ Δ B HeqΞ.
induction Ht; cbn; intros Δ Ξ X ->.
+ constructor.
  revert n Δ e X; induction Ξ as [|D Ξ]; intros n Δ e X; cbn.
  - assumption.
  - destruct n as [|n]; cbn in *; [assumption|].
    apply IHΞ; assumption.
+ apply typing_lam.
  change (A :: (Ξ ++ X :: Δ))%list with ((cons A nil) ++ (Ξ ++ X :: Δ))%list.
  rewrite List.app_assoc.
  apply IHHt; reflexivity.
+ eapply typing_app; intuition eauto.
+ now econstructor. 
+ now econstructor. 
+ econstructor.
  now apply IHHt.
+ apply typing_cse.
  - intuition eauto.
  - apply IHHt2; reflexivity.
  - apply IHHt3; reflexivity.
+ now econstructor.
+ econstructor ; now apply IHHt.
+ apply typing_natrec.
  - intuition eauto.
  - apply IHHt2; reflexivity.
  - apply IHHt3; reflexivity.  
Qed.

Lemma typing_wkn : forall Γ A B t,
  typing Γ t A -> typing (cons B Γ) (lift_term (ELSHFT ELID) t) A.
Proof.
intros.
apply typing_wkn_n with (Δ := nil); assumption.
Qed.

(*A well-typed term stays well-typed after applicaton of a well-typed substitution*)
Lemma typing_subs_compat : forall Γ Δ A s t,
  typing Γ t A -> typing_subs s Δ Γ -> typing Δ (subs_term s t) A.
Proof.
intros Γ Δ A s t Ht; revert Δ s; induction Ht; intros Δ s Hs; cbn.
+ revert n A e; induction Hs; cbn; intros n B e.
  - constructor; assumption.
  - destruct n as [|n]; cbn in *.
    * injection e; intros ->. eapply typing_lift_compat; try eassumption.
      constructor.
    * apply IHHs; assumption.
  - specialize (IHHs _ _ e).
    unfold expand_term in *; cbn.
    destruct (expand s n) as [[v k]|m].
    change (ELSHFTn (S k) ELID) with (lift_compose (ELSHFT ELID) (ELSHFTn k ELID)).
    rewrite lift_term_compose.
    apply typing_wkn; assumption.
    constructor; inversion IHHs; subst; assumption.
  - destruct n as [|n]; cbn.
   * constructor; assumption.
   * specialize (IHHs _ _ e).
    unfold expand_term in *; cbn.
    destruct (expand s n) as [[v k]|m].
    { change (ELSHFTn (S k) ELID) with (lift_compose (ELSHFT ELID) (ELSHFTn k ELID)).
      rewrite lift_term_compose.
      apply typing_wkn; assumption. }
    { constructor; inversion IHHs; subst; assumption. }
+ constructor; apply IHHt; constructor; apply Hs.
+ apply typing_app with A; intuition.
+ now econstructor. 
+ now econstructor. 
+ econstructor.
  now apply IHHt.
+ apply typing_cse.
  - intuition.
  - apply IHHt2 ; assumption.
  - apply IHHt3; assumption.
+ now econstructor.     
+ econstructor.
  now apply IHHt.
+ apply typing_natrec.
  - intuition.
  - apply IHHt2 ; assumption.
  - apply IHHt3; assumption.
Qed.

Lemma typing_subs_compose : forall Γ Δ Ξ σ1 σ2,
    [ Γ ⊢subs σ1 : Δ ] ->
    [ Δ ⊢subs σ2 : Ξ ] ->
    [ Γ ⊢subs (subs_compose subs_term σ1 σ2) : Ξ ].
Proof.
intros Γ Δ Ξ σ1 σ2 Hσ1; revert Ξ σ2; induction Hσ1; cbn in *; intros Ξ σ2 Hσ2.
+ assumption.
+ remember (A :: Δ)%list as Ω in *.
  induction Hσ2; subst.
  - constructor; assumption.
  - constructor; [apply IHHσ2; reflexivity|].
    apply typing_subs_compat with (cons A Δ); [assumption|].
    constructor; assumption.
  - apply IHHσ1.
    injection HeqΩ; intros -> ->; assumption.
  - injection HeqΩ; intros -> ->.
    constructor; [|assumption].
    apply IHHσ1; assumption.
+ constructor.
  apply IHHσ1; assumption.
+ remember (A :: Δ)%list as Ω in *.
  induction Hσ2; subst.
  - constructor; assumption.
  - constructor; [apply IHHσ2; reflexivity|].
    apply typing_subs_compat with (cons A Δ); [assumption|].
    constructor; assumption.
  - injection HeqΩ; intros -> ->.
    constructor; apply IHHσ1; assumption.
  - injection HeqΩ; intros -> ->.
    constructor.
    apply IHHσ1; assumption.
Qed.


Lemma typing_lift_subs : forall Γ Δ e,
  [ Γ ⊢lift e : Δ ] -> [ Γ ⊢subs (subs_of_lift e) : Δ ].
Proof.
  intros.
  pose (q:= typing_lift_rect (fun e G D => [G ⊢subs (subs_of_lift e) : D])).
  cbn in *.
  unshelve eapply q.
  4: eassumption.
  clear Γ Δ H.
  all: intros.
  - econstructor.
  - econstructor.
    eassumption.
  - econstructor.
    eassumption.
Qed.

(*Typed reduction*)
Record ty_reductions l Γ A t u : Type:=
  {
    ty_t : [ Γ ⊢ t : A ] ;
    ty_u : [ Γ ⊢ u : A ] ;
    red : reductions l t u
  }.


(*Monotonicity of typed reduction for the forcing conditions*)
Lemma ty_reductions_Ltrans: forall l l' Γ t r A (f : l' ≤ε l),
    ty_reductions l Γ A t r -> ty_reductions l' Γ A t r.
Proof.
  intros * f [].
  econstructor ; try assumption.
  eapply reductions_Ltrans ; eassumption.
Qed.


(*Compatibility of typed reduction with weakening*)
Lemma ty_reductions_lift {l : wfLCon} {Γ Δ A} : forall t r e,
    [ Δ ⊢lift e : Γ ] ->
    ty_reductions l Γ A t r ->
    ty_reductions l Δ A (lift_term e t) (lift_term e r).
Proof.
  intros * He [].
  econstructor.
  - eapply typing_lift_compat ; eassumption.
  - eapply typing_lift_compat ; eassumption.
  - eapply reductions_lift.
    assumption.
Qed.

Lemma ty_reductions_app_l {l Γ A B}:
  forall (t u r : term),
    ty_reductions l Γ (arr A B) t r ->
    typing Γ u A ->
    ty_reductions l Γ B (app t u) (app r u).
Proof.
  intros * [] Hu.
  econstructor.
  - econstructor ; try eassumption.
  - econstructor ; try eassumption.
  - eapply reductions_app_l.
    assumption.
Qed.

Lemma ty_reductions_trans {l Γ A}:
  forall (t u r : term),
    ty_reductions l Γ A t u ->
    ty_reductions l Γ A u r ->
    ty_reductions l Γ A t r.
Proof.
  intros * [] [].
  econstructor ; try eassumption.
  eapply reductions_trans ; eassumption.
Qed.


Lemma ty_reductions_whnf : forall l Γ t r u A,
    NF.NForms NF.nf u ->
    ty_reductions l Γ A t u ->
    ty_reductions l Γ A t r ->
    ty_reductions l Γ A r u.
Proof.
  intros * nfu [] [].
  econstructor ; try eassumption.
  eapply reductions_whnf ; eassumption.
Qed.    


Lemma ty_reductions_alpha_succ : forall l  Γ t r,
    ty_reductions l Γ bool (alpha t) (alpha r) ->
    ty_reductions l Γ bool (alpha (succ t)) (alpha (succ r)).
Proof.
  intros * [].
  unshelve econstructor.
  3: eapply reductions_alpha_succ ; eassumption.
  all: do 2 econstructor.
  inversion ty_t0 ; subst ; assumption.
  inversion ty_u0 ; subst ; assumption.
Qed.  
