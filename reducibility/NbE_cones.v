Require List.
Require Import CMorphisms PeanoNat Lia.

Require Import Term LContexts Convertibility Typing.

Set Primitive Projections.
Set Implicit Arguments.

(*Strong reducibility for nat*)
Inductive NatProp {l : wfLCon} {Γ : list type} : term -> Set :=
| zeroR  {n}
  (eq: ty_reductions l Γ nat n z): NatProp n
| succR {n}
    (nf : term)
    (eq: ty_reductions l Γ nat n (succ nf)):
    NatProp nf ->
    NatProp n
| natneR {n} (ne : term)
    (eq : ty_reductions l Γ nat n ne): NF.NForms NF.ne ne -> NatProp n.

(*Strong reducibility for bool*)
Inductive BoolProp {l : wfLCon} {Γ : list type} : term -> Set :=
| trueR  {n}
  (eq: ty_reductions l Γ bool n tt): BoolProp n
| falseR  {n}
  (eq: ty_reductions l Γ bool n ff): BoolProp n
| boolneR {n} (ne : term)
    (eq : ty_reductions l Γ bool n ne): NF.NForms NF.ne ne -> BoolProp n.


Unset Implicit Arguments.

(*A term is reified when it reduces to a normal form, after enough extensions of l*)
Record reified_aux l Γ A t : Type :=
  { reify_trm : term ;
    reify_typ : NF.NForms NF.nf reify_trm ;
    reify_cvn : ty_reductions l Γ A t reify_trm ;
  }.

Record reified (l : wfLCon) (Γ : list type) (A : type) (t : term) : Type :=
  {
    RN : Datatypes.nat ;
    Rei : forall {l'} (f : l' ≤ε l)
                 (Minfl : AllInLCon RN l'),
      reified_aux l' Γ A t
  }.

Lemma reified_aux_ty l Γ A t :
  reified_aux l Γ A t -> [ Γ ⊢ t : A ].
Proof.
  now induction 1 as [ ? ? []].
Qed.


Lemma reified_ty l Γ A t :
  reified l Γ A t -> [ Γ ⊢ t : A ].
Proof.
  destruct 1 as [N NR].
  unshelve eapply reified_aux_ty.
  - exact (L_All l N).
  - eapply NR.
    + eapply L_All_wfle.
    + now eapply L_All_All.
Qed.

(*The Weak monad, building the cone of possibilities*)
Record Weak (l : wfLCon) (Γ : list type) (t : term)
  (P : wfLCon -> list type -> term -> Type) : Type :=
  {
    WN : Datatypes.nat ;
    WRed : forall {l'} (f : l' ≤ε l)
                 (Minfl : AllInLCon WN l'),
      P l' Γ t
  }.

(*Strong reducibility for the arrow type. It makes use of the Weak monad*)
Record ArrProp
    {l : wfLCon}
    {Γ : list type} {A B : type} {t : term}
    {Pa Pb :  wfLCon -> list type -> term -> Type}
  : Type := {
    nf_term : term ;
    nf_nf : NF.NForms NF.nf nf_term ;
    nf_red : ty_reductions l Γ (arr A B) t nf_term ;
    domN : Datatypes.nat ;
    codRed : forall Δ l' (e : lift) (eε : typing_lift e Δ Γ)
                    (f : l' ≤ε l) (Ninfl : AllInLCon domN l'),
    forall (x : term) (xε : Pa l' Δ x),
      Weak l' Δ (app (lift_term e t) x) Pb ;
    }.

(*Strong reducibility can be defined by recursion on the type*)
Fixpoint eval (l : wfLCon) (Γ : list type) (A : type) (t : term) {struct A} : Type :=
match A with
| bool => @BoolProp l Γ t
| nat => @NatProp l Γ t
| arr A B => @ArrProp  l Γ A B t
               (fun l' delta a => eval l' delta A a)
               (fun l' delta b => eval l' delta B b)
end.

Notation realizes := eval.

(*Weak reducibility*)
Definition Weval (l : wfLCon) (Γ : list type) (A : type) (t : term) : Type :=
  Weak l Γ t (fun l' delta a => eval l' delta A a).

(*Monotonicity of strong reducibility under extension of l*)
Lemma eval_Ltrans (l l' : wfLCon) (Γ : list type) (A : type) (t : term) :
  l' ≤ε l -> eval l Γ A t -> eval l' Γ A t.
Proof.
  revert l' Γ t.
  induction A ; intros l' Γ t f H.
  - destruct H.
    unshelve econstructor.
    3: eassumption.
    + eassumption.
    + eapply ty_reductions_Ltrans ; eassumption.
    + intros Δ l'' e eε f' Ninfl x xε.
      unshelve eapply codRed0 ; try eassumption.
      etransitivity ; eassumption.
  - revert l' f.
    induction H ; intros.
    + unshelve econstructor.
      eapply ty_reductions_Ltrans ; try eassumption.
    + eapply falseR.
      eapply ty_reductions_Ltrans ; eassumption.
    + eapply boolneR ; try eassumption.
      eapply ty_reductions_Ltrans ; eassumption.
  - revert l' f.
    induction H ; intros.
    + unshelve econstructor.
      eapply ty_reductions_Ltrans ; eassumption.
    + eapply succR.
      eapply ty_reductions_Ltrans ; eassumption.
      eapply IHNatProp.
      assumption.
    + eapply natneR ; try eassumption.
      eapply ty_reductions_Ltrans ; eassumption.
Qed.

(*Strong is weak*)
Definition etaeval (l : wfLCon) (Γ : list type) (A : type) (t : term) :
  eval l Γ A t -> Weval l Γ A t.
Proof.
  exists 0.
  intros ; eapply eval_Ltrans ; eassumption.
Defined.

(*Monotonicity of weak reducibility under extension of l*)
Lemma Weval_Ltrans (l l' : wfLCon) (Γ : list type) (A : type) (t : term) :
  l' ≤ε l -> Weval l Γ A t -> Weval l' Γ A t.
Proof.
  intros f H ; revert l' f.
  intros.
  unshelve econstructor.
  - eapply WN.
    exact H.
  - intros.
    eapply (WRed (l' := l'0) l Γ t _ H).
    2: exact Minfl.
    etransitivity ; eassumption.
Qed.
  
(*Stability of strong reducibility under weakening*)
Lemma lift_eval : forall l Γ Δ A t e, [ Δ ⊢lift e : Γ ] ->
  eval l Γ A t -> eval l Δ A (lift_term e t).
Proof.
  intros l Γ Δ A; revert Γ Δ; induction A; intros Γ Δ t e He Ht; cbn in *.
  - destruct Ht as [nft nfnf rednf Nred Ht].
    unshelve econstructor.
    4: eapply ty_reductions_lift ; try eassumption.
    + eassumption.
    + eapply NF_lift_compat ; try eassumption.
    + intros Ξ l' e' He' f Ninfl x xε.
      cbn.
      rewrite <- lift_term_compose.
      eapply Ht ; try eassumption.
      eapply typing_lift_compose ; eassumption.
 - induction Ht.
   + eapply trueR.
     eapply ty_reductions_lift in eq ; try eassumption.
   + eapply falseR.
     eapply ty_reductions_lift in eq ; try eassumption.
   + unshelve eapply boolneR.
     * exact (lift_term e ne).
     * eapply ty_reductions_lift ; try eassumption.
     * eapply NF_lift_compat ; eassumption.
 - induction Ht.
   + eapply zeroR.
     eapply ty_reductions_lift in eq ; try eassumption.
   + eapply succR.
     eapply ty_reductions_lift in eq ; try eassumption.
     now apply IHHt.
   + unshelve eapply natneR.
     * exact (lift_term e ne).
     * eapply ty_reductions_lift ; try eassumption.
     * eapply NF_lift_compat ; eassumption.
Qed.

(*Stability of weak reducibility under weakening*)
Lemma Wlift_eval : forall l Γ Δ A t e, [ Δ ⊢lift e : Γ ] ->
  Weval l Γ A t -> Weval l Δ A (lift_term e t).
Proof.
  intros * Hel Ht.
  unshelve econstructor.
  - eapply WN.
    eassumption.
  - intros.
    eapply lift_eval ; try eassumption.
    eapply (WRed l Γ t _ Ht f).
    exact Minfl.
Qed.    

(*Escape lemma for strong reducibility*)
Lemma escape_ty : forall l Γ A t, eval l Γ A t -> [ Γ ⊢ t : A ] .
Proof.
  intros l Γ A; revert l Γ.
  induction A as [A IHA B IHB| |]; intros l Γ t Ht; cbn in *.
  - now destruct Ht as [ ? ? [] ?].
  - induction Ht.
    + now destruct eq.
    + now destruct eq.
    + now destruct eq.
  - induction Ht ; try now destruct eq.
Qed.

(*Escape lemma for weak reducibility*)
Lemma Wescape_ty : forall l Γ A t, Weval l Γ A t -> [ Γ ⊢ t : A ] .
Proof.
  intros * Ht.
  unshelve eapply escape_ty.
  - unshelve eapply L_All.
    assumption.
    now eapply WN ; eassumption.
  - eapply (WRed l Γ t _ Ht).
    + eapply L_All_wfle.
    + eapply L_All_All.
Qed.      

(*Compatibility of strong reducibility with reduction*)
Lemma eval_reductions_expansion:
  forall l Γ A t r, ty_reductions l Γ A t r -> eval l Γ A r -> eval l Γ A t.
Proof.
  intros l Γ A; revert l Γ; induction A as [A IHA B IHB| |]; intros l Γ t r Ht Hr; cbn in *.
  - destruct Hr as [nfr nfnf rednf redN Hr].
    unshelve econstructor.
    3: eassumption.
    + eassumption.
    + eapply ty_reductions_trans ; eassumption.
    + intros Δ l' e He f Ninfl x xε.
      unshelve econstructor.
      * eapply WN.
        eapply (Hr Δ l') ; eassumption.
      * intros.
        apply IHB with (app (lift_term e r) x).
        eapply ty_reductions_app_l.
        2: eapply escape_ty ; eassumption.
        eapply ty_reductions_lift ; try eassumption.
        eapply ty_reductions_Ltrans.
        2: eassumption.
        eapply wfLCon_le_trans ; eassumption.
        eapply (WRed l' Δ _ _ (Hr Δ l' e He f Ninfl x xε)) ; eassumption.
  - induction Hr.
    + econstructor.
      eapply ty_reductions_trans ; eassumption.
    + eapply falseR.
      eapply ty_reductions_trans ; eassumption.
    + eapply boolneR.
      eapply ty_reductions_trans ; eassumption.
      assumption.
  - revert t Ht.
    induction Hr ; intros.
    + econstructor.
      eapply ty_reductions_trans ; eassumption.
    + eapply succR ; try eassumption.
      eapply ty_reductions_trans ; eassumption.
    + eapply natneR.
      eapply ty_reductions_trans ; eassumption.
      assumption.
Qed.        

(*Compatibility of weak reducibility with reduction*)
Lemma Weval_reductions_expansion:
  forall l Γ A t r, ty_reductions l Γ A t r -> Weval l Γ A r -> Weval l Γ A t.
Proof.
  intros * Hred Hr.
  unshelve econstructor.
  - eapply WN ; eassumption.
  - intros.
    eapply eval_reductions_expansion.
    + eapply ty_reductions_Ltrans ; eassumption.
    + eapply (WRed l Γ _ _ Hr) ; try eassumption.
Qed.

(*Compatibility the other way around*)
Lemma eval_reductions_compat :
  forall l Γ A t r, ty_reductions l Γ A t r -> eval l Γ A t -> eval l Γ A r.
Proof.
  intros l Γ A; revert l Γ; induction A as [A IHA B IHB| |]; intros l Γ t r Hr Ht; cbn in *.
  - destruct Ht as [nft nfnf rednf redN Ht].
    unshelve econstructor.
    3: eassumption.
    + eassumption.
    + eapply ty_reductions_whnf ; try eassumption.
    + intros Δ l' e He f Ninfl x xε.
      unshelve econstructor.
      * eapply WN.
        eapply (Ht Δ l') ; eassumption.
      * intros.
        apply IHB with (app (lift_term e t) x).
      -- eapply ty_reductions_app_l ; try eassumption.
         eapply ty_reductions_lift ; try eassumption.
         eapply ty_reductions_Ltrans.
         ++ eapply wfLCon_le_trans ; eassumption.
         ++ eassumption.
         ++ eapply escape_ty ; eassumption.
      -- eapply Ht.
         2: exact Minfl.
         eassumption.
  - induction Ht.
    + eapply trueR.
      unshelve eapply ty_reductions_whnf ; try eassumption.
      now eapply NF.NForms_tt.
    + eapply falseR.
      unshelve eapply ty_reductions_whnf ; try eassumption.
      now eapply NF.NForms_ff.
    + eapply boolneR ; try eassumption.
      unshelve eapply ty_reductions_whnf.
      3,4: eassumption.
      constructor ; eassumption.
  -  induction Ht.
    + eapply zeroR.
      unshelve eapply ty_reductions_whnf ; try eassumption.
      now eapply NF.NForms_z.
    + eapply succR with nf ; try eassumption.
      assert (forall t x y, reductions l x (succ t) -> reduction l x y -> reductions l y (succ t)).
      { clear r n Hr nf eq Ht IHHt.
        intros * redxt redxy.
        revert y redxy.
        remember (succ t).
        induction redxt ; intros.
        + subst.
          now inversion redxy.
        + subst.
          eapply reduction_det in redxy.
          2: exact r.
          subst.
          eapply reductions_trans ; try eassumption.
          now econstructor.
      }
      assert (forall t x y, reductions l x (succ t) -> reductions l x y -> reductions l y (succ t)).
      { clear r n Hr nf eq Ht IHHt.
        intros * redxt redxy.
        revert t redxt.
        induction redxy ; intros.
        - assumption.
        - eapply IHredxy.
          eapply H ; eassumption.
      }
      assert (forall t x y, ty_reductions l Γ nat x (succ t) ->
                            ty_reductions l Γ nat x y ->
                            ty_reductions l Γ nat y (succ t)).
      { intros * [] [].
        unshelve econstructor ; try eassumption.
        eapply H0 ; eassumption.
      }
      unshelve eapply H1 with n ; assumption.
    + eapply natneR.
      2: eassumption.
      unshelve eapply ty_reductions_whnf.
      4: eassumption.
      econstructor; eassumption.
      assumption.
Qed.

Lemma Weval_reductions_compat :
  forall l Γ A t r, ty_reductions l Γ A t r -> Weval l Γ A t -> Weval l Γ A r.
Proof.
  intros * Hred Ht.
  unshelve econstructor.
  - eapply WN ; eassumption.
  - intros.
    eapply eval_reductions_compat ; try eassumption.
    + eapply ty_reductions_Ltrans ; try eassumption.
    + eapply (WRed l Γ t _ Ht) ; eassumption.
Qed.

(*The mu operator of the monad: weak weak reducibility is weak reducibility*)
Lemma Weval_fusion Γ A t l N
                   (Ht : forall l' (f : l' ≤ε l)
                                (Minfl : AllInLCon N l'),
                       Weval l' Γ A t) :
  Weval l Γ A t.
Proof.
  unshelve econstructor.
  - unshelve eapply (max N (Max_Bar _ _ _)).
    + assumption.
    + assumption.
    + intros l' f Ninfl.
      now eapply (WN _ _ _ _ (Ht l' f Ninfl)).
  - intros.
    unshelve eapply (WRed _ _ _ _ (Ht _ _ _)).
    unshelve eapply Bar_lub.
    3,4: eassumption.
    + eapply AllInLCon_le ; try eassumption.
      now eapply Nat.max_lub_l.
    + now eapply Bar_lub_ub.
    + now eapply Bar_lub_AllInLCon.
    + now eapply Bar_lub_smaller.
    + eapply AllInLCon_le ; try eassumption.
      etransitivity ; [ | now eapply Nat.max_lub_r].
      etransitivity.
      2: eapply Max_Bar_Bar_lub.
      reflexivity.
Qed.


(*Validity for substitutions*)
Inductive eval_subs (l : wfLCon) : list type -> list type -> subs term -> Type :=
| eval_subs_ESID :
  forall Γ e,
    [Γ ⊢lift e : nil] ->
    eval_subs l Γ nil (subs_of_lift e)
| eval_subs_CONS : forall Γ Δ A t σ,
    realizes l Γ A t ->
    eval_subs l Γ Δ σ ->
    eval_subs l Γ (cons A Δ) (CONS t σ)
| eval_subs_SHFT : forall Γ Δ A σ,
    eval_subs l Γ Δ σ ->
    eval_subs l (cons A Γ) Δ (SHFT σ)
.

(*Monotonicity of validity for subsitutions under extension of l*)
Lemma eval_subs_Ltrans l l' Δ Γ σ :
  l' ≤ε l -> eval_subs l Δ Γ σ -> eval_subs l' Δ Γ σ.
Proof.
  intros f. induction 1.
  - econstructor ; eassumption.
  - econstructor ; try eassumption.
    eapply eval_Ltrans ; eassumption.
  - now econstructor.
Qed.

(*Escape for subsitutions validity*)
Lemma eval_subs_ty l Δ Γ σ : eval_subs l Δ Γ σ -> [Δ ⊢subs σ : Γ].
Proof.
  induction 1.
  - eapply typing_lift_subs ; eassumption.
  - econstructor.
    assumption.
    eapply escape_ty ; eassumption.
  - econstructor.
    assumption.
Qed.
  
Set Implicit Arguments.


Record interp (A : type) := {
  reflect : forall l Γ t, NF.NForms NF.ne t -> [ Γ ⊢ t : A ] -> eval l Γ A t;
  reify : forall l Γ t, eval l Γ A t -> reified_aux l Γ A t;
}.

Unset Implicit Arguments.

(*Completeness of the model : a strongly reducible term reduces
 to a well-typed normal form*)
Lemma completeness : forall A, interp A.
Proof.
  induction A; split; cbn in *.
  - intros l Γ t Ht1 Ht2.
    unshelve econstructor.
    3: econstructor ; eassumption.
    + exact 0.
    + econstructor ; try eassumption.
      now econstructor.
    + cbn.
      intros Δ l'' e eε f' Ninfl x xε.
      assert (r := (IHA1.(reify) _ _ _ xε)).
      clear xε.
      exists 0.
      intros.
      apply IHA2.(reflect).
      * eapply NF.NForms_app ; eapply NF_lift_compat ; eassumption.
      * eapply typing_app.
        -- eapply typing_lift_compat ; eassumption.
        -- clear f Ninfl Minfl IHA1 IHA2 Ht2.
           now destruct r as [ ? ? []].
  - intros l Γ t Ht.
    destruct Ht.
    unshelve econstructor.
    2: eassumption.
    eassumption.
  - intros l Γ t Ht1 Ht2.
    eapply boolneR ; try eassumption.
    econstructor.
    3: now econstructor.
    all : try eassumption.
    all : eapply Ntyping_typing ; eassumption.
  - intros l Γ t Ht.
    destruct Ht.
    + econstructor.
      eapply NF.NForms_tt.
      eassumption.
    + econstructor.
      now eapply NF.NForms_ff.
      eassumption.
    + econstructor ; try eassumption.
      now econstructor.
  - intros l Γ t Ht1 Ht2.
    eapply natneR ; try eassumption.
    econstructor.
    3: now econstructor.
    all : eassumption.
  - intros l Γ t [].
    + econstructor.
      eapply NF.NForms_z.
      eassumption.
    + econstructor.
      now eapply NF.NForms_succ.
      eassumption.
    + econstructor ; try eassumption.
      now econstructor.
Qed.

(*Weak completeness: a weakly reducible term reduces to a normal forms
 after enough extensions of the list l.*)
Lemma Wreified (A: type) :
  forall l Γ t, Weval l Γ A t -> reified l Γ A t.
Proof.
  intros * [N Red].
  exists N.
  intros.
  eapply completeness.
  now eapply Red.
Qed.

(*Lemmas regarding alpha*)

(*If n is in the list then (alpha (nat_to_term n)) is strongly reducible*)
Lemma eval_alpha : forall l Γ n b,
    in_LCon (pi1 l) n b ->
    realizes l Γ bool (alpha (nat_to_term n)).
Proof.
  intros.
  induction b.
  all: eapply eval_reductions_expansion.
  1,3: econstructor.
  3,6: eapply reductions_step ; try (now econstructor).
  3: eapply (reduction_alpha_red _ _ true) ; try eassumption.
  3: eapply (reduction_alpha_red _ _ false) ; try eassumption.
  1-5:cbn ; econstructor.
  4: eapply falseR.
  3,4: econstructor ; try reflexivity.
  all: clear H ; try now econstructor.
  all: induction n ; try now econstructor ; assumption.
Qed.

(*Helper to deal with alpha, separating it into two cases. Either
  alpha (nat_to_term n) or a neutral*)
Inductive canonicity_aux l Γ : term -> Type :=
| eta : forall n t, ty_reductions l Γ bool t (alpha (nat_to_term n)) ->
                    canonicity_aux l Γ t
| canone {n} (ne : term)
    (eq : ty_reductions l Γ bool n ne):
  NF.NForms NF.ne ne  -> canonicity_aux l Γ n.


Inductive alphareduc : term -> Type :=
| alphane n (ne : term):
  NF.NForms NF.ne ne ->
  alphareduc (alpha (nSucc n ne)).

Lemma alphareduction_alphareduc l :
  forall t ne,
    NF.NForms NF.ne ne ->
    reduction l (alpha t) ne ->
    alphareduc ne.
Proof.
  intros.
  inversion H0.
  - subst.
    inversion H ; subst.
    econstructor.
    assumption.
  - exfalso.
    induction b ; subst ; inversion H.
Qed.

Lemma alphareductions_alphareduc l :
  forall t ne,
    NF.NForms NF.ne ne ->
    reductions l (alpha t) ne ->
    alphareduc ne.
Proof.
  intros.
  remember (alpha t).
  revert t Heqt0.
  induction H0 ; intros.
  - subst.
    inversion H ; subst.
    econstructor.
    assumption.
  - subst.
    inversion r ; subst.
    + eapply IHreductions.
      assumption.
      reflexivity.
    + exfalso.
      induction b ; cbn in * ; inversion H0 ; subst.
      all: try now inversion H.
Qed.

(*Our helper inductive is useful thanks to this lemma*)
Lemma NatProp_canonicity_aux : forall l Γ t,
    @NatProp l Γ t -> canonicity_aux l Γ (alpha t).
Proof.
  intros * ; induction 1.
  - unshelve eapply eta.
    exact 0.
    destruct eq ; econstructor.
    3: eapply (reductions_alpha_subst _ _ _ 0) ; eassumption.
    all: now econstructor.
  - remember (alpha nf).
    induction IHNatProp.
    + unshelve eapply eta.
      exact (S n0).
      eapply ty_reductions_trans.
      1: econstructor.
      3: eapply (reductions_alpha_subst _ _ _ 0).
      3: destruct eq ; eassumption.
      all: cbn ; econstructor.
      3: econstructor.
      1-3: now destruct eq.
      * do 2 econstructor.
         clear t0 ; induction n0 ; cbn ; econstructor.
         assumption.
      * subst.
         eapply ty_reductions_alpha_succ.
         eassumption.
    + subst.
      destruct eq0.
      assert (Hyp:= alphareductions_alphareduc _ _ _ n1 red).
      destruct Hyp.
      unshelve eapply canone.
      exact (alpha (nSucc (S n0) ne)).
      2: now eapply NF.NForms_alpha.
      destruct eq.
      * econstructor ; cbn in * ; try now econstructor.
        do 2 econstructor.
        now inversion ty_u ; subst.
        eapply reductions_trans.
        eapply (reductions_alpha_subst _ _ _ 0) ; eassumption.
        cbn.
        eapply reductions_alpha_succ.
        assumption.
  - destruct eq.
    eapply (reductions_alpha_subst _ _ _ 0) in red.
    eapply (NF.NForms_alpha _ 0) in n0 ; cbn in *.
    eapply typing_alpha in ty_u.
    destruct (alphareductions_alphareduc _ _ _ n0 red).
    eapply canone.
    econstructor.
    3: eassumption.
    3: eassumption.
    all: econstructor ; try eassumption.
    clear red n0.
    now inversion ty_u ; subst.
Qed. 

(*To prove weak reducibility for alpha, we need to provide a natural number.
  This natural number comes from the term alpha is applied to. 
  This is the meaning behind AlphaN  *)
Definition AlphaN_aux (l : wfLCon) Γ t
  (Ht : canonicity_aux l Γ t) : Datatypes.nat :=
  match Ht with
  | eta _ _ n _ _ => n
  | _ => 0
  end.

Lemma AlphaN_aux_Ltrans (l l' : wfLCon) Γ t Ht Ht' :
  l' ≤ε l -> AlphaN_aux l Γ t Ht = AlphaN_aux l' Γ t Ht'.
Proof.
  revert l' Ht'.
  induction Ht ; intros.
  - induction Ht'.
    + cbn.
      destruct t0 ; destruct t1.
      clear ty_t ty_u ty_t0 ty_u0.
      remember (alpha (nat_to_term n)).
      induction red.
      * subst.
        remember (alpha (nat_to_term n0)).
        remember (alpha (nat_to_term n)).
        destruct red0.
        -- subst.
           injection Heqt0 ; intros ; subst.
           now eapply nattoterminj.
        -- subst.
           inversion r ; subst.
           assert (z:= nat_to_term_nSucc (eq_sym H0)) ; subst.
           ++ induction (n - n1).
              ** cbn in *.
                 inversion H1.
              ** cbn in *.
                 now inversion H1.
           ++ induction b ; cbn in *.
              ** remember (alpha (nat_to_term n0)).
                 remember (tt).
                 destruct red0.
                 --- rewrite Heqt in Heqt0.
                     now inversion Heqt0.
                 --- subst.
                     now inversion r0.
              ** remember (alpha (nat_to_term n0)).
                 remember ff.
                 destruct red0.
                 --- rewrite Heqt in Heqt0.
                     now inversion Heqt0.
                 --- subst.
                     now inversion r0.
      * remember (alpha (nat_to_term n0)).
        destruct red0.
        -- subst.
           inversion r ; subst.
           ++ assert (z:= nat_to_term_nSucc (eq_sym H0)).
              subst.
              induction (n0 - n1).
              ** now inversion H1.
              ** now inversion H1.
           ++ induction b ; cbn in *.
              ** remember (alpha (nat_to_term n)).
                 remember (tt).
                 destruct red.
                 --- rewrite Heqt in Heqt0.
                     now inversion Heqt0.
                 --- subst.
                     now inversion r0.
              ** remember (alpha (nat_to_term n)).
                 remember ff.
                 destruct red.
                 --- rewrite Heqt in Heqt0.
                     now inversion Heqt0.
                 --- subst.
                     now inversion r0.
        -- subst.
           eapply IHred.
           reflexivity.
           assert (u = u0).
           { eapply reduction_det ; [ | eassumption].
             eapply reduction_Ltrans ; eassumption.
           }
           subst ; assumption.
    + exfalso.
      destruct t0 ; destruct eq.
      assert (Hred := reductions_whnf l' _ _ _ (NF.NForms_ne _ n1) red0
                        (reductions_Ltrans l l' _ _ H red)).
      clear red red0.
      remember (alpha (nat_to_term n)).
      destruct Hred.
      * subst.
        inversion n1.
        assert (Heq := nat_to_term_nSucc (eq_sym H0)) ; subst.
        induction (n-n2) ; now inversion H1.
      * subst.
        inversion r ; subst.
        -- assert (Heq := nat_to_term_nSucc (eq_sym H0)) ; subst.
           induction (n-n2) ; now inversion H1.        
        -- clear r n2 H1 H0.
           induction b ; cbn in *.
           ++ remember tt ; destruct Hred ; subst.
              ** now inversion n1.
              ** now inversion r.
           ++ remember ff ; destruct Hred ; subst.
              ** now inversion n1.
              ** now inversion r.
  - induction Ht'.
    + exfalso.
      destruct t0 ; destruct eq.
      assert (Hred := reductions_whnf l' _ _ _ (NF.NForms_ne _ n0)
                        (reductions_Ltrans l l' _ _ H red0) red).
      clear red red0.
      remember (alpha (nat_to_term n)).
      destruct Hred.
      * subst.
        inversion n0.
        assert (Heq := nat_to_term_nSucc (eq_sym H0)) ; subst.
        induction (n-n1) ; now inversion H1.
      * subst.
        inversion r ; subst.
        -- assert (Heq := nat_to_term_nSucc (eq_sym H0)) ; subst.
           induction (n-n1) ; now inversion H1.        
        -- clear r n1 H1 H0.
           induction b ; cbn in *.
           ++ remember tt ; destruct Hred ; subst.
              ** now inversion n0.
              ** now inversion r.
           ++ remember ff ; destruct Hred ; subst.
              ** now inversion n0.
              ** now inversion r.
    + reflexivity.
Qed.

Definition AlphaN (l : wfLCon) Γ t N
  (Ht : forall l' : wfLCon, l' ≤ε l -> AllInLCon N l' -> canonicity_aux l' Γ t) :
  Datatypes.nat.
Proof.
  unshelve eapply Max_Bar.
  - assumption.
  - assumption.
  - intros l' f Ninfl.
    specialize (Ht l' f Ninfl).
    eapply AlphaN_aux.
    eassumption.
Defined.

(*Weak reducibility helper for natrec*)
Lemma Weval_natrec Γ A t u1 u2 l
  (Redt1 :  realizes l Γ nat t)
  (Redu1 :  realizes l Γ A u1)
  (Redu2 :  realizes l Γ (arr A A) u2) :
  Weval l Γ A (natrec t u1 u2).
Proof.
  induction Redt1.
  - exists 0.
    intros.
    unshelve eapply eval_reductions_expansion.
    + exact u1.
    + destruct eq.
      eapply ty_reductions_trans.
      { unshelve econstructor.
        3:  eapply reductions_natrec_e.
        2: econstructor.
        2: exact ty_u.
        4: eapply reductions_Ltrans ; eassumption.
        all: try econstructor ; try eassumption.
        all : try eapply escape_ty.
        all: try eassumption.
      }
      unshelve econstructor.
      3: eapply reductions_step.
      3: eapply reduction_ι_z.
      econstructor ; try eassumption.
      4: now econstructor.
      all : try eapply escape_ty ; eassumption.
    + eapply eval_Ltrans ; eassumption.
  - unshelve eexists (max (max (WN _ _ _ _ IHRedt1) Redu2.(domN)) _) ; intros.
    { unshelve eapply Max_Bar.
      + eassumption.
      + exact  (max (WN _ _ _ _ IHRedt1) Redu2.(domN)).
      + intros l' f Ninfl.
        unshelve eapply (WN _ _ _ _ (@codRed _ _ A A u2 _ _ Redu2 _ _ _ (typing_ELID _) _ _ _ _)).
        5: eapply (WRed _ _ _ _ IHRedt1).
        3,5: eapply AllInLCon_le ; try eassumption.
        1,4: eassumption.
        now eapply Nat.max_lub_r.
        now eapply Nat.max_lub_l.
    }
    unshelve eapply eval_reductions_expansion.
    + exact (app u2 (natrec nf u1 u2)).
    + destruct eq.
      eapply ty_reductions_trans.
      { unshelve econstructor.
        3: eapply reductions_natrec_e ; eapply reductions_Ltrans ; eassumption.
        all: econstructor ; try eassumption.
        all: eapply escape_ty ; try eassumption.
      }
      unshelve econstructor.
      3: eapply reductions_step ; try now econstructor.
      econstructor ; try eassumption.
      3: econstructor.
      1-4: eapply escape_ty.
      all: try eassumption.
      eapply (WRed _ _ _ _ IHRedt1).
      eassumption.
      eapply AllInLCon_le ; try eassumption.
      eapply Nat.max_lub_l.
      now eapply Nat.max_lub_l.
    + erewrite <- (lift_term_ELID u2) at 1.
      unshelve eapply (WRed _ _ _ _ (@codRed _ _ A A u2 _ _ Redu2 _ _ _ (typing_ELID _) _ _ _ _)).
      unshelve eapply Bar_lub.
      4: eassumption.
      exact (max (WN _ _ _ _ IHRedt1) (domN Redu2)).
      2: now eapply Bar_lub_ub.
      3: eapply (WRed _ _ _ _ IHRedt1).
      * eapply AllInLCon_le ; try eassumption.
        now eapply Nat.max_lub_l.
      * eapply AllInLCon_le ; [ | now eapply Bar_lub_AllInLCon].
        now eapply Nat.max_lub_r.
      * now eapply Bar_lub_ub.
      * eapply AllInLCon_le ; [ | now eapply Bar_lub_AllInLCon].
        now eapply Nat.max_lub_l.
      * now eapply Bar_lub_smaller.
      * eapply AllInLCon_le ; [ | exact Minfl].
        etransitivity.
        2: now eapply Nat.max_lub_r.
        etransitivity.
        2: unshelve eapply Max_Bar_Bar_lub.
        reflexivity.
  - exists 0.
    intros.
    unshelve eapply eval_reductions_expansion.
    + exact (natrec ne u1 u2).
    + destruct eq.
      econstructor.
      3: eapply reductions_natrec_e ; eapply reductions_Ltrans ; eassumption.
      all: econstructor ; try eassumption.
      all: eapply escape_ty ; eassumption.
    + destruct eq.
      eapply reflect.
      2: now econstructor.
      now eapply completeness.
      econstructor ; try eassumption.
      all: eapply escape_ty ; eassumption.
Qed.

(*The fundamental theorem*)
Theorem soundness : forall l Γ Δ A t σ,
  [ Γ ⊢ t : A ] -> eval_subs l Δ Γ σ -> Weval l Δ A (subs_term σ t).
Proof.
  intros l Γ Δ A t σ Ht; revert l Δ σ; induction Ht; cbn in *; intros l Δ σ Hσ.
  - revert n A e; induction Hσ; intros n B e'; cbn in *.
    + exists 0.
      apply List.nth_error_In in e'; elim e'.
    + destruct n as [|n]; cbn in *.
      { injection e'; intros ->; rewrite lift_term_ELID.
        exists 0 ; intros.
        eapply eval_Ltrans.
        2: apply e; constructor.
        assumption.
      }
      { apply IHHσ; assumption. }
    + rewrite expand_term_SHFT.
      unshelve econstructor.
      * eapply WN.
        eapply IHHσ.
        eassumption.
      * intros.
        apply lift_eval with Γ; [repeat constructor|].
        eapply (WRed l Γ _ _ (IHHσ _ _ _) ) ; try eassumption.
  - exists 0.
    intros.
    unshelve econstructor.
    + exact (lam (subs_term (LIFT σ) t)).
    + exact 0.
    + eapply NF.NForms_lam.
    + econstructor ; try eassumption.
      3: now econstructor.
      all: eapply typing_lam.
      all: eapply typing_subs_compat ; try eassumption.
      all: econstructor.
      all: eapply eval_subs_ty ; eassumption.      
    + intros Ξ l'' e eε f' Ninfl x xε.
      assert (Weval l'' Ξ B (subs_term (CONS x (subs_compose subs_term (subs_of_lift e) σ)) t)).
      { unshelve eapply IHHt .
        constructor; [assumption|].
        clear - f f' eε Hσ.
        revert Γ σ Hσ; induction eε; intros Ξ σ Hσ; cbn.
        + eapply eval_subs_Ltrans.
          2: try eassumption.
          etransitivity ; eassumption.
        + constructor; apply IHeε; assumption.
        + remember (A :: Δ)%list as Ω.
          revert A Δ eε IHeε HeqΩ.
          induction Hσ; intros B Ξ eε IHeε HeqΩ; subst.
          { clear - t eε. destruct e.
            - exfalso ; now inversion t.
            - cbn.
              rewrite <- subs_lift_compose; do 2 constructor.
              inversion t ; subst.
              eapply typing_lift_compose ; eassumption.
            - exfalso.
              inversion t.
            }
            { constructor.
              + change (LIFT (subs_of_lift el)) with (@subs_of_lift term (ELLFT el)).
                rewrite subs_term_lift; eapply lift_eval; [constructor|]; try eassumption.
                eapply eval_Ltrans.
                2: eassumption.
                etransitivity ; eassumption.
              + eapply IHHσ; [eassumption|eassumption|reflexivity].
            }
            { constructor; apply IHeε.
              injection HeqΩ; intros -> ->; assumption.
            }
        }
        eapply Weval_reductions_expansion.
        econstructor.
        3: eapply reductions_step.
        4: now econstructor.
        3: eapply reduction_β.
        2,3:rewrite <- subs_term_lift, <- !subs_term_compose; cbn.
        3: eassumption.
        2: eapply Wescape_ty ; eassumption.
        econstructor.
        2: eapply escape_ty ; eassumption.
        eapply typing_lift_compat.
        2: eassumption.
        econstructor.
        eapply typing_subs_compat.
        eassumption.
        eapply typing_LIFT.
        eapply eval_subs_ty.
        eassumption.
  - (*regarder application.v dans logrel-coq, il faut faire la même chose pour
     donner un entier*)
    specialize (IHHt2 l Δ σ Hσ).
    specialize (IHHt1 l Δ σ Hσ).
    destruct IHHt2 as [Nu Ru].
    destruct IHHt1 as [Nt Rt].
    unshelve econstructor.
    + refine (max (max Nu Nt) (max _ _)).
      {unshelve refine (Max_Bar _ _ _).
      * assumption.
      * exact (max Nu Nt).
      * intros l' f Ninfl.
        unshelve refine ((Rt l' f _).(domN)).
        abstract (eapply AllInLCon_le ; try eassumption ;
                  now eapply Nat.max_lub_r).
      }
      { unshelve refine (Max_Bar _ _ _).
        * assumption.
        * unshelve refine (max (max Nu Nt) (Max_Bar _ _ _)).
          -- assumption.
          -- exact (max Nu Nt).
          -- intros l' f Ninfl.
             unshelve refine ((Rt l' f _).(domN)).
             eapply soundness_subproof.
             eassumption.
        * intros l' f Ninfl.
          unshelve refine (WN _ _ _ _ _).
          5: unshelve refine (codRed (Rt (Bar_lub l l' (max Nu Nt) f _) _ _)
                                _ l' _ (typing_ELID Δ) _ _ _ _).
          7 : eapply Ru.
          -- abstract (eapply AllInLCon_le ; try exact Ninfl ;
                       now eapply Nat.max_lub_l).
          -- now eapply Bar_lub_ub.
          -- eapply soundness_subproof.
             now eapply Bar_lub_AllInLCon.
          -- now eapply Bar_lub_smaller.
          -- abstract (eapply AllInLCon_le ; [ | exact Ninfl] ;
                       etransitivity ;
                       [ | now eapply Nat.max_lub_r] ;
               refine (Max_Bar_Bar_lub l (max Nu Nt) _ l' f _ _ _) ;
               eapply Bar_lub_AllInLCon).
          -- assumption.
          -- eapply AllInLCon_le ; try exact Ninfl.
             eapply Nat.max_lub_l.
             now eapply Nat.max_lub_l.
      }
    + cbn.
      intros.
      rewrite <- (lift_term_ELID (subs_term σ t)).
      assert (AllInLCon (max Nu Nt) l') as Ntuinf.
      { eapply AllInLCon_le ; try eassumption.
        now eapply Nat.max_lub_l.
      }
      assert (AllInLCon Nu l') as Nuinf.
      { eapply AllInLCon_le ; try exact Minfl.
        eapply Nat.max_lub_l.
        now eapply Nat.max_lub_l.
      }
      unshelve eapply ((Rt _ _ _ ).(codRed) _ _ _ (typing_ELID _) _).
      * unshelve eapply (Bar_lub l _ (max Nu Nt)).
        -- unshelve refine (Bar_lub l l' (max (max Nu Nt) _) f _).
           refine (Max_Bar l (Init.Nat.max Nu Nt) _).
           intros l'' f' Ninfl.
           refine (domN (Rt l'' f' _)).
           eapply soundness_subproof ; eassumption.
           abstract (eapply AllInLCon_le ; try exact Minfl ;
                  eapply Nat.max_le_compat ; auto ;
                  now eapply Nat.max_lub_l).
        -- now eapply Bar_lub_ub.
        -- eapply soundness_subproof0.
           now eapply Bar_lub_AllInLCon.
      * now eapply Bar_lub_ub.
      * eapply soundness_subproof.
        eapply Bar_lub_AllInLCon.
      * unshelve eapply (Bar_lub l l' (max (max Nu Nt) _) f _).
        -- refine (Max_Bar l (max Nu Nt) _).
           intros l'' f' Ninfl.
           refine (domN (Rt l'' f' _)).
           eapply soundness_subproof ; eassumption.
        -- eapply soundness_subproof2 ; eassumption.
      * now eapply Bar_lub_smaller.
      * eapply soundness_subproof1. 
      * eapply Ru.
        -- now eapply Bar_lub_ub.
        -- eapply AllInLCon_le.
           2: now eapply Bar_lub_AllInLCon.
           eapply Nat.max_lub_l.
           now eapply Nat.max_lub_l.
      * now eapply Bar_lub_smaller.
      * eapply AllInLCon_le ; try exact Minfl.
        etransitivity ; [ |now eapply Nat.max_lub_r].
        etransitivity ; [ |now eapply Nat.max_lub_r].
        etransitivity.
        2:{ unshelve eapply Max_Bar_Bar_lub.
            + exact l'.
            + assumption.
            + eapply soundness_subproof2.
              eassumption.
            + now eapply Bar_lub_ub.
            + now eapply Bar_lub_AllInLCon.
        }
        reflexivity.
  - exists 0.
    intros.
    econstructor.
    unshelve econstructor.
    all: econstructor.
  - exists 0.
    intros.
    eapply falseR.
    unshelve econstructor.
    all: econstructor.
  - specialize (IHHt _ _ _ Hσ) ; destruct IHHt as [Nt Redt].
    assert (H := fun l' f allinl => NatProp_canonicity_aux _ _ _ (Redt l' f allinl)).
    clear Redt.
    exists (max Nt (S (AlphaN l Δ _ Nt H))).
    intros.
    assert (AllInLCon Nt l') as Ntinf.
    {eapply AllInLCon_le ; try eassumption.
     now eapply Nat.max_lub_l.
    }
    assert (AllInLCon (S (AlphaN l Δ _ Nt H)) l') as AlphaNinf.
    {eapply AllInLCon_le ; [ | exact Minfl].
     now eapply Nat.max_lub_r.
    }
    remember (H l' f Ntinf) as HK.
    induction HK.
    + unshelve eapply eval_reductions_expansion.
      2: eassumption.
      unshelve edestruct (AlphaNinf n).
      2,3: eapply eval_alpha ; eassumption. 
      unfold AlphaN.
      eapply le_n_S.
      etransitivity.
      2:{ unshelve eapply Max_Bar_le.
          4: clear l' f Minfl Ntinf AlphaNinf t1 HeqHK ; intros.
          4: unshelve erewrite <- AlphaN_aux_Ltrans.
          6: reflexivity.
          all: eassumption.
      }
      rewrite <- HeqHK.
      reflexivity.
    + eapply eval_reductions_expansion.
      2: eapply boolneR ; try eassumption.
      destruct eq ; econstructor.
      all: try eassumption.
      now econstructor.
  - specialize (IHHt1 _ _ _ Hσ) ; destruct IHHt1 as [Nt1 Redt1].
    specialize (IHHt2 _ _ _ Hσ) ; destruct IHHt2 as [Nu2 Redu2].
    specialize (IHHt3 _ _ _ Hσ) ; destruct IHHt3 as [Nu3 Redu3].
    exists (max Nt1 (max Nu2 Nu3)).
    intros.
    assert (AllInLCon Nt1 l') as Hall1.
    { eapply AllInLCon_le ; [ | exact Minfl].
      now eapply Nat.max_lub_l.
    }
    assert (AllInLCon Nu2 l') as Hall2.
    { eapply AllInLCon_le ; [ | exact Minfl].
      eapply Nat.max_lub_l.
      now eapply Nat.max_lub_r.
    }
    assert (AllInLCon Nu3 l') as Hall3.
    { eapply AllInLCon_le ; [ | exact Minfl].
      eapply Nat.max_lub_r.
      now eapply Nat.max_lub_r.
    }
    specialize (Redt1 l' f Hall1).
    induction Redt1 as [ ? [] | ? [] | ].
    + unshelve eapply eval_reductions_expansion.
      * exact (subs_term σ u1).
      * eapply ty_reductions_trans.
      { unshelve econstructor.
        3: eapply reductions_cse_e ; eassumption.
        all: econstructor ; try eassumption.
        all: eapply typing_subs_compat ; try eapply eval_subs_ty. 
        all: eassumption.
      }
      unshelve econstructor.
      3: eapply reductions_step.
      3: eapply reduction_ι_tt.
      econstructor ; try eassumption.
      4: now econstructor.
      all: eapply typing_subs_compat ; try eapply eval_subs_ty.
      all: eassumption.
      * eapply Redu2 ; assumption.
    + unshelve eapply eval_reductions_expansion.
      * exact (subs_term σ u2).
      * eapply ty_reductions_trans.
      { unshelve econstructor.
        3: eapply reductions_cse_e ; eassumption.
        all: econstructor ; try eassumption.
        all: eapply typing_subs_compat ; try eapply eval_subs_ty. 
        all: eassumption.
      }
      unshelve econstructor.
      3: eapply reductions_step.
      3: eapply reduction_ι_ff.
      econstructor ; try eassumption.
      4: now econstructor.
      all: eapply typing_subs_compat ; try eapply eval_subs_ty.
      all: eassumption.
      * eapply Redu3 ; assumption.
    + destruct eq.
      eapply eval_reductions_expansion.
      econstructor.
      3: eapply reductions_cse_e ; eassumption.
      3: unshelve eapply (completeness A).(reflect).
      1-4: econstructor ; try eassumption.
      all : try eapply typing_subs_compat ; try eapply eval_subs_ty.
      all: try eassumption.
  - exists 0.
    intros.
    econstructor.
    unshelve econstructor.
    all: econstructor.
  - specialize (IHHt _ _ _ Hσ) ; destruct IHHt as [Nt Redt].
    exists Nt.
    intros.
    eapply succR.
    unshelve econstructor.
    3: now econstructor.
    1,2: econstructor.
    1,2: eapply typing_subs_compat ; try eapply eval_subs_ty.
    all: try eassumption.
    eapply Redt ; assumption.
  - specialize (IHHt1 _ _ _ Hσ) ; destruct IHHt1 as [Nt1 Redt1].
    specialize (IHHt2 _ _ _ Hσ) ; destruct IHHt2 as [Nu1 Redu1].
    specialize (IHHt3 _ _ _ Hσ) ; destruct IHHt3 as [Nu2 Redu2].
    unshelve eapply Weval_fusion.
    + exact (max Nt1 (max Nu1 Nu2)).
    + intros.
      assert (AllInLCon Nt1 l') as Hall1.
      { eapply AllInLCon_le ; [ | exact Minfl].
        now eapply Nat.max_lub_l.
      }
      assert (AllInLCon Nu1 l') as Hall2.
      { eapply AllInLCon_le ; [ | exact Minfl].
        eapply Nat.max_lub_l.
        now eapply Nat.max_lub_r.
      }
      assert (AllInLCon Nu2 l') as Hall3.
      { eapply AllInLCon_le ; [ | exact Minfl].
        eapply Nat.max_lub_r.
        now eapply Nat.max_lub_r.
      }
      specialize (Redt1 l' f Hall1).
      specialize (Redu1 l' f Hall2).
      specialize (Redu2 l' f Hall3).
      eapply Weval_natrec ; try eassumption.
Qed.      
