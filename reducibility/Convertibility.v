Require Import Setoid Morphisms Term LContexts.

(*Untyped reduction*)
Inductive reduction (l : wfLCon) : term -> term -> Type :=
| reduction_β : forall t u, reduction l (app (lam t) u) (subs_term (CONS u ESID) t)
| reduction_ι_tt : forall u1 u2, reduction l (cse tt u1 u2) u1
| reduction_ι_ff : forall u1 u2, reduction l (cse ff u1 u2) u2
| reduction_cse_e : forall t u1 u2 r, reduction l t r -> reduction l (cse t u1 u2) (cse r u1 u2)
| reduction_app_l : forall t u r, reduction l t r -> reduction l (app t u) (app r u)
| reduction_ι_z : forall u1 u2, reduction l (natrec z u1 u2) u1
| reduction_ι_succ : forall n u1 u2, reduction l (natrec (succ n) u1 u2) (app u2 (natrec n u1 u2))
| reduction_natrec_e : forall t u1 u2 r, reduction l t r -> reduction l (natrec t u1 u2) (natrec r u1 u2)
| reduction_alpha_subst :
  forall t r n,
    reduction l t r ->
    reduction l (alpha (nSucc n t)) (alpha (nSucc n r))
| reduction_alpha_red: forall n b, in_LCon (pi1 l) n b -> reduction l (alpha (nat_to_term n)) (bool_to_term b)
.

(*Conversion*)
Inductive convertible (l : wfLCon) : term -> term -> Prop :=
| convertible_β : forall t u, convertible l (app (lam t) u) (subs_term (CONS u ESID) t)
| convertible_lam : forall t r, convertible l t r -> convertible l (lam t) (lam r)
| convertible_app_l :forall t u r, convertible l t r -> convertible l (app t u) (app r u)
| convertible_app_r : forall t u r, convertible l u r -> convertible l (app t u) (app t r)
| convertible_η : forall t, convertible l (lam (app (lift_term (ELSHFT ELID) t) (var 0))) t
| convertible_cse_e : forall t u1 u2 r, convertible l t r -> convertible l (cse t u1 u2) (cse r u1 u2)
| convertible_cse_l : forall t u1 u2 r, convertible l u1 r -> convertible l (cse t u1 u2) (cse t r u2)
| convertible_cse_r : forall t u1 u2 r, convertible l u2 r -> convertible l (cse t u1 u2) (cse t u1 r)
| convertible_ι_tt : forall u1 u2, convertible l (cse tt u1 u2) u1
| convertible_ι_ff : forall u1 u2, convertible l (cse ff u1 u2) u2
| convertible_ι_z : forall u1 u2, convertible l (natrec z u1 u2) u1
| convertible_ι_succ : forall n u1 u2, convertible l (natrec (succ n) u1 u2) (app u2 (natrec n u1 u2))
| convertible_natrec_e : forall t u1 u2 r, convertible l t r -> convertible l (natrec t u1 u2) (natrec r u1 u2)
| convertible_natrec_l : forall t u1 u2 r, convertible l u1 r -> convertible l (natrec t u1 u2) (natrec t r u2)
| convertible_natrec_r : forall t u1 u2 r, convertible l u2 r -> convertible l (natrec t u1 u2) (natrec t u1 r)
| convertible_succ_subst : forall t r, convertible l t r -> convertible l (succ t) (succ r)
| convertible_alpha_subst : forall t r, convertible l t r -> convertible l (alpha t) (alpha r)
| convertible_alpha_red: forall n b, in_LCon (pi1 l) n b -> convertible l (alpha (nat_to_term n)) (bool_to_term b)
| convertible_refl : forall t, convertible l t t
| convertible_sym : forall t u, convertible l t u -> convertible l u t
| convertible_trans : forall t u r, convertible l t u -> convertible l u r -> convertible l t r
| convertible_split : forall t u n (ne : not_in_LCon (pi1 l) n),
    convertible (l ,,l (ne, true)) t u -> convertible (l ,,l (ne, false)) t u ->
    convertible l t u.

(*Many steps reduction*)
Inductive reductions (l : wfLCon) : term -> term -> Type :=
| reductions_refl : forall t, reductions l t t
| reductions_step : forall t u v, reduction l t u -> reductions l u v -> reductions l t v. 

Instance Equivalence_convertible {l : wfLCon} : RelationClasses.Equivalence (convertible l).
Proof.
split.
+ intro; apply convertible_refl.
+ intros x y; apply convertible_sym.
+ intros x y z; apply convertible_trans.
Qed.

(*Instance reductions_preorder {l} : PreOrder (reductions l).
Proof.
  split.
  - now econstructor.
  - intros * ; induction 1.
    1: easy.
    intros.
    econstructor ; try eassumption.
    now eapply IHreductions.    
Qed.*)

Lemma reductions_trans : forall l t r u,
    reductions l t r ->
    reductions l r u ->
    reductions l t u.
Proof.
  intros * ; induction 1.
  1: easy.
  intros.
  econstructor ; try eassumption.
  now eapply IHreductions.
Qed.               

Lemma convertible_step {l} : forall t u, reduction l t u -> convertible l t u.
Proof.
  intros t u.
  induction 1.
  all: try (econstructor ; eassumption).
  eapply convertible_alpha_subst.
  induction n.
  + cbn.
    eassumption.
  + cbn.
    now eapply convertible_succ_subst.
Qed.

Lemma reductions_app_l :
  forall (l : wfLCon) (t u r : term),
    reductions l t r -> reductions l (app t u) (app r u).
Proof.
  intros l t u r H ; induction H.
  - econstructor.
  - econstructor.
    2: eassumption.
    now apply reduction_app_l.
Qed.

Lemma reductions_cse_e :
  forall l t u1 u2 r, reductions l t r ->
                    reductions l (cse t u1 u2) (cse r u1 u2).
Proof.
  intros.
  induction H.
  - econstructor.
  - econstructor.
    + eapply reduction_cse_e.
      exact r.
    + assumption.
Qed.

Lemma reductions_natrec_e :
  forall l t u1 u2 r, reductions l t r ->
                      reductions l (natrec t u1 u2) (natrec r u1 u2).
Proof.
  intros.
  induction H.
  - econstructor.
  - econstructor.
    2: eassumption.
    now eapply reduction_natrec_e.
Qed.    

Lemma reductions_alpha_subst :
  forall l t r n,
    reductions l t r ->
    reductions l (alpha (nSucc n t)) (alpha (nSucc n r)).
Proof.
  intros.
  induction H.
  - econstructor.
  - econstructor.
    2: eassumption.
    now eapply reduction_alpha_subst.
Qed.    

Instance Proper_convertible_app {l : wfLCon} : Proper (convertible l ==> convertible l ==> convertible l) app.
Proof.
intros t1 t2 Ht u1 u2 Hu.
transitivity (app t1 u2).
+ apply convertible_app_r; assumption.
+ apply convertible_app_l; assumption.
Qed.


Instance Proper_convertible_lam {l : wfLCon} : Proper (convertible l ==> convertible l) lam.
Proof.
intros t1 t2 Ht.
apply convertible_lam; assumption.
Qed.

Lemma convertible_cse {l : wfLCon} : forall t u1 u2 r r1 r2,
  convertible l t r ->
  convertible l u1 r1 ->
  convertible l u2 r2 ->
  convertible l (cse t u1 u2) (cse r r1 r2).
Proof.
intros t u1 u2 r r1 r2 H H1 H2.
etransitivity; [apply convertible_cse_e; eassumption|].
etransitivity; [apply convertible_cse_l; eassumption|].
etransitivity; [apply convertible_cse_r; eassumption|].
reflexivity.
Qed.

Lemma subs_term_CONS_LFT_n : forall t u e n,
subs_term (LIFTn n (CONS (lift_term e u) ESID)) (lift_term (ELLFTn n (ELLFT e)) t) =
lift_term (ELLFTn n e) (subs_term (LIFTn n (CONS u ESID)) t).
Proof.
induction t; intros u e m; cbn.
+ revert n u e; induction m as [|m]; intros n u e; cbn.
  - rewrite !expand_term_CONS; destruct n as [|n]; reflexivity.
  - rewrite !expand_term_LIFT; destruct n as [|n]; [reflexivity|].
    rewrite IHm, <- !lift_term_compose; f_equal; cbn; f_equal.
    rewrite lift_compose_ELID_r; reflexivity.
+ f_equal; intuition.
+ f_equal; apply (IHt _ _ (S m)).
+ f_equal; intuition.
+ f_equal; intuition.
+ f_equal; intuition.
+ f_equal; intuition.
+ f_equal; intuition.
+ f_equal; intuition.
+ f_equal; intuition.
Qed.

Lemma subs_term_LFT_CONS : forall t u e,
  subs_term (CONS (lift_term e u) ESID) (lift_term (ELLFT e) t) =
  lift_term e (subs_term (CONS u ESID) t).
Proof.
induction t; intros u e; cbn.
- rewrite !expand_term_CONS; destruct n as [|n]; reflexivity.
- f_equal; intuition.
- f_equal.
  apply subs_term_CONS_LFT_n with (n := 1).
- f_equal; intuition.
- f_equal; intuition.
- f_equal; intuition.
- f_equal; intuition; apply subs_term_CONS_LFT_n with (n := 1).
- f_equal; intuition.
- f_equal; intuition.
- f_equal; intuition; apply subs_term_CONS_LFT_n with (n := 1).
Qed.

Lemma nSucc_lift : forall t n e,
    lift_term e (nSucc n t) = nSucc n (lift_term e t).
Proof.
  induction n.
  - reflexivity.
  - cbn.
    intros.
    f_equal.
    now eapply IHn.
Qed.

Lemma bool_to_term_lift : forall b e,
    lift_term e (bool_to_term b) = bool_to_term b.
Proof.
  induction b ; cbn ; reflexivity.
Qed.

(*Stability of reduction under weakening*)
Lemma reduction_lift {l : wfLCon} : forall t r e,
  reduction l t r -> reduction l (lift_term e t) (lift_term e r).
Proof.
intros t r e Hr; revert e; induction Hr; intros e; cbn in *.
+ evar (r : term); match goal with [ |- reduction _ _ ?t ] => replace t with r; [refine (reduction_β _ _ _)|] end;
  unfold r; clear r.
  now apply subs_term_LFT_CONS.
+ refine (reduction_ι_tt _ _ _).
+ refine (reduction_ι_ff _ _ _).
+ apply reduction_cse_e; intuition.
+ apply reduction_app_l; intuition.
+ refine (reduction_ι_z _ _ _).
+ refine (reduction_ι_succ _ _ _ _).
+ apply reduction_natrec_e; intuition.
+ cbn.
  unshelve (do 2 erewrite nSucc_lift).
  apply reduction_alpha_subst; intuition.
+ unfold nat_to_term.
  erewrite (nSucc_lift).
  erewrite bool_to_term_lift.
  now econstructor.
Qed.

Lemma reductions_lift {l : wfLCon} : forall t r e,
    reductions l t r -> reductions l (lift_term e t) (lift_term e r).
Proof.
  intros.
  induction H.
  - econstructor.
  - eapply reductions_step ; try eassumption.
    now eapply reduction_lift.
Qed.

(*Stability of conversion under weakening*)
Lemma convertible_lift {l} : forall t r e,
  convertible l t r -> convertible l (lift_term e t) (lift_term e r).
Proof.
  intros t r e H.
  revert e.
  induction H ; intros.
  19: reflexivity.
  19: symmetry; now eapply IHconvertible.
  19: etransitivity; auto.
  all: try now (cbn ; econstructor).
+ evar (r : term); match goal with [ |- convertible _ _ ?t ] => replace t with r; [refine (convertible_β _ _ _)|] end;
  unfold r; clear r.
  apply subs_term_LFT_CONS.
+ cbn.
  rewrite <- lift_term_compose; cbn.
  rewrite lift_compose_ELID_r.
  replace (lift_term (ELSHFT e) t) with (lift_term (ELSHFT ELID) (lift_term e t)).
  - apply convertible_η.
  - rewrite <- lift_term_compose; reflexivity.
+ unfold nat_to_term.
  cbn.
  erewrite nSucc_lift, bool_to_term_lift.
  now econstructor.
Qed.

(*Determinism of reduction*)
Lemma reduction_det : forall l t u v,
    reduction l t u -> reduction l t v -> u = v.
Proof.
  intros * redu.
  revert v.
  induction redu ; intros v redv.
  all : try now (inversion redv ; subst ; [reflexivity | now inversion H3]).
  - inversion redv ; subst.
    + reflexivity.
    + now inversion H2.
  - inversion redv ; subst.
    + now inversion redu.
    + now inversion redu.
    + now rewrite (IHredu r0 H3).
  - inversion redv ; subst.
    + now inversion redu.
    + now rewrite (IHredu r0 H2).
  - inversion redv ; subst.
    + now inversion redu.
    + now inversion redu.
    + now rewrite (IHredu r0 H3).
  - inversion redv ; subst.
    destruct (Compare_dec.le_ge_dec n n0).
    + assert (q:= @nSucc_inj_nSucc _ _ _ _  (eq_sym H) l0).
      subst.
      induction l0.
      * cbn in *.
        try erewrite PeanoNat.Nat.sub_diag in * ; cbn in *.
        erewrite IHredu ; try eassumption.
        reflexivity.
      * clear IHl0.
        erewrite PeanoNat.Nat.sub_succ_l in redu ; try eassumption.
        cbn in *.
        now inversion redu.
    + assert (q:= @nSucc_inj_nSucc _ _ _ _  H g).
      subst.
      unfold ge in g.
      induction g.
      * cbn in *.
        try erewrite PeanoNat.Nat.sub_diag in * ; cbn in *.
        erewrite IHredu ; try eassumption.
        reflexivity.
      * clear IHg.
        erewrite PeanoNat.Nat.sub_succ_l in H0 ; try eassumption.
        cbn in *.
        now inversion H0.
    + eapply nat_to_term_nSucc in H ; subst.
      exfalso ; clear IHredu redv H0 b.
      induction (n0 - n) ; cbn in * ; now inversion redu.
  - inversion redv ; subst.
    + symmetry in H ; eapply nat_to_term_nSucc in H ; subst.
      exfalso; clear redv i b.
      induction (n - n0) ; cbn in * ; now inversion H0.
    + eapply nattoterminj in H ; subst.
      erewrite uniqueinLCon ; try eassumption.
      reflexivity.
      now destruct l.
Qed.

Lemma reduction_alpha_succ : forall l t r,
    reduction l (alpha t) (alpha r) ->
    reduction l (alpha (succ t)) (alpha (succ r)).
Proof.
  intros.
  inversion H ; subst.
  1: now eapply (reduction_alpha_subst l t0 r0 (S n)).
  exfalso ; induction b ; now inversion H1.
Qed.

Lemma reductions_alpha_succ : forall l t r,
    reductions l (alpha t) (alpha r) ->
    reductions l (alpha (succ t)) (alpha (succ r)).
Proof.
  intros * H.
  remember (alpha t) ; remember (alpha r).
  revert t r Heqt0 Heqt1.
  induction H ; intros. 
  - subst.
    inversion Heqt1 ; subst.
    econstructor.
  - subst.
    inversion r.
    + subst.
      specialize (IHreductions _ _ (eq_refl _) (eq_refl _)).
      econstructor ; try eassumption.
      eapply reduction_alpha_succ.
      assumption.
    + subst.
      exfalso.
      * inversion H ; subst.
        -- induction b ; now inversion H3.
        -- induction b ; now inversion H0.
Qed.

(*Stability of reduction under the extension of the list l*)
Lemma reduction_Ltrans: forall l l' t r (f : l' ≤ε l),
    reduction l t r -> reduction l' t r.
Proof.
  intros.
  induction H.
  all: try now econstructor.
  econstructor.
  now eapply f.
Qed.

Lemma reductions_Ltrans: forall l l' t r (f : l' ≤ε l),
    reductions l t r -> reductions l' t r.
Proof.
  intros.
  induction H.
  - now econstructor.
  - econstructor.
    2:eassumption.
    eapply reduction_Ltrans ; eassumption.
Qed.

(*Stability of conversion under the extension of the list l*)
Lemma convertible_Ltrans: forall l l' t r (f : l' ≤ε l),
    convertible l t r -> convertible l' t r.
Proof.
  intros.
  revert l' f.
  induction H ; intros.
  all: try (econstructor ; now eauto).
  destruct (decidInLCon l' n) as [ [] | ].
  - eapply IHconvertible1.
    now eapply LCon_le_in_LCon.
  - eapply IHconvertible2.
    now eapply LCon_le_in_LCon.
  - unshelve eapply convertible_split ; try eassumption.
    + eapply IHconvertible1 ; try eassumption.
      now eapply LCon_le_up.
    + eapply IHconvertible2 ; try eassumption.
      now eapply LCon_le_up.
Qed.


(*Definition of normal forms*)

Module NF.

Inductive shape := nf | ne.

Inductive NForms : shape -> term -> Type :=
| NForms_ne : forall t, NForms ne t -> NForms nf t
| NForms_var : forall n, NForms ne (var n)
| NForms_lam : forall t, NForms nf (lam t)
| NForms_app : forall t u,
    NForms ne t ->
    NForms ne (app t u)
| NForms_tt : NForms nf tt
| NForms_ff : NForms nf ff
| NForms_cse : forall t u1 u2,
  NForms ne t ->
  NForms ne (cse t u1 u2)
| NForms_z : NForms nf z
| NForms_succ : forall n, NForms nf (succ n)
| NForms_natrec : forall t u1 u2,
    NForms ne t ->
    NForms ne (natrec t u1 u2)
| NForms_alpha : forall t n, NForms ne t -> NForms ne (alpha (nSucc n t))
.

End NF.

(*Stability of normal forms under weakenings*)
Lemma NF_lift_compat : forall s t el,
  NF.NForms s t -> NF.NForms s (lift_term el t).
Proof.
  intros s t el Ht.
  induction Ht; cbn.
  - econstructor.
    eapply IHHt ; eassumption.
  - econstructor.
  - eapply NF.NForms_lam.
  - eapply NF.NForms_app.
    eapply IHHt ; try eassumption.
  - eapply NF.NForms_tt.
  - eapply NF.NForms_ff.
  - eapply NF.NForms_cse.
    eapply IHHt ; eassumption.
  - eapply NF.NForms_z.
  - eapply NF.NForms_succ.
  - eapply NF.NForms_natrec.
    eapply IHHt ; eassumption.
  - erewrite nSucc_lift.
    eapply NF.NForms_alpha.
    eapply IHHt ; eassumption.
Qed.



(*Normal forms and reduction*)
Lemma reduction_whnf : forall l t u,
    NF.NForms NF.nf t -> reduction l t u -> False.
Proof.
  intros.
  revert H.
  induction H0 ; intros.
  all: inversion H ; subst.
  all: try now (inversion H0 ; inversion H3).
  all: try now (inversion H1 ; subst ; eapply IHreduction ; econstructor).
  - inversion H1.
    destruct (Compare_dec.le_ge_dec n n0).
    + assert (q:= @nSucc_inj_nSucc n n0 _ _ (eq_sym H2) l0).
      subst.
      destruct (n0 - n) ; eapply IHreduction ; cbn in *.
      * now econstructor.
      * now eapply NF.NForms_succ.
    + assert (q:= @nSucc_inj_nSucc _ _ _ _ H2 g).
      subst.
      destruct (n - n0).
      * eapply IHreduction ; cbn in *.
        now econstructor.
      * inversion H3.
  - inversion H0.
    assert (q := @nat_to_term_nSucc _ _ _ (eq_sym H1)).
    subst.
    destruct (n - n0) ; now inversion H2.
Qed.


Lemma reduction_reductions_whnf : forall l t r u,
    NF.NForms NF.nf u -> reductions l t u -> reduction l t r -> reductions l r u.
Proof.
  intros * nfu redtu redtr.
  induction redtu.
  - exfalso.
    eapply reduction_whnf ; eassumption.
  - assert (r = u).
    eapply reduction_det ; eassumption.
    subst.
    assumption.
Qed.


Lemma reductions_whnf : forall l t r u,
    NF.NForms NF.nf u -> reductions l t u -> reductions l t r -> reductions l r u.
Proof.
  intros * nfu redtu redtr.
  revert u nfu redtu.
  induction redtr ; intros.
  - assumption.
  - eapply IHredtr.
    assumption.
    eapply reduction_reductions_whnf ; eassumption.
Qed.   
