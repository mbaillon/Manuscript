Require List.
Require Import CMorphisms PeanoNat Lia.

Require Import Term LContexts Convertibility Typing.

Set Primitive Projections.
Set Implicit Arguments.

Inductive NatProp {l : wfLCon} {Γ : list type} : term -> Set :=
| zeroR  {n}
  (eq: ty_reductions l Γ nat n z): NatProp n
| succR {n}
    (nf : term)
    (eq: ty_reductions l Γ nat n (succ nf)):
    NatProp nf ->
    NatProp n
| natneR {n} (ne : term)
    (eq : ty_reductions l Γ nat n ne): NF.NForms NF.ne ne -> NatProp n
| natsplit {n t} {ne : not_in_LCon (pi1 l) n} :
  @NatProp (l ,,l (ne, true)) Γ t ->
  @NatProp (l ,,l (ne, false)) Γ t ->
  @NatProp l Γ t.

Inductive BoolProp {l : wfLCon} {Γ : list type} : term -> Set :=
| trueR  {n}
  (eq: ty_reductions l Γ bool n tt): BoolProp n
| falseR  {n}
  (eq: ty_reductions l Γ bool n ff): BoolProp n
| boolneR {n} (ne : term)
    (eq : ty_reductions l Γ bool n ne): NF.NForms NF.ne ne -> BoolProp n
| boolsplit {n t} {ne : not_in_LCon (pi1 l) n} :
  @BoolProp (l ,,l (ne, true)) Γ t ->
  @BoolProp (l ,,l (ne, false)) Γ t ->
  @BoolProp l Γ t.


Unset Implicit Arguments.
Inductive reified l Γ A t : Type :=
| Build_Reified 
    (reify_trm : term)
    (reify_typ : NF.NForms NF.nf reify_trm)
    (reify_cvn : ty_reductions l Γ A t reify_trm)
| Reified_split {n} {ne : not_in_LCon (pi1 l) n} :
  reified (l ,,l (ne, true)) Γ A t ->
  reified (l ,,l (ne, false)) Γ A t ->
  reified l Γ A t.

Lemma reified_ty  l Γ A t :
  reified  l Γ A t -> [ Γ ⊢ t : A ].
Proof.
  induction 1.
  - now destruct reify_cvn.
  - assumption.
Qed.

    
Record ArrProp
    {l : wfLCon}
    {Γ : list type} {A B : type} {t : term}
    {Pa Pb :  wfLCon -> list type -> term -> Type}
  : Type := {
    rei : reified l Γ (arr A B) t ;
    redfun : forall Δ l' (e : lift) (eε : typing_lift e Δ Γ) (f : l' ≤ε l),
    forall (x : term) (xε : Pa l' Δ x),
     Pb l' Δ (app (lift_term e t) x) ;
  }.

Fixpoint eval (l : wfLCon) (Γ : list type) (A : type) (t : term) {struct A} : Type :=
match A with
| bool => @BoolProp l Γ t
| nat => @NatProp l Γ t
| arr A B => @ArrProp  l Γ A B t
               (fun l delta a => eval l delta A a)
               (fun l delta b => eval l delta B b)
end.

Notation realizes := eval.

Lemma eval_Ltrans (l l' : wfLCon) (Γ : list type) (A : type) (t : term) :
  l' ≤ε l -> eval l Γ A t -> eval l' Γ A t.
Proof.
  revert l' Γ t.
  induction A ; intros l' Γ t f H.
  - destruct H.
    unshelve econstructor.
    { clear redfun0 IHA1 IHA2.
      revert l' f.
      induction rei0 ; intros.
      + econstructor.
        eassumption.
        eapply ty_reductions_Ltrans ; eassumption.
      + cbn in *.
        destruct (decidInLCon l' n) as [ [] | ].
        * apply IHrei0_1.
          eapply LCon_le_in_LCon ; eassumption.
        * apply IHrei0_2.
          eapply LCon_le_in_LCon ; eassumption.
        * unshelve eapply Reified_split.
          2: eassumption.
          -- eapply IHrei0_1.
             eapply LCon_le_up ; eassumption.
          -- eapply IHrei0_2.
             eapply LCon_le_up ; eassumption.             
    }
    intros Δ l'' e eε f' x xε.
    eapply redfun0 ; try eassumption.
    etransitivity ; eassumption.
  - revert l' f.
    induction H ; intros.
    + unshelve econstructor.
      eapply ty_reductions_Ltrans ; try eassumption.
    + eapply falseR.
      eapply ty_reductions_Ltrans ; eassumption.
    + eapply boolneR ; try eassumption.
      eapply ty_reductions_Ltrans ; eassumption.
    + destruct (decidInLCon l' n).
      * destruct s.
        -- assert (l' ≤ε l,,l (ne, true)) by
             (eapply LCon_le_in_LCon ; eassumption).
           now apply IHBoolProp1.
        -- assert (l' ≤ε l,,l (ne, false)) by
             (eapply LCon_le_in_LCon ; eassumption).
           now apply IHBoolProp2.
      * unshelve eapply boolsplit.
        3: {apply IHBoolProp1.
            eapply LCon_le_up ; eassumption.
        }
        assumption.
        apply IHBoolProp2.
        eapply LCon_le_up ; eassumption.
  - revert l' f.
    induction H ; intros.
    + unshelve econstructor.
      eapply ty_reductions_Ltrans ; eassumption.
    + eapply succR.
      eapply ty_reductions_Ltrans ; eassumption.
      eapply IHNatProp.
      assumption.
    + eapply natneR ; try eassumption.
      eapply ty_reductions_Ltrans ; eassumption.
    + destruct (decidInLCon l' n).
      * destruct s.
        -- assert (l' ≤ε l,,l (ne, true)) by
             (eapply LCon_le_in_LCon ; eassumption).
           now apply IHNatProp1.
        -- assert (l' ≤ε l,,l (ne, false)) by
             (eapply LCon_le_in_LCon ; eassumption).
           now apply IHNatProp2.
      * unshelve eapply natsplit.
        3: {apply IHNatProp1.
            eapply LCon_le_up ; eassumption.
        }
        assumption.
        apply IHNatProp2.
        eapply LCon_le_up ; eassumption.
Qed.  

Lemma eval_split (l : wfLCon) (Γ : list type) (A : type) (t : term) :
  forall n (ne : not_in_LCon (pi1 l) n),
    eval (l ,,l (ne, true)) Γ A t ->
    eval (l ,,l (ne, false)) Γ A t ->
    eval l Γ A t.
Proof.
  intros n ne.
  revert l ne Γ t.
  induction A.
  all: intros l ne Γ t Ht Hf.
  2: eapply boolsplit ; eassumption.
  2: eapply natsplit ; eassumption.
  unshelve econstructor.
  { destruct Ht, Hf.
    eapply Reified_split ; eassumption.
  }
  intros Δ l' e eε f x xε.
  destruct (decidInLCon l' n) as [[] | ].
  - eapply Ht ; try eassumption.
    eapply LCon_le_in_LCon ; assumption.
  - eapply Hf ; try eassumption.
    eapply LCon_le_in_LCon ; assumption.
  - unshelve eapply IHA2 ; try assumption.
    + eapply Ht ; try eassumption.
      unshelve eapply LCon_le_up ; try assumption.
      eapply eval_Ltrans ; try eassumption.
      now eapply LCon_le_step.
    + eapply Hf ; try eassumption.
      unshelve eapply LCon_le_up ; try assumption.
      eapply eval_Ltrans ; try eassumption.
      now eapply LCon_le_step.
Qed.

Lemma lift_eval : forall l Γ Δ A t e, [ Δ ⊢lift e : Γ ] ->
  eval l Γ A t -> eval l Δ A (lift_term e t).
Proof.
  intros l Γ Δ A; revert Γ Δ; induction A; intros Γ Δ t e He Ht; cbn in *.
  - unshelve econstructor.
    { destruct Ht.
      clear redfun0 IHA1 IHA2.
      induction rei0.
      + econstructor.
        2: eapply ty_reductions_lift ; try eassumption.
        eapply NF_lift_compat ; try eassumption.
      + eapply Reified_split.
        * now eapply IHrei0_1.
        * now eapply IHrei0_2.
    }
    intros Ξ l' e' He' f x xε.
   rewrite <- lift_term_compose.
   apply Ht; [|assumption | assumption].
   apply typing_lift_compose with Δ; assumption.
 - induction Ht.
   + eapply trueR.
     eapply ty_reductions_lift in eq ; try eassumption.
   + eapply falseR.
     eapply ty_reductions_lift in eq ; try eassumption.
   + unshelve eapply boolneR.
     * exact (lift_term e ne).
     * eapply ty_reductions_lift ; try eassumption.
     * eapply NF_lift_compat ; eassumption.
   + specialize (IHHt1 He) ; specialize (IHHt2 He).
     eapply boolsplit ; eassumption.
 - induction Ht.
   + eapply zeroR.
     eapply ty_reductions_lift in eq ; try eassumption.
   + eapply succR.
     eapply ty_reductions_lift in eq ; try eassumption.
     now apply IHHt.
   + unshelve eapply natneR.
     * exact (lift_term e ne).
     * eapply ty_reductions_lift ; try eassumption.
     * eapply NF_lift_compat ; eassumption.
   + specialize (IHHt1 He) ; specialize (IHHt2 He).
     eapply natsplit ; eassumption.
Qed.

(* Definition realizes Γ A t := forall Δ (e : lift) (eε : typing_lift e Δ Γ), eval Δ A (lift_term e t). *)

(*Lemma eval_ntr_convertible_compat : forall Γ A t r, convertible t r -> eval_ntr Γ A t -> eval_ntr Γ A r.
Proof.
intros Γ A t r Hr Ht.
destruct Ht as [s Hs H]; exists s.
- assumption.
- apply convertible_trans with t; [|assumption].
  apply convertible_sym; assumption.
Qed.
 *)

Lemma escape_ty : forall l Γ A t, eval l Γ A t -> [ Γ ⊢ t : A ] .
Proof.
  intros l Γ A; revert l Γ.
  induction A as [A IHA B IHB| |]; intros l Γ t Ht; cbn in *.
  - destruct Ht.
    eapply reified_ty ; eassumption.
  - induction Ht.
    + now destruct eq.
    + now destruct eq.
    + now destruct eq.
    + assumption.
  - induction Ht ; try now destruct eq.
    assumption.
Qed.

Lemma eval_reductions_expansion:
  forall l Γ A t r, ty_reductions l Γ A t r -> eval l Γ A r -> eval l Γ A t.
Proof.
  intros l Γ A; revert l Γ; induction A as [A IHA B IHB| |]; intros l Γ t r Ht Hr; cbn in *.
  - econstructor.
    { destruct Hr.
      clear IHA IHB redfun0.
      induction rei0.
      + econstructor.
        eassumption.
        eapply ty_reductions_trans ; eassumption.
      + eapply Reified_split ; [eapply IHrei0_1 | eapply IHrei0_2].
        all: eapply ty_reductions_Ltrans ; try eassumption.
        all: now eapply LCon_le_step.
    }
    cbn.
    intros Δ l' e He f x xε.
    apply IHB with (app (lift_term e r) x).
    + eapply ty_reductions_app_l.
      2: eapply escape_ty ; eassumption.
      eapply ty_reductions_lift ; try eassumption.
      eapply ty_reductions_Ltrans ; eassumption.
    + apply Hr, xε; assumption.
  - induction Hr.
    + econstructor.
      eapply ty_reductions_trans ; eassumption.
    + eapply falseR.
      eapply ty_reductions_trans ; eassumption.
    + eapply boolneR.
      eapply ty_reductions_trans ; eassumption.
      assumption.
    + eapply boolsplit.
      * eapply IHHr1.
        eapply ty_reductions_Ltrans ; try eassumption.
        now eapply LCon_le_step.
      * eapply IHHr2.
        eapply ty_reductions_Ltrans ; try eassumption.
        now eapply LCon_le_step.
  - revert t Ht.
    induction Hr ; intros.
    + econstructor.
      eapply ty_reductions_trans ; eassumption.
    + eapply succR ; try eassumption.
      eapply ty_reductions_trans ; eassumption.
    + eapply natneR.
      eapply ty_reductions_trans ; eassumption.
      assumption.
    + eapply natsplit.
      * eapply IHHr1.
        eapply ty_reductions_Ltrans ; try eassumption.
        now eapply LCon_le_step.
      * eapply IHHr2.
        eapply ty_reductions_Ltrans ; try eassumption.
        now eapply LCon_le_step.
Qed.        
      
Lemma eval_reductions_compat :
  forall l Γ A t r, ty_reductions l Γ A t r -> eval l Γ A t -> eval l Γ A r.
Proof.
  intros l Γ A; revert l Γ; induction A as [A IHA B IHB| |]; intros l Γ t r Hr Ht; cbn in *.
  - unshelve econstructor.
    { destruct Ht ; clear IHA IHB redfun0.
      induction rei0.
      + eapply Build_Reified.
        eassumption.
        eapply ty_reductions_whnf ; try eassumption.
      + eapply Reified_split ; [eapply IHrei0_1 | eapply IHrei0_2].
        all: eapply ty_reductions_Ltrans ; try eassumption.
        all: now eapply LCon_le_step.
    }
    intros Δ l' e He f x xε.
    apply IHB with (app (lift_term e t) x).
    + eapply ty_reductions_app_l ; try eassumption.
      eapply ty_reductions_lift ; try eassumption.
      eapply ty_reductions_Ltrans ; eassumption.
      eapply escape_ty ; try eassumption.
    + apply Ht, xε; assumption.
  - induction Ht.
    + eapply trueR.
      unshelve eapply ty_reductions_whnf ; try eassumption.
      now eapply NF.NForms_tt.
    + eapply falseR.
      unshelve eapply ty_reductions_whnf ; try eassumption.
      now eapply NF.NForms_ff.
    + eapply boolneR ; try eassumption.
      unshelve eapply ty_reductions_whnf.
      3,4: eassumption.
      constructor ; eassumption.
    + eapply boolsplit.
      * eapply IHHt1.
        eapply ty_reductions_Ltrans ; try eassumption.
        now eapply LCon_le_step.
      * eapply IHHt2.
        eapply ty_reductions_Ltrans ; try eassumption.
        now eapply LCon_le_step.
  -  induction Ht.
    + eapply zeroR.
      unshelve eapply ty_reductions_whnf ; try eassumption.
      now eapply NF.NForms_z.
    + eapply succR with nf ; try eassumption.
      assert (forall t x y, reductions l x (succ t) -> reduction l x y -> reductions l y (succ t)).
      { clear r n Hr nf eq Ht IHHt.
        intros * redxt redxy.
        revert y redxy.
        remember (succ t).
        induction redxt ; intros.
        + subst.
          now inversion redxy.
        + subst.
          eapply reduction_det in redxy.
          2: exact r.
          subst.
          eapply reductions_trans ; try eassumption.
          now econstructor.
      }
      assert (forall t x y, reductions l x (succ t) -> reductions l x y -> reductions l y (succ t)).
      { clear r n Hr nf eq Ht IHHt.
        intros * redxt redxy.
        revert t redxt.
        induction redxy ; intros.
        - assumption.
        - eapply IHredxy.
          eapply H ; eassumption.
      }
      assert (forall t x y, ty_reductions l Γ nat x (succ t) ->
                            ty_reductions l Γ nat x y ->
                            ty_reductions l Γ nat y (succ t)).
      { intros * [] [].
        unshelve econstructor ; try eassumption.
        eapply H0 ; eassumption.
      }
      unshelve eapply H1 with n ; assumption.
    + eapply natneR.
      2: eassumption.
      unshelve eapply ty_reductions_whnf.
      4: eassumption.
      econstructor; eassumption.
      assumption.
    + eapply natsplit.
      * eapply IHHt1.
        eapply ty_reductions_Ltrans ; try eassumption.
        now eapply LCon_le_step.
      * eapply IHHt2.
        eapply ty_reductions_Ltrans ; try eassumption.
        now eapply LCon_le_step.
Qed.

(*Instance Proper_eval {l} : Proper (eq ==> eq ==> reductions l ==> iffT) (eval l).
Proof.
intros Γ Γ' <- A A' <- t t' Ht; split; intros; eapply eval_reductions_compat; try eassumption.
symmetry; eassumption.
Qed.*)

Inductive eval_subs (l : wfLCon) : list type -> list type -> subs term -> Type :=
| eval_subs_ESID :
  forall Γ e,
    [Γ ⊢lift e : nil] ->
    eval_subs l Γ nil (subs_of_lift e)
| eval_subs_CONS : forall Γ Δ A t σ,
    realizes l Γ A t ->
    eval_subs l Γ Δ σ ->
    eval_subs l Γ (cons A Δ) (CONS t σ)
| eval_subs_SHFT : forall Γ Δ A σ,
    eval_subs l Γ Δ σ ->
    eval_subs l (cons A Γ) Δ (SHFT σ)
(* | eval_subs_LIFT : forall Γ Δ A σ, eval_subs Γ Δ σ -> eval_subs (cons A Γ) (cons A Δ) (LIFT σ) *)
.

Lemma eval_subs_Ltrans l l' Δ Γ σ :
  l' ≤ε l -> eval_subs l Δ Γ σ -> eval_subs l' Δ Γ σ.
Proof.
  intros f. induction 1.
  - econstructor ; eassumption.
  - econstructor ; try eassumption.
    eapply eval_Ltrans ; eassumption.
  - now econstructor.
Qed.

Lemma eval_subs_ty l Δ Γ σ : eval_subs l Δ Γ σ -> [Δ ⊢subs σ : Γ].
Proof.
  induction 1.
  - eapply typing_lift_subs ; eassumption.
  - econstructor.
    assumption.
    eapply escape_ty ; eassumption.
  - econstructor.
    assumption.
Qed.
  
Set Implicit Arguments.




Record interp (A : type) := {
  reflect : forall l Γ t, NF.NForms NF.ne t -> [ Γ ⊢ t : A ] -> eval l Γ A t;
  reify : forall l Γ t, realizes l Γ A t -> reified l Γ A t;
}.

Unset Implicit Arguments.

Lemma completeness : forall A, interp A.
Proof.
  induction A; split; cbn in *.
  - intros l Γ t Ht1 Ht2.
    unshelve econstructor.
    { econstructor.
      econstructor. eassumption.
      econstructor.
      3: now econstructor.
      all: eassumption.
    } 
    intros Δ l' e eε f x xε.
    assert (r := (IHA1.(reify) _ _ _ xε)).
    clear xε.
    apply IHA2.(reflect).
    2:{ eapply typing_app.
        * eapply typing_lift_compat ; eassumption.
        * clear f IHA1 IHA2.
          induction r.
          -- now destruct reify_cvn.
          -- now eapply IHr1.
    }
    eapply NF.NForms_app.
    eapply NF_lift_compat ; eassumption.
  - intros l Γ t [ty_t Ht].
    assumption.
  -  intros l Γ t Ht1 Ht2.
     eapply boolneR ; try eassumption.
     econstructor.
     3: now econstructor.
     all : try eassumption.
     all : eapply Ntyping_typing ; eassumption.
  - intros l Γ t Ht.
    induction Ht.
    1-3: econstructor ; try eassumption.
    + eapply NF.NForms_tt.
    + eapply NF.NForms_ff.
    + now constructor.
    + eapply Reified_split ; eassumption.
  - intros l Γ t Ht1 Ht2.
     eapply natneR ; try eassumption.
     econstructor.
     3: now econstructor.
     all : eassumption.
  - intros l Γ t Ht.
    induction Ht.
    1-3: econstructor ; try eassumption.
    + eapply NF.NForms_z.
    + eapply NF.NForms_succ.
    + now constructor.
    + eapply Reified_split ; eassumption.
Qed.

Lemma subs_term_CONS_SHFT_LIFT_n : forall t σ n,
  subs_term (LIFTn n (CONS (var 0) (SHFT σ))) t = subs_term (LIFTn (S n) σ) t.
Proof.
induction t; intros σ m; cbn in *; try (solve [f_equal; intuition eauto]).
+ revert n σ; induction m as [|m]; intros n σ; cbn.
  - rewrite expand_term_CONS, expand_term_LIFT.
    destruct n as [|n]; [reflexivity|].
    rewrite expand_term_SHFT; f_equal.
  - rewrite !expand_term_LIFT; destruct n as [|n]; [reflexivity|]; cbn.
    f_equal; apply IHm.
+ f_equal; apply (IHt _ (S m)).
Qed.

Lemma subs_term_CONS_SHFT_LIFT : forall t σ,
  subs_term (CONS (var 0) (SHFT σ)) t = subs_term (LIFT σ) t.
Proof.
intros t σ; apply subs_term_CONS_SHFT_LIFT_n with (n := 0).
Qed.

Lemma eval_alpha : forall l Γ n b,
    in_LCon (pi1 l) n b ->
    realizes l Γ bool (alpha (nat_to_term n)).
Proof.
  intros.
  induction b.
  all: eapply eval_reductions_expansion.
  1,3: econstructor.
  3,6: eapply reductions_step ; try (now econstructor).
  3: eapply (reduction_alpha_red _ _ true) ; try eassumption.
  3: eapply (reduction_alpha_red _ _ false) ; try eassumption.
  1-5:cbn ; econstructor.
  4: eapply falseR.
  3,4: econstructor ; try reflexivity.
  all: clear H ; try now econstructor.
  all: induction n ; try now econstructor ; assumption.
Qed.

Inductive canonicity_aux l Γ : term -> Type :=
| eta : forall n t, ty_reductions l Γ bool t (alpha (nat_to_term n)) ->
                    canonicity_aux l Γ t
| canone {n} (ne : term)
    (eq : ty_reductions l Γ bool n ne):
  NF.NForms NF.ne ne  -> canonicity_aux l Γ n
| beta : forall t n (ne : not_in_LCon (pi1 l) n),
    canonicity_aux (l ,,l (ne, true)) Γ t ->
    canonicity_aux (l ,,l (ne, false)) Γ t ->
    canonicity_aux l Γ t.


Inductive alphareduc : term -> Type :=
| alphane n (ne : term):
  NF.NForms NF.ne ne -> alphareduc (alpha (nSucc n ne)).

Lemma alphareduction_alphareduc l :
  forall t ne,
    NF.NForms NF.ne ne ->
    reduction l (alpha t) ne ->
    alphareduc ne.
Proof.
  intros.
  inversion H0.
  - subst.
    inversion H ; subst.
    econstructor.
    assumption.
  - exfalso.
    induction b ; subst ; inversion H.
Qed.

Lemma alphareductions_alphareduc l :
  forall t ne,
    NF.NForms NF.ne ne ->
    reductions l (alpha t) ne ->
    alphareduc ne.
Proof.
  intros.
  remember (alpha t).
  revert t Heqt0.
  induction H0 ; intros.
  - subst.
    inversion H ; subst.
    econstructor.
    assumption.
  - subst.
    inversion r ; subst.
    + eapply IHreductions.
      assumption.
      reflexivity.
    + exfalso.
      induction b ; cbn in * ; inversion H0 ; subst.
      all: try now inversion H.
Qed.        
    
Lemma NatProp_canonicity_aux : forall l Γ t,
    @NatProp l Γ t -> canonicity_aux l Γ (alpha t).
Proof.
  intros * ; induction 1.
  - destruct eq.
    unshelve econstructor.
    + exact 0.
    + unshelve econstructor.
      3: eapply (reductions_alpha_subst _ _ _ 0) ; eassumption.
      all: econstructor ; eassumption.
  - remember (alpha nf).
    induction IHNatProp.
    + subst.
      unshelve econstructor.
      * exact (S n0).
      * eapply ty_reductions_trans.
        1: econstructor.
        3: eapply (reductions_alpha_subst _ _ _ 0).
        3: destruct eq ; eassumption.
        all: cbn ; econstructor.
        3: econstructor.
        1-3: now destruct eq.
        -- do 2 econstructor.
           clear t0 ; induction n0 ; cbn ; econstructor.
           assumption.
        -- eapply ty_reductions_alpha_succ.
           eassumption.
    + subst.
      destruct eq0.
      assert (Hyp:= alphareductions_alphareduc _ _ _ n1 red).
      destruct Hyp.
      unshelve eapply canone.
      exact (alpha (nSucc (S n0) ne)).
      2: now eapply NF.NForms_alpha.
      destruct eq.
      * econstructor ; cbn in * ; try now econstructor.
        do 2 econstructor.
        now inversion ty_u ; subst.
        eapply reductions_trans.
        eapply (reductions_alpha_subst _ _ _ 0) ; eassumption.
        cbn.
        eapply reductions_alpha_succ.
        assumption.
    + eapply beta.
      * eapply IHIHNatProp1.
        3: eassumption.
        eapply ty_reductions_Ltrans ; try eassumption.
        2: change (eval (l,,l (ne, true)) Γ nat nf).
        2: eapply eval_Ltrans ; try eassumption.
        all: eapply LCon_le_step ; reflexivity.
      * eapply IHIHNatProp2.
        3: eassumption.
        eapply ty_reductions_Ltrans ; try eassumption.
        2: change (eval (l,,l (ne, false)) Γ nat nf).
        2: eapply eval_Ltrans ; try eassumption.
        all: eapply LCon_le_step ; reflexivity.
  - destruct eq.
    eapply (reductions_alpha_subst _ _ _ 0) in red.
    eapply (NF.NForms_alpha _ 0) in n0 ; cbn in *.
    eapply typing_alpha in ty_u.
    destruct (alphareductions_alphareduc _ _ _ n0 red).
    eapply canone.
    econstructor.
    3: eassumption.
    3: eassumption.
    all: econstructor ; try eassumption.
    clear red n0.
    now inversion ty_u ; subst.
  - eapply beta ; eassumption.
Qed.  
  
(* Γ ⊢ t : A -> α ⊩ Γ -> α ⊩ A *)
Lemma soundness : forall l Γ Δ A t σ,
  [ Γ ⊢ t : A ] -> eval_subs l Δ Γ σ -> eval l Δ A (subs_term σ t).
Proof.
  intros l Γ Δ A t σ Ht; revert l Δ σ; induction Ht; cbn in *; intros l Δ σ Hσ.
  - revert n A e; induction Hσ; intros n B e'; cbn in *.
    + apply List.nth_error_In in e'; elim e'.
    + destruct n as [|n]; cbn in *.
      { injection e'; intros ->; rewrite lift_term_ELID; apply e; constructor. }
      { apply IHHσ; assumption. }
    + rewrite expand_term_SHFT.
      apply lift_eval with Γ; [repeat constructor|].
      apply IHHσ; assumption.
  - unshelve econstructor.
    2:{ intros Ξ l' e eε f x xε.
        assert (realizes l' Ξ B (subs_term (CONS x (subs_compose subs_term (subs_of_lift e) σ)) t)).
        { eapply IHHt .
          constructor; [assumption|].
          clear - f eε Hσ.
          revert Γ σ Hσ; induction eε; intros Ξ σ Hσ; cbn.
          + eapply eval_subs_Ltrans ; try eassumption.
          + constructor; apply IHeε; assumption.
          + remember (A :: Δ)%list as Ω.
            revert A Δ eε IHeε HeqΩ.
            induction Hσ; intros B Ξ eε IHeε HeqΩ; subst.
            { clear - t eε. destruct e.
              - exfalso ; now inversion t.
              - cbn.
                rewrite <- subs_lift_compose; do 2 constructor.
                inversion t ; subst.
                eapply typing_lift_compose ; eassumption.
              - exfalso.
                inversion t.
            }
            { constructor.
              + change (LIFT (subs_of_lift el)) with (@subs_of_lift term (ELLFT el)).
                rewrite subs_term_lift; eapply lift_eval; [constructor|]; try eassumption.
                eapply eval_Ltrans ; eassumption.
              + eapply IHHσ; [eassumption|eassumption|reflexivity].
            }
            { constructor; apply IHeε.
              injection HeqΩ; intros -> ->; assumption.
            }
        }
        eapply eval_reductions_expansion.
        econstructor.
        3: eapply reductions_step.
        4: now econstructor.
        3: eapply reduction_β.
        2,3:rewrite <- subs_term_lift, <- !subs_term_compose; cbn.
        3: eassumption.
        2: eapply escape_ty ; eassumption.
        econstructor.
        2: eapply escape_ty ; eassumption.
        eapply typing_lift_compat.
        2: eassumption.
        econstructor.
        eapply typing_subs_compat.
        eassumption.
        eapply typing_LIFT.
        eapply eval_subs_ty.
        eassumption.
    }
    unshelve econstructor.
    + exact (lam (subs_term (LIFT σ) t)).
    + eapply NF.NForms_lam.
    + econstructor.
      3:now econstructor.
      all: eapply typing_lam.
      all: eapply typing_subs_compat ; try eassumption.
      all: econstructor.
      all: eapply eval_subs_ty ; eassumption.
  - rewrite <- (lift_term_ELID (subs_term σ t)).
    eapply IHHt1.
    eassumption.
    2:reflexivity.
    econstructor.
    apply IHHt2; assumption.
  - econstructor.
    unshelve econstructor.
    3:now econstructor.
    all:econstructor.
  - eapply falseR.
    unshelve econstructor.
    3:now econstructor.
    all:econstructor.
  - specialize (IHHt _ _ _ Hσ).
    eapply NatProp_canonicity_aux in IHHt.
    induction IHHt.
    + destruct t1.
      destruct (decidInLCon l n) as [ [] | ].
      * unshelve eapply (eval_reductions_expansion _ _ bool).
        -- exact tt.
        -- econstructor ; try assumption.
           now econstructor.
           eapply reductions_trans ; try eassumption.
           eapply reductions_step ; try now econstructor.
           change (reduction l (alpha (nat_to_term n)) (bool_to_term true)).
           eapply reduction_alpha_red ; assumption.
        -- do 2 econstructor.
           all: now econstructor.
      * unshelve eapply (eval_reductions_expansion _ _ bool).
        -- exact ff.
        -- econstructor ; try assumption.
           now econstructor.
           eapply reductions_trans ; try eassumption.
           eapply reductions_step ; try now econstructor.
           change (reduction l (alpha (nat_to_term n)) (bool_to_term false)).
           eapply reduction_alpha_red ; assumption.
        -- eapply falseR.
           econstructor ; now econstructor.
      * unshelve eapply boolsplit.
        2: eassumption.
        -- unshelve eapply (eval_reductions_expansion _ _ bool).
           ++ exact tt.
           ++ econstructor ; try assumption.
              now econstructor.
              eapply reductions_trans ; try eassumption.
              eapply reductions_Ltrans ; try eassumption.
              eapply LCon_le_step ; reflexivity.
              eapply reductions_step ; try now econstructor.
              change (reduction (l,,l (n0, true)) (alpha (nat_to_term n)) (bool_to_term true)).
              eapply reduction_alpha_red.
              now econstructor.
           ++ do 2 econstructor.
              all: now econstructor.
        -- unshelve eapply (eval_reductions_expansion _ _ bool).
           ++ exact ff.
           ++ econstructor ; try assumption.
              now econstructor.
              eapply reductions_trans ; try eassumption.
              eapply reductions_Ltrans ; try eassumption.
              eapply LCon_le_step ; reflexivity.
              eapply reductions_step ; try now econstructor.
              change (reduction (l,,l (n0, false)) (alpha (nat_to_term n)) (bool_to_term false)).
              eapply reduction_alpha_red.
              now econstructor.
           ++ eapply falseR.
              econstructor ; now econstructor.
    + eapply boolneR ; eassumption.
    + eapply boolsplit.
      * eapply IHIHHt1.
        eapply eval_subs_Ltrans ; try eassumption.
        eapply LCon_le_step ; reflexivity.
      * eapply IHIHHt2.
        eapply eval_subs_Ltrans ; try eassumption.
        eapply LCon_le_step ; reflexivity.
  - induction (IHHt1 _ _ _ Hσ) as [? ? r Htr |? ? r Htr|  |].
    + destruct Htr.
      unshelve eapply eval_reductions_expansion.
      * exact (subs_term σ u1).
      * eapply ty_reductions_trans.
      { unshelve econstructor.
        3: eapply reductions_cse_e ; eassumption.
        all: econstructor ; try eassumption.
        all: eapply typing_subs_compat ; try eapply eval_subs_ty. 
        all: eassumption.
      }
      unshelve econstructor.
      3: eapply reductions_step.
      3: eapply reduction_ι_tt.
      econstructor ; try eassumption.
      4: now econstructor.
      all: eapply typing_subs_compat ; try eapply eval_subs_ty.
      all: eassumption.
      * eapply IHHt2 ; assumption.
    + destruct Htr.
      unshelve eapply eval_reductions_expansion.
      * exact (subs_term σ u2).
      * eapply ty_reductions_trans.
      { unshelve econstructor.
        3: eapply reductions_cse_e ; eassumption.
        all: econstructor ; try eassumption.
        all: eapply typing_subs_compat ; try eapply eval_subs_ty. 
        all: eassumption.
      }
      unshelve econstructor.
      3: eapply reductions_step.
      3: eapply reduction_ι_ff.
      econstructor ; try eassumption.
      4: now econstructor.
      all: eapply typing_subs_compat ; try eapply eval_subs_ty.
      all: eassumption.
      * eapply IHHt3 ; assumption.
    + destruct eq.
      eapply eval_reductions_expansion.
      econstructor.
      3: eapply reductions_cse_e ; eassumption.
      3: eapply (completeness A).(reflect).
      1-4: econstructor ; try eassumption.
      all : try eapply typing_subs_compat ; try eapply eval_subs_ty.
      all: try eassumption.
    + eapply eval_split.
      * eapply IHb1.
        eapply eval_subs_Ltrans ; try eassumption.
        now eapply LCon_le_step.
      * eapply IHb2.
        eapply eval_subs_Ltrans ; try eassumption.
        now eapply LCon_le_step.
  - econstructor.
    econstructor ; try reflexivity ; now econstructor.
  - eapply succR.
    econstructor ; try reflexivity ; econstructor.
    1,2: eapply typing_subs_compat ; try eapply eval_subs_ty.
    all: try eassumption.
    eapply IHHt ; assumption.
  - induction (IHHt1 _ _ _ Hσ) as [? ? r Htr |? ? r u Htr|  |].
    + destruct Htr.
      eapply eval_reductions_expansion.
      * eapply ty_reductions_trans.
      { unshelve econstructor.
        3: eapply reductions_natrec_e ; try eassumption.
        all: econstructor ; try eassumption.
        all: eapply typing_subs_compat ; try eapply eval_subs_ty. 
        all: eassumption.
      }
      unshelve econstructor.
      3: eapply reductions_step.
      3: eapply reduction_ι_z.
      econstructor ; try eassumption.
      4: now econstructor.
      all: eapply typing_subs_compat ; try eapply eval_subs_ty.
      all: eassumption.
      * eapply IHHt2 ; assumption.
    + destruct Htr.
      eapply eval_reductions_expansion.
      * eapply ty_reductions_trans.
      { unshelve econstructor.
        3: eapply reductions_natrec_e ; eassumption.
        all: econstructor ; try eassumption.
        all: eapply typing_subs_compat ; try eapply eval_subs_ty. 
        all: eassumption.
      }
      unshelve econstructor.
      3: eapply reductions_step ; try reflexivity.
      4: now econstructor.
      3: eapply reduction_ι_succ.
      all: econstructor ; try eassumption.
      4: econstructor ; try eassumption.
      all: try eapply typing_subs_compat ; try eapply eval_subs_ty.
      all: try eassumption.
      eapply escape_ty.
      exact n.
      * specialize (IHHt3 l _ _ Hσ) as [].
        specialize (redfun0 _ l ELID (typing_ELID _)).
        erewrite lift_term_ELID in redfun0.
        eapply redfun0.
        reflexivity.
        eapply IHn.
        assumption.
    + destruct eq.
      eapply eval_reductions_expansion.
      econstructor.
      3: eapply reductions_natrec_e ; eassumption.
      3: eapply (completeness A).(reflect).
      1-4: econstructor ; try eassumption.
      all : eapply typing_subs_compat ; try eapply eval_subs_ty.
      all: try eassumption.
    + eapply eval_split.
      * eapply IHn1.
        eapply eval_subs_Ltrans ; try eassumption.
        now eapply LCon_le_step.
      * eapply IHn2.
        eapply eval_subs_Ltrans ; try eassumption.
        now eapply LCon_le_step.
Qed.      
