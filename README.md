## PhD-Thesis
This is the source for my PhD thesis. It owes a lot to the nice kaobook template, many thanks to its contributors!

## Build requirements
If you want to build the pdf yourself from the .tex files, you need to compile them with XeLaTeX.
