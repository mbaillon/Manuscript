From mathcomp Require Import all_ssreflect.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Require Import List Lia PeanoNat.
Import ListNotations.

Require Import ConstructiveEpsilon.


Section fix_types.

(* I removed *)
Variable Q A O : Type.

(* Definition 1: Dialogue continuity à la PMP *)
Inductive C : ((Q -> A) -> O) -> Type :=
  | etaC o : C (fun _ => o)
  | betaC (q : Q) (k : A -> (Q -> A) -> O) (H : forall a, C (k a))
      : C (fun f => k (f q) f).

(* Definition 2: inductive dialogue trees *)
Inductive dialogue :=
  | eta (o : O)
  | beta (q : Q) (k : A -> dialogue).

(* Arguments exchanged compared to the main file *)
Fixpoint eval_dialogue (d : dialogue) (f : Q -> A) :=
  match d with
  | eta o => o
  | beta q k => eval_dialogue (k (f q)) f
  end.

Definition continuous_via_dialogues_ex F (d : dialogue) :=
  F =1 eval_dialogue d.

Definition continuous_via_dialogues F :=
  exists d, continuous_via_dialogues_ex F d.

(* 2 -> 1 modulo extensional_equality *)
Fixpoint dialogue_to_C (d : dialogue) : {F & C F}.
Proof.
  case: d=> [o|q k].
  - exists (fun _ => o).
    exact (etaC o).
  - set k' := fun a=> (let (k',_) := (dialogue_to_C (k a)) in k').
    exists (fun f => k' (f q) f).
    apply betaC=> a.
    rewrite/k' ; move: (dialogue_to_C (k a))=> []//.
Defined.

Proposition continuous_dialogue_to_C_correct d : {F & C F & F =1 eval_dialogue d}.
Proof.
  exists (projT1 (dialogue_to_C d)).
  - exact: projT2 (dialogue_to_C d).
  - elim: d => [//|q k /=] + f.
    move/(_ (f q)) ; move: (dialogue_to_C _)=> []//.
Qed.


Fixpoint C_to_dialogue (F : (Q -> A) -> O) (CF : C F) : dialogue.
Proof.
  case: CF => [o|q k H].
  - exact (eta o).
  - exact (beta q (fun a => (C_to_dialogue (k a) (H a)))).
Defined.

Proposition continuous_C_to_dialogue : forall F : (Q -> A) -> O, C F -> continuous_via_dialogues F.
Proof.
  move=> F CF.
  exists (C_to_dialogue CF)=> f.
  elim: CF=> [o|q k H] //=.
Qed.


(* Definition 3: extensional dialogues *)
Notation result Q O := (option (Q + O))%type.
Notation ask q := (Some (inl q)).
Notation reject := None.
Notation output o := (Some (inr o)).

Definition ext_tree := list A -> result Q O.

Definition sub_ext_tree (τ : ext_tree) a := (fun l => τ (a::l)).

Record is_total_tree (τ : ext_tree) := {
  is_not_empty' : τ [] <> reject ;
  is_valid_nodes' : forall l,
    match τ l with
    | ask q => forall a, τ (rcons l a) <> reject
    | _ => forall a, τ (rcons l a) = reject
    end
}.

Definition well_founded (τ : ext_tree) :=
  forall f : nat -> A, exists n o,
    τ (map f (iota 0 n)) = output o.


Fixpoint eval_ext_tree (τ : ext_tree) (f : Q -> A) (n : nat)
  (l : list A): result Q O.
Proof.
  refine (match n with
          | 0 => τ l
          | S k => _
          end).
  - refine (match τ l with
            | ask q => eval_ext_tree τ f k (rcons l _)
            | output o => reject
            | reject => reject
            end).
    exact (f q).
Defined.


Fixpoint eval_ext_tree2 (τ : ext_tree) (f : Q -> A) (n : nat) : result Q O :=
  match τ [] with
  | ask q => if n is S n' then eval_ext_tree2 (sub_ext_tree τ (f q)) f n' else ask q
  | output o => if n is S n' then reject else output o
  | reject => reject
  end.

(* I guess the last part would imply well_foundedness *)
Definition continuous_via_ext_tree_ex F τ :=
  is_total_tree τ /\
  well_founded τ /\
  forall f, exists n, eval_ext_tree τ f n [] = output (F f).

Definition continuous_via_ext_tree F :=
  exists τ, continuous_via_ext_tree_ex F τ.

Definition weak_continuous_via_ext_tree_ex F τ :=
  forall f, exists n, eval_ext_tree τ f n [] = output (F f).

Definition weak_continuous_via_ext_tree F :=
  exists τ, weak_continuous_via_ext_tree_ex F τ.

End fix_types.

Variable (A : Type).
Lemma continous_via_ext_tree_DNE :
  ext_tree A False False -> A.
Proof.
  intros tau.
  pose (q:= tau []).
  eval_ext_tree (Q := A) (A := False) (O := False)
    τ f n [] = Some (inr a) -> False.

(* Conjecture : other direction *)

Fixpoint to_total (o0 : O) τ l :=
  match τ [], l with
  | reject, [] => output o0
  | α, [] => α
  | ask q, a::l' => to_total o0 (sub_ext_tree τ a) l'
  | _, _ => reject
  end.

Lemma to_total_correct o0 τ : is_total_tree (to_total o0 τ) /\ (to_total o0 τ) =1 τ.
Proof.
  split.
  - split=> [/=|l] ; first case: τ=> // [[]] //.
    have ->: τ = (fun l => τ ([] ++ l)) by [].
    elim: l [] => [|a l H] acc /=.
    - case: (τ _) => [[]|]// _ ?.
      case: sub_ext_tree => [[]|] => //.
    - case: (τ _)=> [[]|]// _.
      have ->: sub_ext_tree (fun l => τ (acc ++ l)) a = (fun l => τ ((rcons acc a) ++ l)).
        Fail rewrite cat_rcons. (* need extensional equality *)
Abort.

Section to_well_founded_aux.

Variable eq_q : rel Q.
Hypothesis eq_q_correct : forall q1 q2, reflect (q1 = q2) (eq_q q1 q2).

Fixpoint to_well_founded_aux (f : Q -> option A) τ l :=
  match τ [], l with
  | α, [] => α
  | ask q, a :: l' =>
    if f q is Some a' then
      to_well_founded_aux f (sub_ext_tree τ a') l'
    else
      to_well_founded_aux (fun q' => if eq_q q q' then Some a else None) (sub_ext_tree τ a) l'
  | _, _ => reject
  end.

Definition to_well_founded := to_well_founded_aux (fun=> None).

Lemma to_well_founded_correct τ : well_founded (to_well_founded τ) /\ eval_ext_tree τ =2 eval_ext_tree (to_well_founded τ).
Abort.

Theorem unweak_continuous_via_ext_tree_ex F :
  weak_continuous_via_ext_tree F -> continuous_via_ext_tree F.
Abort.

End to_well_founded_aux.
(* End conjecture *)


Fixpoint dialogue_to_ext_tree (d : dialogue) : ext_tree := fun l =>
  match l, d with
  | [], eta o => output o
  | [], beta q _ => ask q
  | _, eta _ => reject
  | a::l, beta _ k => dialogue_to_ext_tree (k a) l
  end.

Fixpoint dialogue_to_ext_tree2 (d : dialogue) (l : list A) {struct l} : result Q O.
Proof.
  refine (match d with
          | eta a => _
          | beta i k => _
          end).
  + refine (match l with
            | [] => output a
            | _ => reject
            end).
  + refine (match l with
            | [] => ask i
            | cons o q => _
            end).
  + exact (dialogue_to_ext_tree2 (k o) q).
Defined.

Lemma continous_dialogue_to_ext_tree : forall F : (Q -> A) -> O, continuous_via_dialogues F -> continuous_via_ext_tree F.
Proof.
  intros F [d Hd].
  exists (dialogue_to_ext_tree2 d).
  split ; [ | split].
  admit.
  admit.
  intros f.
  specialize (Hd f).
  rewrite Hd ; clear Hd.
  - induction d.
    + exists 0.
      reflexivity.
    + destruct (H (f q)) as [n eq_].
      rewrite -eq_/=.
      exists n.+1.
      clear eq_ H.
      generalize (nil (T := A)).
      induction n.
      * cbn ; intros.
        reflexivity.
      * cbn ; intros.
        destruct (dialogue_to_ext_tree2 (k (f q)) l).
        destruct s.
        2,3 : reflexivity.
        erewrite IHn.
        reflexivity.
      

    
    suff -> : forall m, map f (iota m.+1 n) = map (fun i : nat => f i.+1) (iota m n) ; first done.
      elim: n {eq_} => [//|n H' m /=].
      rewrite H' ; done.
  - move=> f ; rewrite (F_eq1_eval_d f).
    elim: d {F_eq1_eval_d} => [o|q k Hr].
    - exists 0 ; done.
    - move: (Hr (f q))=> [] n <-.
      exists n.+1=> /= ; done.
  specialize (Hd f).
  rewrite Hd ; clear Hd.
  induction d ; intros.
  - exists 0.
    reflexivity.
  - cbn.
    specialize (H (f q)).
    destruct H as [n H].
    exists (S n).
    revert H.
    generalize (nil (T := A)).
    induction n ; intros ; cbn.
    + cbn in *.
      rewrite <- H ; clear H.
      induction l.
      * cbn.
        reflexivity.
      * cbn in *.
        
      assumption.
    + cbn in *.
      destruct ((k (f q))).
      inversion H.
      cbn in *.
      
      destruct s.
      
    
    cbn.
    
  
  move=> F [] d F_eq1_eval_d.
  exists (dialogue_to_ext_tree d) ; do!split.
  1-3: move {F_eq1_eval_d}.
  - case: d ; done.
  - move=> l ; elim: d l=> [o [] //|q k H l /=].
    case: l=> [a /=|a l] ; first by case k.
    apply H ; done.
  - elim: d=> [o|q k H f /=] ; first by exists 0 ; exists o.
    move: (H (f 0) (fun i => f i.+1))=> []n []o eq_.
    exists n.+1 ; exists o ; rewrite -eq_/=.
    suff -> : forall m, map f (iota m.+1 n) = map (fun i : nat => f i.+1) (iota m n) ; first done.
      elim: n {eq_} => [//|n H' m /=].
      rewrite H' ; done.
  - move=> f ; rewrite (F_eq1_eval_d f).
    elim: d {F_eq1_eval_d} => [o|q k Hr].
    - exists 0 ; done.
    - move: (Hr (f q))=> [] n <-.
      exists n.+1=> /= ; done.
Qed.


(* Definition 4: modulus definition *)

(* Definition Forall T (P : T -> Prop) l := fold_right and True (map P l). *)

Definition modulus_on O' (F : (Q -> A) -> O') (f : Q -> A) (L : list Q) :=
  forall g, Forall (fun q => f q = g q) L -> F f = F g.

Definition modulus_continuous O' (F : (Q -> A) -> O') :=
  forall f, exists L, modulus_on F f L.

Definition modulus O' (F : (Q -> A) -> O') (M : (Q -> A) -> list Q) :=
  forall f g, Forall (fun q => f q = g q) (M f) -> F f = F g.

Definition continuous_via_modulus O' (F : (Q -> A) -> O') :=
  exists M, modulus F M.


Fixpoint eval_ext_tree_modulus (τ : ext_tree) (f : Q -> A) (n : nat) : list Q :=
  match τ [], n with
  | ask q, S n' => q :: (eval_ext_tree_modulus (fun l => τ ((f q) :: l)) f n')
  | _, _ => []
  end.

Lemma unique_depth τ f n n' o o' : eval_ext_tree τ f n = output o -> eval_ext_tree τ f n' = output o' -> n = n'.
Proof.
  elim: n n' τ => [|n H] [|n'] τ=> //=.
  1,2: case: (τ [])=> [|//] [] //.
  case: (τ [])=> [|//] [|//] ? /H /[apply] -> ; done.
Qed.

Lemma continuous_ext_tree_to_modulus F :
  weak_continuous_via_ext_tree F -> modulus_continuous F.
Proof.
  move=> [] τ F_eq_eval f.
  move: (F_eq_eval f) => [] n F_eq_eval_f.
  exists (eval_ext_tree_modulus τ f n)=> g g_coin.
  have eval_f_g_eq : eval_ext_tree τ g n = eval_ext_tree τ f n.
    elim: n τ g_coin {F_eq_eval_f F_eq_eval}=> [//|n H/= τ].
    case: (τ [])=> [[q|//]|//].
    set l := eval_ext_tree_modulus _ _ _ ; have: ((q :: l) <> []) by done.
    move: {-1}l (erefl (q :: l)).
    rewrite/l=> {}l h1 h2 H' ; move: H' h1 h2 => [] // q' l' fq_eq_gq /[swap] [[]] -> <- /H.
    rewrite fq_eq_gq=> ->//.
  move: (F_eq_eval g) => [] n' F_eq_eval_g.
  have n_eq_n' : n = n'.
    move: eval_f_g_eq ; rewrite F_eq_eval_f=> /unique_depth /(_ F_eq_eval_g) ; done.
  move: eval_f_g_eq ; rewrite {1}n_eq_n' F_eq_eval_f F_eq_eval_g=> [[]] ; done.
Qed.

Inductive interrogation (f : Q -> A) : list A -> (list A -> option (Q + O)) -> Prop :=
  NoQuestions τ : interrogation f [] τ
| Ask l τ q a : f q = a ->
                interrogation f l τ ->
                τ l = Some (inl q) ->
                interrogation f (l ++ [a]) τ.

Definition continuous_via_interrogations_ex F τ :=
  forall f, exists ans, interrogation f ans τ /\ τ ans = output (F f).

Definition continuous_via_interrogations F :=
  exists τ, continuous_via_interrogations_ex F τ.

Lemma eval_ext_tree_ext τ τ' f n :
  (forall l, τ l = τ' l) ->
  eval_ext_tree τ f n = eval_ext_tree τ' f n.
Proof.
  intros Heq. induction n in τ, τ', Heq |- *.
  - cbn. rewrite Heq. reflexivity.
  - cbn. rewrite Heq. destruct τ' as [ [] | ]; eauto.
Qed.

Lemma interrogation_plus τ f n l  :
  interrogation f l τ ->
  eval_ext_tree (fun l' => τ (l ++ l')) f n = eval_ext_tree τ f (length l + n).
Proof.
  intros H. induction H in n |- *.
  - reflexivity.
  - rewrite app_length. cbn -[eval_ext_tree].
    replace ((length l + 1)%coq_nat + n)%Nrec with (length l + (S n)).
    2: admit.
    rewrite <- IHinterrogation.
    cbn. rewrite app_nil_r H1. eapply eval_ext_tree_ext.
    cbn. intros. now rewrite <- app_assoc, H. 
Admitted.

Lemma interrogation_cons q1 a1 a2 τ (f : Q -> A) :
  τ [] = Some (inl q1) ->
  f q1 = a1 ->
  interrogation f a2 (fun l => τ (a1 :: l)) ->
  interrogation f (a1 :: a2) τ.
Proof.
  intros H1 H2.
  induction a2 in a1, H1, H2 |- * using rev_ind.
  - inversion 1; subst.
    + eapply Ask with (l := []); eauto. constructor.
    + destruct l; cbn in *; congruence.
  - inversion 1.
    + destruct a2; cbn in *; congruence.
    + subst. eapply app_inj_tail in H0 as [<- <-].
      eapply Ask with (l := f q1 :: l); eauto.
Qed.

Lemma interrogation_ext τ τ' a f :
  (forall l, τ l = τ' l) ->
  interrogation f a τ <-> interrogation f a τ'.
Proof.
  intros. split; induction 1; econstructor; eauto; congruence.
Qed.

Lemma interrogation_app a1 a2 τ f :
  interrogation f a1 τ ->
  interrogation f a2 (fun l => τ (a1 ++ l)) ->
  interrogation f (a1 ++ a2) τ.
Proof.
  induction 1 in a2 |- *; cbn.
  - eauto.
  - intros. rewrite <- !app_assoc.
    eapply IHinterrogation.
    eapply interrogation_cons.
    + rewrite app_nil_r. eassumption.
    + eauto.
    + eapply interrogation_ext; eauto. cbn. intros. now rewrite <- app_assoc.
Qed.

Lemma eval_ext_tree_to_interrogation τ f n o :
  eval_ext_tree τ f n = output o ->
  exists ans, length ans <= n /\ interrogation f ans τ /\ τ ans = output o.
Proof.
  intros H.
  induction n in τ, H |- *.
  - cbn in *. exists []. repeat split. econstructor. destruct τ as [ [] | ]; eauto.
  - cbn in *. destruct τ as [ [] | ] eqn:E; try congruence.
    destruct (IHn (sub_ext_tree τ (f q))) as (ans & H3 & H1 & H2); eauto.
    exists (f q :: ans). repeat split.
    + admit.
    + eapply interrogation_cons; eauto. 
      now rewrite <- H2.
Admitted.

Lemma interrogation_equiv_eval_ext_tree τ f o :
    (exists (ans : list A), interrogation f ans τ /\ τ ans = output o) <-> (exists n : nat, eval_ext_tree τ f n = output o).
Proof.
  split.
  + intros (ans & Hi & Hout).
    exists (length ans + 1). rewrite <- interrogation_plus; eauto.
    cbn. rewrite app_nil_r Hout. admit.
  + intros [n H]. eapply eval_ext_tree_to_interrogation in H as (? & ? & ? & ?); eauto.
Admitted.

Lemma continuous_via_interrogations_iff F τ :
  continuous_via_interrogations_ex F τ <-> weak_continuous_via_ext_tree_ex F τ.
Proof.
  split.
  - intros H f. eapply interrogation_equiv_eval_ext_tree. eapply H.
  - intros H f. eapply interrogation_equiv_eval_ext_tree. eapply H.
Qed.

End fix_types.

Section fix_types.

Definition Q := nat.

Variables (A O : Type).

Notation ask q := (Some (inl q)).
Notation reject := None.
Notation output o := (Some (inr o)).

(* Apparently F continuous for M and M continuous implies there (exist M', auto_modulus F M') (ref?) *)

Definition auto_modulus (F : (Q -> A) -> O) M :=
  modulus F M /\ modulus M M.

Definition extend (a0 : A) l := seq.nth a0 l.

Definition modulus_in (a0 : A) M l := all (fun i => i < size l) (M (extend a0 l)).
Definition modulus_exact (a0 : A) M l := has (fun i => i.+1 == size l) (M (extend a0 l)).

Lemma Forall_all T P (l : seq.seq T) : all P l <-> Forall P l.
Proof.
  elim: l=> // a l [] H1 H2 ; split=> [/=/andP[] ??|].
  - apply Forall_cons=> // ; apply H1 ; done.
  - move: {-1} l (erefl (a :: l))=> l' /[swap] ; case=> // a' l'' P_a' /[swap] [[]] _ <- /H2 ? ; apply/andP ; done.
Qed.

Lemma modulus_in_cat a0 M l a : modulus M M -> modulus_in a0 M l -> M (extend a0 l) = M (extend a0 (rcons l a)).
Proof.
  move=> mod_M mod_in ; apply mod_M.
  move: mod_in ; rewrite/modulus_in Forall_all.
  rewrite !Forall_forall => + i i_in ; move/(_ _ i_in).
  rewrite /extend nth_rcons ; case: ifP ; done.
Qed.

Definition modulus_to_ext_tree a0 (F : (Q -> A) -> O) M l :=
  let f := extend a0 l in
  let n := size l in
  if modulus_in a0 M l then
    if modulus_exact a0 M l then
      output (F f)
    else
      reject
  else
    ask n.


Lemma continuous_auto_modulus_to_ext_tree a0 F M:
  auto_modulus F M -> continuous_via_ext_tree_ex F (modulus_to_ext_tree a0 F M).
Proof.
Abort.

