From mathcomp Require Import all_ssreflect.
Require Import continuity_zoo.

Section EscardoXuInconsistency.
Definition modulus_nat (F : (nat -> nat) -> nat) (M : (nat -> nat) -> nat) :=
   forall f g,  map f (iota 0 (M f)) = map g (iota 0 (M f)) ->  F f = F g.

Definition continuous_via_modulus_nat (F : (nat -> nat) -> nat) :=
  { M & modulus_nat F M }.

Fixpoint nat_to_Baire (k n m : nat) : nat :=
  match n,m with
  | 0, _ => k
  | _ , 0 => 0
  | S i, S j => nat_to_Baire k i j
  end.

Lemma nat_to_Baire_le (k n m : nat) : le n m -> nat_to_Baire k n m = k.
Proof.
  revert m.
  induction n ; intros m H ; [reflexivity | ].
  cbn.
  destruct m ; [now inversion H |].
  eapply IHn.
  now eapply le_S_n.
Qed.

Lemma nat_to_Baire_gt (k n m : nat) : gt n m -> nat_to_Baire k n m = 0.
Proof.
  revert m ; induction n ; intros m H ; [now inversion H |].
  cbn.
  destruct m ; [reflexivity |].
  eapply IHn.
  now eapply Arith_prebase.gt_S_n_stt.
Qed.

Lemma nat_to_Baire_eq (k n : nat) : nat_to_Baire k n n = k.
Proof.
  now induction n.
Qed.  
  

Theorem absurd_continuous :
  (forall (F : (nat -> nat) -> nat), continuous_via_modulus_nat F) -> 0 = 1.
Proof.
  intro H.
  pose (const_zero := fun _ : nat => 0).
  pose (M := fun F => projT1 (H F) const_zero).
  pose (Me := fun F => projT2 (H F)).
  pose (m := M (fun _ : nat -> nat => 0)).
  pose (f := fun (alpha : nat -> nat) => M (fun beta => alpha (beta m))).
  assert (f const_zero = m) as checkf by reflexivity.
  have f_eq: (forall beta, map const_zero (iota 0 (M f)) = map beta (iota 0 (M f)) -> m = f beta).
  { rewrite <- checkf.
    now eapply Me.
  }
  have cont_alpha : forall alpha beta,
      map const_zero (iota 0 (f alpha)) = map beta (iota 0 (f alpha)) ->
      alpha 0 = alpha (beta m).
  { intros alpha beta.
    now eapply (Me (fun beta => alpha (beta m)) const_zero beta).
  }
  pose (alpha := nat_to_Baire 1 (S (M f))).
  pose (beta := nat_to_Baire (S (M f)) m).
  have eqfalpha : m = f alpha.
  { eapply f_eq.
    unfold alpha.
    suff: forall k, [seq const_zero i | i <- iota k (M f)] =
                      [seq nat_to_Baire 1 (S (k + (M f))) i | i <- iota k (M f)] by
        (intros Heq ; exact (Heq 0)).
    clear.
    induction (M f) ; [reflexivity|] ; intros k.
    cbn.
    f_equal.
    { destruct k ; [reflexivity |].
      unfold const_zero ; symmetry.
      eapply nat_to_Baire_gt ; unfold gt.
      eapply PeanoNat.Nat.lt_lt_add_r.
      now eapply PeanoNat.Nat.lt_succ_diag_r.
    }
    erewrite (IHn (S k)).
    cbn.
    eapply eq_map ; intros i.
    destruct i ; [reflexivity |].
    erewrite <- plus_n_Sm.
    destruct i ; [| reflexivity]. 
    symmetry ; eapply nat_to_Baire_gt ; now eapply Arith_prebase.gt_Sn_O_stt.
  }
  have eqbeta : alpha 0 = alpha (beta m) .
  { eapply cont_alpha.
    rewrite <- eqfalpha.
    unfold beta.
    suff: forall k j, [seq const_zero i | i <- iota k m] =
                      [seq nat_to_Baire j (k + m) i | i <- iota k m]
        by (intros Heq ; exact (Heq 0 _)).
    clear.
    induction m ; [reflexivity |] ; intros k j.
    cbn ; f_equal.
    { symmetry ; eapply nat_to_Baire_gt.
      erewrite <- Plus.plus_Snm_nSm_stt.
      eapply PeanoNat.Nat.lt_lt_add_r ; eauto.
    }
    erewrite (IHm (S k) j).
    cbn.
    eapply eq_map ; intros i.
    erewrite <- plus_n_Sm ; now destruct i.
  }
  change (alpha 0 = 1).
  rewrite eqbeta.
  unfold alpha ; unfold beta.
  erewrite nat_to_Baire_eq.
  erewrite nat_to_Baire_eq.
  reflexivity.
Qed.

End EscardoXuInconsistency.

Section DNE.
  Proposition continuity_DNE (A : Type) (F : (A -> False) -> False) :
     seq_contW F -> A.
  Proof.
    intros [tau H].
    now destruct (tau nil).
  Qed.
  
End DNE.

Section DNS.
  Proposition continuity_DNS (I O : Type) :
    forall (F : (I -> O) -> False), dialogue_cont F -> (I -> (O -> False) -> False) -> False.
  Proof.
    intros F [d H] ; intros f.
    clear F H.
    induction d as [ | i k ke] ; [assumption |].
    exact (f i ke).
  Qed.
  
End DNS.
