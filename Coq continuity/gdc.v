Require Import Program.
Require Import Lia.
Set Contextual Implicit.


(** * Sequences *)

Definition not_empty A :=exists (x:A), True.
Definition seq A :=  nat -> A.

Notation "A ^ℕ":=(seq A) (at level 71).
Notation "A °":=(list A) (at level 71).

(* ◬ Lists are written from left to right... *)

Notation "u ⋆ a":=(cons a u) (at level 40).

Notation "# u ":=(length u) (at level 41).


Fixpoint concat `{A:Type} (u v:list A):=
  match v with
  |nil => u
  |cons a v => (concat u v) ⋆ a
  end.


Notation "u @ v":=(concat u v) (at level 40).

Eval compute in (nil ⋆ 0 ⋆ 1 ⋆ 2) @ (nil ⋆ 3 ⋆ 4).

(** ** Infrastructures (definitions / lemmas for lists & concat)  *)

Lemma cat_nil_l `{A:Type}:
  forall (u :A°), nil @ u = u.
Proof.
  induction u.
  - reflexivity.
  - simpl. now rewrite IHu.
Qed.

Lemma cat_nil_r `{A:Type}:
  forall (u :A°), u @ nil = u.
Proof.
  reflexivity.
Qed.


Lemma cat_comm `{A:Type}:
  forall (u v w:A°), u @ (v @ w) = (u @ v) @ w.
Proof.
  intros u v w;revert u w; induction w;simpl.
  - reflexivity.
  - now rewrite IHw.
Qed.

Lemma cat_nil_nil `{A:Type}:
  forall (u v:A°),u @ v =nil ->  u = nil /\ v = nil.
Proof.
  intros u v;revert u;induction v;simpl;intros;split;trivial.
  inversion H. inversion H.
Qed.

Lemma cat_inv_r `{A:Type}:
  forall (a:A) (u v w:A°), u@v =w ⋆ a -> v = nil \/ exists v', u@v' = w /\ v = v' ⋆ a.
Proof.
  intros a u v ;revert u a; induction v;intros;simpl in *.
  - now left.
  - inversion H. right. exists v;now split.
Qed.

Lemma cat_inv_l `{A:Type}:
  forall (a b:A) (u v:A°), (nil ⋆ a) @ u =(nil⋆ b) @ v -> a=b /\ u = v.
Proof.
  intros a b u v;revert a b u; induction v;intros;simpl in *.
  - destruct u;simpl in *;inversion H;subst. now split.
    destruct (cat_nil_nil _ _ H2). inversion H0.
  - destruct (cat_inv_r _ _ _ _ H);subst;simpl in *.
    inversion H;subst. symmetry in H2. destruct (cat_nil_nil _ _ H2). inversion H0.
    destruct H0 as (w,(Hw,Hu));subst.
    destruct (IHv _ _ _ Hw);subst;now split.
Qed.

Lemma len_cat `{A:Type}:
  forall (u v:A°), # (u @ v) = #u + # v.
Proof.
  intros u v;induction v;intros.
  - simpl. lia.
  - simpl. rewrite IHv. lia.
Qed.    

Fixpoint rev_aux `{A:Type} (u acc:list A):=
  match u with
  |nil => acc
  |cons a v => (rev_aux v (acc ⋆ a))
  end.

Definition rev `{A:Type} (u:list A):= rev_aux u nil.

Lemma rev_aux_cat  `{A:Type}:
  forall (l acc:list A), rev_aux l acc = acc @ rev l.
Proof.  
  intro l;induction l;intros.
  - reflexivity.
  - cbn. do 2 rewrite IHl.
    now rewrite cat_comm.
Qed.

Lemma rev_cons  `{A:Type} (l:list A) a:
  rev (l⋆a) = (nil ⋆ a) @ (rev l).
Proof.
  cbn. now rewrite rev_aux_cat.
Qed.


Lemma rev_cat  `{A:Type}:
  forall (l1 l2:list A), rev (l1@l2) = (rev l2) @ rev l1.
Proof.
  intros l1 l2;revert l1;induction l2;intros.
  - unfold rev. simpl. now rewrite cat_nil_l.
  - simpl. now rewrite rev_cons,IHl2,cat_comm,<-rev_cons.
Qed.

Lemma rev_idempotent `{A:Type}:
  forall (l:A°), rev (rev l)=l.
Proof.
  induction l.
  - reflexivity.
  - now rewrite rev_cons,rev_cat,IHl.
Qed.

Lemma list_ind_rev `{A:Type}:
  forall (P : A ° -> Prop),
       P nil ->
       (forall (a : A) (l : A °), P l -> P ((nil ⋆ a)@l)) -> forall l : A °, P l.
Proof.
  intros P Hnil IH l.
  rewrite <- (rev_idempotent l).
  induction (rev l).
  - assumption.
  - rewrite rev_cons. now apply IH.
Qed. 


Lemma nonemptylist_cat `{A:Type}:
  forall (u:A°), #u >0 -> exists a v, u = (nil ⋆ a)@v.
Proof.
  pose (P:=fun u => # u > 0 -> exists (a : A) (v : A °), u = (nil ⋆ a) @ v).
  apply (list_ind_rev P).
  - intros H. inversion H.
  - intros a l IH H.  
    exists a; exists l;reflexivity.
Qed.

Lemma nil_nonempty `{A:Type}:
  forall (u:A°) a, nil = (nil ⋆ a)@u -> False.
Proof.
  intros.
  assert (0 = # ((nil ⋆ a) @ u)).
  replace 0 with (# (@nil A)) by reflexivity. now f_equal.
  rewrite len_cat in H0. simpl in H0. lia.
Qed.

Lemma cut_list `{A:Type}:
  forall (u:A°) n, n <= #u -> exists v w, u = v@w /\ # v = n.
Proof.
  intros u n. induction n;intros.
  - exists nil;exists u;split;trivial. now rewrite cat_nil_l.
  - destruct IHn as (v,(w,(Hu,Hv))). lia.
    destruct (nonemptylist_cat w) as (a,(w',H')).
    rewrite Hu,len_cat,Hv in H. lia.
    exists (v⋆a);exists w';split.
    + now rewrite Hu,H',cat_comm. 
    + simpl;now rewrite Hv.
Qed.

Program Fixpoint concat_seq `{A:Type} (u:list A) (α:seq A) {measure (length u)}:=
  match u with
  |nil => α
  |cons a v => concat_seq v (fun n=> match n with 0 => a | _ => α (n+1) end)
  end.


Inductive prefix_seq {A}: A° -> A^ℕ -> Prop:=
|prefix_seq_nil : forall α, prefix_seq (@nil A) α
|prefix_seq_cons : forall u α a,
    prefix_seq u α -> (α (#u) = a) -> prefix_seq (u ⋆ a) α.

Infix "≺":=prefix_seq (at level 71).

Inductive prefix `{A:Type}: A° -> A° -> Prop:=
|prefix_id: forall u, prefix u u
|prefix_cons: forall u v a, prefix u v ->  prefix u (v ⋆ a).

Infix "≤s":=prefix (at level 71).

Lemma prefix_cat `{A:Type}:
  forall (u v w:A°), v ≤s w -> u @ v ≤s u @ w.
Proof.
  intros u v w H.
  induction H.
  - apply prefix_id.
  - simpl.
    now apply prefix_cons.
Qed.

Lemma prefix_nil `{A:Type}:
  forall (u:A°), nil ≤s u.
Proof.
  intros u. induction u.
  - apply prefix_id.
  - now apply prefix_cons.
Qed.    


Lemma prefix_trans `{A:Type}: forall (u v w:A°), u ≤s v -> v ≤s w -> u ≤s w.
Proof.
  intros u v w Hu Hv.
  induction Hv.
  - assumption.
  - apply prefix_cons.
    apply IHHv, Hu.
Qed.

Lemma cat_prefix `{A:Type}:
  forall (u v:A°), u ≤s v -> exists v', v = u @ v'.
Proof.
  intros u v. revert u.
  induction v;intros u H;inversion H;subst.
  - now exists nil.
  - now exists nil. 
  - destruct (IHv _ H2);subst.
    now exists (x ⋆ a).
Qed.



(** * Trees as predicates *)

Definition included `{A:Type} (T U:A° -> Prop) := forall u, T u -> U u.
Notation "T ⊆ U ":=(included T U) (at level 51).

Definition equiv `{A:Type} (T U:A° -> Prop) := T ⊆ U /\ U ⊆ T.
Notation "T ≅ U ":=(equiv T U) (at level 51).

(* Notation "u ∈ T":=(T u) (at level 71). *)
Definition is_tree `{A:Type} (T:A° -> Prop) := forall u, T u -> forall v, v≤s u -> T v.
Definition is_monotone `{A:Type} (T:A° -> Prop) := forall u, T u -> forall v, u ≤s v -> T v.

Definition tree `{A:Type}:= {T:A° -> Prop| is_tree T}.
Definition monotone `{A:Type}:= {T:A° -> Prop| is_monotone T}.


Definition career `{A:Type} `{P:A -> Prop}:= @proj1_sig A P.

(** ** Inductive trees  *)

Inductive tree_int `{B:Type}:=
  Leaf | Node : (B -> tree_int) -> tree_int.

Fixpoint T_ext  `{B:Type} (t:@tree_int B):B° -> Prop:=
  match t with
  | Leaf => fun u => False
  | Node f =>  fun u =>
                u = nil \/ exists a u', u = ((cons a nil) @ u') /\ T_ext (f a) u'
 end. 


Lemma T_ext_nil `{B:Type}: forall t (x:B°), T_ext t x -> T_ext t nil.
Proof.
  intros t; induction t;intros x Hx;simpl in *;trivial.
  now left.
Qed.


  
Lemma T_ext_closed `{B:Type}: forall u (x:B°) t, T_ext t (u@x) -> T_ext t u.
Proof.
  intro u.
  pose (P:=fun u => forall (x:B°) t, T_ext t (u@x) -> T_ext t u).
  apply (list_ind_rev P);unfold P;simpl.
  - intros x t Hx; induction t;simpl in *;trivial.
    now left.
  - intros a l IHl q t H.
    destruct t;simpl in *;trivial.
    destruct H as [H |(b,(v,(Ha,Hv)))].
    + destruct q;simpl in *;[left;assumption|inversion H].
    + rewrite <- cat_comm in Ha.
      destruct (cat_inv_l _ _ _ _ Ha);subst.
      right. exists b. exists l. split;trivial.
      now apply IHl with q.
Qed.

Lemma T_ext_tree `{B:Type}: forall t, @is_tree B (T_ext t).
Proof.
  intros t u Hu v Hv.
  destruct (cat_prefix _ _ Hv);subst.
  now apply T_ext_closed with x.
Qed.
        
Inductive realises `{B:Type}: tree_int -> (B° -> Prop) -> Prop:=
| real_leaf: forall T, not (T nil) ->  realises Leaf T
| real_node:forall (T:B°->Prop) f,  (T nil ) ->  (forall a, realises (f a) (fun u => T ((nil ⋆ a)@u)))
                                       -> realises (Node f) T.


Lemma realises_entails `{B:Type}:
  forall (T U:B° -> Prop) t, (forall u, T u <-> U u) -> realises t T-> realises t U.
Proof.
  intros T U t;revert T U;induction t;intros.
  - inversion H0. apply real_leaf. intro. now apply H1,H.
  - inversion H1;subst.
    apply real_node;intros.
    + now apply H0.
    + apply H with ((fun u : B ° => T ((nil ⋆ a) @ u)));[|apply H4].
      intros u. split;intros;now apply H0.
Qed.

Lemma T_ext_realises `{B:Type}:
  forall (t:@tree_int B), realises t (T_ext t).
Proof.
  intros t.
  induction t;simpl.
  - now apply real_leaf.
  - apply real_node;[now left|].
    intros a. apply (realises_entails (T_ext (t a)));[|apply H].
    intros u;split;intro Hu.
    right. exists a; exists u;split;trivial.
    destruct Hu as [Hu|(b,(v,(Hb,Hv)))].
    + apply cat_nil_nil in Hu as (Ha,Hu). inversion Ha.
    + apply cat_inv_l in Hb as (Ha,Hu). now subst.
Qed.


(** ** Arborification *)


Definition down_arborification `{B:Type} (T:B° -> Prop):=
  fun u => forall u',(u' ≤s u -> T u' ).

Definition up_arborification `{B:Type} (T:B° -> Prop):=
  fun u => exists u',(u≤s u' /\ T u').

Definition down_monotonisation `{B:Type} (T:B° -> Prop):=
  fun u => forall u',(u ≤s u' -> T u').

Definition up_monotonisation `{B:Type} (T:B° -> Prop):=
  fun u => exists u',(u'≤s u /\ T u').

Notation "↓- T":=(down_arborification T) (at level 41).
Notation "↑- T":=(up_arborification T) (at level 41).
Notation "↑+ T":=(up_monotonisation T) (at level 41).
Notation "↓+ T":=(down_monotonisation T) (at level 41).


Lemma down_arb_tree `{B:Type} (T:B° -> Prop):
  is_tree (↓- T).
Proof.  
  intros u H v Hv.
  intros u' Hu'.
  apply H. now apply prefix_trans with v.
Qed.

Lemma up_arb_tree `{B:Type} (T:B° -> Prop):
  is_tree (↑- T).
Proof.
  intros u [u' [Hu Hu']] v Hv.
  exists u';split;[|assumption].
  now apply prefix_trans with u.
Qed.


Lemma down_mon_mon `{B:Type} (T:B° -> Prop):
  is_monotone (↓+ T).
Proof.
  intros u H v Hv.
  intros u' Hu'.
  apply H. now apply prefix_trans with v.
Qed.

Lemma up_mon_mon `{B:Type} (T:B° -> Prop):
  is_monotone (↑+ T).
Proof.
  intros u [u' [Hu Hu']] v Hv.
  exists u';split;[|assumption].
  now apply prefix_trans with u.
Qed.


Lemma down_arb_inc `{A:Type}  (T:A° -> Prop):
  ↓-T ⊆ T.
Proof.
  intros u H. apply (H u),prefix_id.
Qed.

Lemma tree_down_arb_id `{A:Type}  (T:A° -> Prop):
  is_tree T -> ↓-T ≅ T.
Proof.
  intros Tree; split;intros u H.
  - simpl in H. apply H,prefix_id.
  - intros v Hv;unfold is_tree in *. apply (Tree u H _  Hv).
Qed.

Lemma up_arb_inc `{A:Type}  (T:A° -> Prop):
  T ⊆ ↑- T.
Proof.
  intros u H. exists u;split;trivial. apply prefix_id.
Qed.


Lemma tree_up_arb_id `{A:Type}  (T:A° -> Prop):
  is_tree T -> ↑-T ≅ T.
Proof.
  intros Tree; split;intros u H.
  - simpl in H. destruct H as (v,(Hv,Tv)). apply (Tree v Tv _ Hv). 
  - exists u;split;[apply prefix_id|assumption]. 
Qed.


Lemma monotone_down_mon_id `{A:Type}  (T:A° -> Prop):
  is_monotone T ->  ↓+T ≅ T.
Proof.
  intros Mon; split;intros u H.
  - simpl in H. apply H,prefix_id.
  - intros v Hv. apply (Mon u H _  Hv).
Qed.

Lemma  monotone_up_mon_id `{A:Type}  (T:A° -> Prop):
  is_monotone T ->  ↑+T ≅ T.
Proof.
  intros Mon; split;intros u H.
  - simpl in H. destruct H as (v,(Hv,Tv)). apply (Mon v Tv _ Hv). 
  - exists u;split;[apply prefix_id|assumption]. 
Qed.



Definition leq_map  `{A:Type}  (X Y:A° -> Prop):=forall u, X u -> Y u.
Definition monotone_map  `{A:Type} F:=forall (X Y:A° -> Prop), leq_map X Y -> @leq_map A (F X) (F Y).


(** *  Definitions    *)

(** ** Progressing  *)

Definition progressing_at `{A:Type} (T:A° -> Prop) u:=
  T u -> exists (a:A), T (u ⋆ a).

Definition progressing `{A:Type} (T:A° -> Prop) :=
  forall u, progressing_at T u.


(** ** Hereditary *)

Definition hereditary_at `{A:Type} (T:A° -> Prop) u:=
  (forall (a:A), T (u ⋆ a)) -> T u .

Definition hereditary `{A:Type} (T:A° -> Prop) :=
  forall u, hereditary_at T u.




(** * TABLE V  *)

(** ** Closure operators *)

Definition hereditary_map  `{A:Type} T :=
  fun (X:A° -> Prop) u =>  T u \/ forall (a:A), X (u ⋆ a) .


Lemma monotone_hereditary `{A:Type} T:@monotone_map A (hereditary_map T).
Proof.
  intros X Y H u Hu.
  destruct Hu.
  - now left.
  - right.
    intro a.
    apply H.
    now apply H0.
Qed.

Inductive hereditary_closure  `{A:Type} T:A° -> Prop  :=
|hereditary_self : forall u, T u -> hereditary_closure T u
|hereditary_sons :  forall u,
    (forall (a:A), hereditary_closure T (u ⋆ a)) ->
    hereditary_closure T u.

(** Equivalently, we could define the hereditary closure via 
the following application of the map, which would be less convenient
since the two disjunctive cases are not tied to a specific constructor. *)

(* Inductive hereditary_closure `{A:Type} T:A° -> Prop := *)
(*   hereditary_case: forall u,  hereditary_map T (@hereditary2 A T) u -> hereditary_closure T u. *)


Definition pruning_map `{A:Type} T :=
  fun X u => T u /\ exists (a:A), X (u ⋆ a) .

Lemma monotone_pruning `{A:Type} T:@monotone_map A (pruning_map T).
Proof.
  intros X Y H u (HT,(a,HX)).
  split;auto;exists a;auto. 
Qed.


CoInductive pruning `{A:Type} T:A° -> Prop :=
  prune: forall u,
    (T u  /\ exists (a:A), @pruning A T (u ⋆ a)) ->
    pruning T u.

(** Equivalent definition *)

(* CoInductive pruning `{A:Type} T:A° -> Prop := *)
(*   prune: forall u,  pruning_map T (@pruning A T) u -> pruning T u. *)






(** ** Intensional concepts  *)

Definition spread `{A:Type} (T:A° -> Prop):=
  T nil /\ progressing T.

Definition productive `{A:Type} (T:A° -> Prop):=
  pruning T nil.

Definition barricaded `{A:Type} (T:A° -> Prop):=
  hereditary T -> T nil.

Definition inductively_barred `{A:Type} (T:A° -> Prop):=
  hereditary_closure T nil.


(** ** Intensional concepts, finite case  *)

Definition has_unbounded_paths `{A:Type} (T:A° -> Prop):=
  forall n, exists u, (#u) = n /\  (↓-T) u.
  
Definition staged_infinite `{A:Type} (T:A° -> Prop):=
  forall n, exists u, (#u) = n /\  T u.

Definition uniformly_barred `{A:Type} (T:A° -> Prop):=  
  exists n, forall u, (#u) = n ->  (↑+ T) u.

Definition staged_barred `{A:Type} (T:A° -> Prop):=  
  exists n, forall u, (#u) = n -> T u.


(** ** Extensional concepts *)

Definition has_infinite_branch `{A:Type} (T:A° -> Prop):=
  exists α, forall u, u ≺ α -> T u.


Definition barred `{A:Type} (T:A° -> Prop):=
  forall α, exists u, u ≺ α /\ T u.


(** Proposition 2 *)

Proposition unbounded_staged_inf `{A:Type} (T:A° -> Prop):
  is_tree T -> (has_unbounded_paths T <-> staged_infinite T).
Proof.
  (* unfold has_unbounded_paths, staged_infinite. *)
  intros Tree ; split ; intros H n.
  - destruct (H n) as (u,(Hu,Tu)).
    exists u.
    split ; [assumption | ].
    apply tree_down_arb_id.
    + assumption.
    + assumption.
  - destruct (H n) as (u,(Hu,Tu)).
    exists u.
    split ; [assumption | ].
    apply tree_down_arb_id.
    + assumption.
    + assumption.
Qed.



(** Proposition 3 *)

Proposition productive_pruning_spread `{A:Type} (T:A° -> Prop):
  productive T -> spread (pruning T).
Proof.
  intros H.  split. unfold productive in H. apply H.
  intros u Hu.  inversion Hu as (v,(Hv,(a,Ha))). subst. now exists a.
Qed.

Lemma pruning_progressing `{A:Type} (T:A° -> Prop):
  progressing T -> T ⊆ pruning T.
Proof.
  intros H.
  cofix IH.
  intros u Hu.  
  destruct (H _ Hu) as (a,Ha).
  apply prune;split;trivial.
  exists a.
  now apply IH.
Qed.

Lemma pruning_inc `{A:Type} (T:A° -> Prop):
  pruning T ⊆ T.
Proof.
  intros u (v,(Hu,lala)).
  assumption.
Qed.


(** Proposition 4 *)

Proposition spread_productive `{A:Type} (T:A° -> Prop):
  spread T -> productive T.
Proof.
  intros (Hnil,H).
  now apply pruning_progressing.
Qed.  


Lemma inc_hereditary `{A:Type} (T:A° -> Prop):
  T ⊆ hereditary_closure T.
Proof.
  intros t Ht.
  now econstructor.
Qed. 

Lemma hereditary_inc `{A:Type} (T:A° -> Prop):
  hereditary T -> hereditary_closure T ⊆ T.
Proof.
  intros H u Hu.
  induction Hu.
  - assumption.
  - apply H.
    assumption.
Qed.

Proposition ind_barred_barricaded `{A:Type} (T:A° -> Prop):
  inductively_barred T -> barricaded T.
Proof.
  intros H Her.
  apply hereditary_inc.
  - assumption.
  - unfold inductively_barred in H.
    assumption.
Qed.  


Lemma inc_pruning `{A:Type} (T U:A° -> Prop):
  T ⊆ U -> pruning T ⊆ pruning U.
Proof.
  intros H.
  cofix IH.
  intros v (u,(Hu,(a,Ha))).
  apply prune;split.
  - now apply H.
  - exists a. now apply IH.
Qed.

Lemma inc_productive `{A:Type} (T U:A° -> Prop):
  T ⊆ U -> productive T -> productive U.
Proof.
  intros H Prod.
  now apply (inc_pruning _ _ H).
Qed.


(** Proposition 5 *)

Proposition productive_spread  `{A:Type} (T:A° -> Prop):
  productive T <-> exists U, U ⊆ T /\ spread U.
Proof.
  split.
  - intros Prod. exists (pruning T);split.
    + apply pruning_inc.
    + now apply productive_pruning_spread.
  - intros (U,(HT,HU)).
    apply (inc_productive  _ _ HT).
    now apply spread_productive.
Qed.




(** * Relativisations *)


Definition productive_from `{A:Type} (T:A° -> Prop) u:=
  pruning T u.

Definition inductively_barred_from `{A:Type} (T:A° -> Prop) u:=
  hereditary_closure T u.

Definition has_unbounded_paths_from `{A:Type} (T:A° -> Prop) v:=
  forall n, exists u, (#u) = n /\  (↓- T) (v@u).
  

Definition uniformly_barred_from `{A:Type} (T:A° -> Prop) v:=  
  exists n, forall u, (#u) = n -> (↑+ T) (v@u).

Definition has_infinite_branch_from `{A:Type} (T:A° -> Prop) v:=
  exists α, forall u, u ≺ α -> T (v@u).

Definition barred_from `{A:Type} (T:A° -> Prop) v:=
  forall α, exists u, u ≺ α /\ T (v@u).





(** Proposition 6 *)

(* We restrict to the cas of trees branching on bool *)

Fixpoint n_a `{A:Type} (a:A) (n:nat) :=
  match n with 0 => (nil ⋆ a) | S m => n_a a m ⋆ a end.

Lemma len_n_a `{A:Type} (a:A):
  forall n, #(n_a a n) = S n.
Proof.
  induction n.
  - now cbv.
  - simpl. now f_equal.
Qed.

(* The generalization of proposition 6 as stated in the proof is false 
essentially because of the following : *)

Lemma pruning_down_arb `{A:Type}:
 not_empty A -> not (forall (T:A°->Prop),pruning T ⊆ ↓-T).
Proof.
  intros (a,_) H.
  pose (T:= fun u:A° => (#u) > 0).
  assert (forall n, pruning T (n_a a n)).
  - cofix IH.
    intros n.
    apply prune;split.
    + unfold T; rewrite len_n_a. lia.
    + exists a. replace (n_a a n ⋆ a) with (n_a a (S n)) by reflexivity.
      apply IH.
  - assert (T nil).
    apply (H T _ (H0 0)). apply prefix_cons,prefix_id.
    cbv in H1. inversion H1.
Qed.

Proposition not_productive_unbounded_paths:
  exists (T:bool°-> Prop) u,  productive_from T u /\ not (has_unbounded_paths_from T u).
Proof.
  pose (T:=fun u:bool° => (#u) > 0).
  exists T. exists (nil ⋆ true). split.
  - assert (forall n, pruning T (n_a true n)).
    + cofix IH.
      intros n. apply prune;split.
      * unfold T; rewrite len_n_a. lia.
      * exists true. replace (n_a true n ⋆ true) with (n_a true (S n)) by reflexivity.
        apply IH.
    + apply (H 0).
  - intro H. destruct (H 0) as (u,(Len,Hu)).
    destruct u;simpl in*.
    2:{ now inversion Len. }
    assert (T nil). apply Hu.  apply prefix_cons,prefix_id.
    cbv in H0. inversion H0.
Qed.

(* Nonetheless it can be fixed by strengthening the hypotheses to
further assume that u ∈ (↓- T). *)

Proposition productive_unbounded_paths_fixed  (T:bool° -> Prop):
  forall u, (↓- T) u ->  productive_from T u  -> has_unbounded_paths_from T u.
Proof.
  intros u Tu Prod n.  revert Prod. revert Tu. revert u. induction n;intros u Tu Prod.
  - destruct Prod as (u,(Hu,(a,Ha))).
    exists nil;split;trivial;simpl. 
  - destruct Prod as (u,(Hu,(a,Ha))).
    assert (Hua: (↓-T) (u⋆a)).
    + intros v Hv. inversion Hv;subst.
      * now inversion Ha as (H1,(H2,_));subst.
      * now apply Tu.
    + destruct (IHn _ Hua Ha) as (w,(Len,Hw)).
      exists ((nil⋆ a)@w);split. 
     * rewrite len_cat,Len. reflexivity.
     * rewrite cat_comm. apply Hw.
Qed.

Proposition unbounded_paths_productive  (T:bool° -> Prop):
  (forall (P Q: nat -> Prop),
      (forall x y, P x \/ Q y) ->
      (forall x, P x) \/ forall y, Q y) ->
  (* is_tree T ->*)
  forall u,  has_unbounded_paths_from T u -> productive_from T u.
Proof.
  intros D. (* tree.*)
  cofix IH. intros.
  unfold productive_from.
  unfold has_unbounded_paths_from in H.
  apply prune;split.
  + destruct (H 0) as (v,(Len,Hv)).
    destruct v;[|inversion Len].
    apply (Hv u). apply prefix_id.
  + cut (exists b, has_unbounded_paths_from T (u⋆b)).
    intros (b,Hb). exists b. apply (IH _ Hb).
    assert (forall n0 n1 : nat,
                 (exists u0 : bool °,  # u0 = n0 /\ ((↓- T) ((u ⋆ true) @ u0)))
                 \/ exists u1 : bool °, # u1 = n1 /\ (↓- T) ((u ⋆ false) @ u1)).      
    intros n0 n1.
    destruct (H (S(max n0 n1))) as (v,(Len,Hv)).
    destruct (nonemptylist_cat v) as (b,(w,Hw));subst.
    rewrite Len;lia.
    rewrite cat_comm in Hv;simpl in Hv.
    rewrite len_cat in Len;simpl in Len.
    inversion Len as (Lenw).
    destruct (cut_list w n0) as (u0,(w0,(H0,Len0)));[lia|] .
    destruct (cut_list w n1) as (u1,(w1,(H1,Len1)));[lia|].
    destruct b;[left;exists u0|right;exists u1];repeat split;trivial;
    intros u' H'; apply Hv; apply (prefix_trans _ _ _ H').
    * rewrite H0. replace u0 with (u0 @ nil) at 1 by reflexivity.
      do 2 apply prefix_cat. apply prefix_nil.
    * rewrite H1. replace u1 with (u1 @ nil) at 1 by reflexivity.
      do 2 apply prefix_cat. apply prefix_nil.
    * destruct (D _ _ H0);[exists true|exists false];intros n;apply H1.
Qed.



Inductive Dialogue (I : Type)(O : I -> Type) (A : Type) :=
| eta : forall (a : A), Dialogue I O A
| beta : forall (i : I) (k : O i -> Dialogue I O A),
    Dialogue I O A.

Fixpoint deval {I O A} (alpha : forall i : I, O i) (d : Dialogue I O A) : A :=
  match d with
  | eta _ _ _ a => a
  | beta _ _ _  i k => deval alpha (k (alpha i))
  end.

(*instead of proving 
  forall (f : forall i : I, not (not P i)), 
         not (not (forall i : I, P i)),
  we prove the intuitionistically equivalent
  forall (f : not (forall i : I, P i)),
         not (forall i, not (not P i)).*)

Lemma test (f : (forall n : nat, n = 0) -> False)
      (d : Dialogue nat (fun n => n = 0) False)
      (Hyp : forall alpha, f alpha = deval alpha d)
  : (forall n : nat, (n = 0 -> False) -> False) -> False.
Proof.
  cbn in *.
  clear Hyp f.
  intro h.
  induction d.
  - assumption.
  - cbn in *.
    eapply h.
    exact H.
Qed.    
(** * Tree-based DC & BI principles *)



Definition injection `{A:Type}  (f:A -> nat) :=
  forall a a', a <> a' -> f a <> f a'.

Definition bounded `{A:Type} (f:A -> nat) :=
  exists n, forall a, f a <= n.

(* probably very naive *)
Definition finite (A:Type) := exists (f:A->nat), injection f /\ bounded f.


Definition DC_spread `{B:Type} (T:B°->Prop):=
  spread T -> has_infinite_branch T.

Definition DC_productive `{B:Type} (T:B°->Prop):=
  productive T -> has_infinite_branch T.

Definition BI_barricaded `{B:Type} (T:B°->Prop):=
  barred T -> barricaded T.

Definition BI_ind `{B:Type} (T:B°->Prop):=
  barred T -> inductively_barred T.


(** ** Finite case *)


Definition KL_spread `{B:Type} (T:B°->Prop):=
  finite B -> DC_spread T.

Definition KL_productive `{B:Type} (T:B°->Prop):=
  finite B -> DC_productive T.

Definition KL_unbounded `{B:Type} (T:B°->Prop):=
  finite B -> has_unbounded_paths T -> has_infinite_branch T.

Definition KL_staged `{B:Type} (T:B°->Prop):=
  finite B -> is_tree T -> staged_infinite T -> has_infinite_branch T.


Definition FT_barricaded `{B:Type} (T:B°->Prop):=
  finite B -> BI_barricaded T.

Definition FT_ind `{B:Type} (T:B°->Prop):=
  finite B -> BI_ind T.

Definition FT_uniform `{B:Type} (T:B°->Prop):=
  finite B -> barred T -> uniformly_barred T.

Definition FT_staged `{B:Type} (T:B°->Prop):=
  finite B -> barred T -> is_monotone T -> staged_barred T.



(** ** Theorem 1 *)

Theorem DC_spread_prod `{B:Type}:
  (forall (T:B°-> Prop), DC_spread T) <->  (forall (T:B°-> Prop), DC_productive T).
Proof.
  split;intros DC T H.
  - apply productive_spread in H.
    destruct H as (U,(Inc,HU)).
    destruct (DC U HU) as (α,Hα).
    exists α. intros u Hu. apply (Inc _ (Hα u Hu)).
  - now apply DC,spread_productive.
Qed.


(** ** Proposition 7 *)

Proposition KL_staged_unbounded `{B:Type}:
  (forall (T:B°-> Prop), KL_staged T) <->  (forall (T:B°-> Prop), KL_unbounded T).
Proof.
  split;intros KL T Fin H.
  - destruct (KL (↓- T) Fin (down_arb_tree T) H) as (α,Hα).
    exists α. intros u Hu. now apply down_arb_inc,Hα.
  - intros Staged. apply unbounded_staged_inf in Staged;trivial.
    now apply KL.
Qed.



(** ** Theorem 2 *)


Theorem inf_branch_prod `{B:Type} (T:B° -> Prop) :
  forall u,  has_infinite_branch_from T u -> productive_from T u.
Proof.
  cofix IH.
  intros u (α,Hα). apply prune. split.
  - apply (Hα nil (prefix_seq_nil _)).
  - exists (α 0). apply IH.
    pose (ß := fun n => α (S n)).
    exists ß. intros v Hv.
    replace ((u ⋆ α 0) @ v) with ((u @ (nil ⋆ α 0)) @ v) by reflexivity.
    rewrite <- cat_comm. apply Hα.
    induction v.
    + simpl. apply prefix_seq_cons;trivial. apply prefix_seq_nil.
    + simpl. inversion Hv;subst. apply prefix_seq_cons;trivial.
      * now apply IHv. 
      * rewrite len_cat;simpl. reflexivity.
Qed.



(** * Relational versions *)

Definition serial `{B:Type} (R:B -> B -> Prop):=
  forall b, exists b', R b b'.

Definition left_not_full `{B:Type} (R:B -> B -> Prop):=
  forall b, exists b', not (R b b').

Definition has_least_element `{B:Type} (R:B -> B -> Prop):=
  exists b, forall b', R b b'.

Definition has_maximal_element `{B:Type} (R:B -> B -> Prop):=
  exists b, forall b', not (R b b').


  
CoInductive chaining_from `{B:Type} (R:B -> B -> Prop): B -> B° -> Prop:=
  chain_nil : forall b, chaining_from R b nil
| chain_step : forall b b' u, R b b' -> chaining_from R b' u -> chaining_from R b (nil⋆ b' @ u).                                 

Definition alignment_from `{B:Type} (R:B -> B -> Prop) (b0:B):=
  fun u => match u with
        | nil => True
        | nil ⋆ b => R b0 b
        | u' ⋆ b ⋆ b' => R b b'
        end.


Notation " R ⊤{ b *}" := (chaining_from R b) (at level 71).
Notation " R ⊤{ b ▹}" := (alignment_from R b) (at level 71).


Inductive antichain_from `{B:Type} (R:B -> B -> Prop): B -> B° -> Prop:=
| antichain_step : forall b b' u, (R b b' \/ antichain_from R b' u) -> antichain_from R b (nil⋆ b' @ u).                                 

Definition blocking_from `{B:Type} (R:B -> B -> Prop) (b0:B):=
  fun u => match u with
        | nil => False
        | nil ⋆ b => R b0 b
        | u' ⋆ b ⋆ b' => R b b'
        end.


Notation " R ⊥{ b *}" := (antichain_from R b) (at level 71).
Notation " R ⊥{ b ▹}" := (blocking_from R b) (at level 71).





(** Proposition 8 *)


Lemma chaining_is_tree `{B:Type}:
 forall R (b0:B), is_tree (R⊤{b0*}).
Proof.
  intros R b u Hu v Hv.
  revert Hv Hu. revert v b. revert u.
  apply (list_ind_rev (fun u => forall v b, v ≤s u -> (R ⊤{ b *}) u -> (R ⊤{ b *}) v)).
  - intros v b Hv H;now inversion Hv. 
  - intros a l IH v b Hv Hu.
    inversion Hu;[destruct l;inversion H1; simpl in H1|].
    destruct (cat_inv_l _ _ _ _ H);subst. clear H.
    destruct (cat_prefix _ _ Hv) as (w,Hw).
    destruct v. apply chain_nil.
    destruct (nonemptylist_cat (v ⋆ b0)) as (x,(z,Hz));[simpl;lia|].
    rewrite Hz in *.
    rewrite <- cat_comm in Hw. destruct (cat_inv_l _ _ _ _ Hw);subst.
    apply chain_step;trivial.
    apply IH. replace z with (z @ nil) at 1 by reflexivity.
    apply prefix_cat,prefix_nil.
    assumption. 
Qed. 


Lemma chaining_cat `{B:Type} :
  forall R (b0:B) u v a, (R ⊤{ b0 *}) (u ⋆ a @ v) ->  (R ⊤{ b0 *}) u /\ (R ⊤{ a*}) v.
Proof.
  intros R b0 u. revert b0. revert R.
  apply (list_ind_rev (fun u =>
                         forall R b0 v a, (R ⊤{ b0 *}) ((u ⋆ a) @ v) -> (R ⊤{ b0 *}) u /\ (R ⊤{ a *}) v)).
  - intros. inversion H.
    + exfalso. now apply nil_nonempty in H2. 
    + destruct (cat_inv_l _ _ _ _ H0);subst.
      split. apply chain_nil. assumption.
  - intros.
    replace (((((nil ⋆ a) @ l) ⋆ a0) @ v)) with ((nil ⋆ a) @ ((l ⋆ a0) @ v)) in H0
        by (now rewrite cat_comm).
    inversion H0.
    + exfalso. now apply nil_nonempty in H3. 
    + destruct (cat_inv_l _ _ _ _ H1);subst.
      rewrite cat_comm in H0. simpl in H0.
      destruct (H _ _ _ _ H4).
      split;trivial.
      now apply chain_step. 
Qed.




Lemma chaining_alignment_inc `{B:Type} :
  forall R (b0:B), (R ⊤{ b0 *}) ⊆ (R ⊤{ b0▹}).
Proof.
  intros R b0 u H.
  destruct u;[now simpl|]. 
  destruct u.
  + simpl. inversion H.
    rewrite <- (cat_nil_r (nil⋆b)) in H0.
    destruct (cat_inv_l _ _ _ _ H0);now subst.
  + replace (u ⋆ b1 ⋆ b) with ((u ⋆ b1)@(nil ⋆ b)) in H by reflexivity.
    destruct (chaining_cat _ _ _ _ _ H) as (Hu,Hb).
    inversion Hb;subst.
    rewrite <- (cat_nil_r (nil⋆b)) in H0.
    destruct (cat_inv_l _ _ _ _ H0);now subst.
Qed.



Proposition chaining_arb_alignement `{B:Type}:
 forall R (b0:B) u, R⊤{b0*} u <-> (↓- (R⊤{ b0 ▹} )) u.
Proof.
  intros R b0 u. split;intros. 
  - intros v Hv.
    apply chaining_alignment_inc.
    now apply chaining_is_tree with u.
  - revert H. revert b0. revert u.
    apply (list_ind_rev (fun u => forall b0, (↓- (R ⊤{ b0▹})) u -> (R ⊤{ b0 *}) u)).
    + intros;apply chain_nil.  
    + intros a l IHl b0 H. apply chain_step.
      apply (H (nil ⋆ a) (prefix_cat _ _ _ (prefix_nil _ ))).
      apply IHl.
      intros v Hv. 
      destruct v;[now simpl|destruct v;simpl].
      * apply (H (nil ⋆ a ⋆ b)).
        apply (prefix_cat _ _ _ Hv).
      * apply (H ((nil ⋆ a) @ (v⋆b1 ⋆ b))).
        apply (prefix_cat _ _ _ Hv).
Qed.

  
Definition DC_serial `{B:Type} (R:B -> B -> Prop) (b0:B):=
  serial R -> has_infinite_branch (alignment_from R b0).


Definition BI_least `{B:Type} (R:B -> B -> Prop) (b0:B):=
  barred (blocking_from R b0) -> has_least_element R.
