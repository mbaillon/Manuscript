From mathcomp Require Import all_ssreflect.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Require Import List Lia PeanoNat.
Import ListNotations.

Require Import ConstructiveEpsilon.


Section fix_types.

  (* I removed *)
  Definition I := nat.
Variable A : Type.
Variable O : I -> Type.

(* Definition 1: Dialogue continuity à la PMP *)
Inductive C : ((forall i, O i) -> A) -> Type :=
  | etaC o : C (fun _ => o)
  | betaC (i : I) (k : O i -> (forall i, O i) -> A) (H : forall o, C (k o))
      : C (fun f => k (f i) f).

(* Definition 2: inductive dialogue trees *)
Inductive dialogue :=
  | eta (A : A)
  | beta (i : I) (k : O i -> dialogue).

(* Arguments exchanged compared to the main file *)
Fixpoint eval_dialogue (d : dialogue) (f : forall i, O i) :=
  match d with
  | eta o => o
  | beta q k => eval_dialogue (k (f q)) f
  end.

Definition continuous_via_dialogues_ex F (d : dialogue) :=
  F =1 eval_dialogue d.

Definition continuous_via_dialogues F :=
  exists d, continuous_via_dialogues_ex F d.

(* 2 -> 1 modulo extensional_equality *)
Fixpoint dialogue_to_C (d : dialogue) : {F & C F}.
Proof.
  case: d=> [o|q k].
  - exists (fun _ => o).
    exact (etaC o).
  - set k' := fun a=> (let (k',_) := (dialogue_to_C (k a)) in k').
    exists (fun f => k' (f q) f).
    apply betaC=> a.
    rewrite/k' ; move: (dialogue_to_C (k a))=> []//.
Defined.

Proposition continuous_dialogue_to_C_correct d : {F & C F & F =1 eval_dialogue d}.
Proof.
  exists (projT1 (dialogue_to_C d)).
  - exact: projT2 (dialogue_to_C d).
  - elim: d => [//|q k /=] + f.
    move/(_ (f q)) ; move: (dialogue_to_C _)=> []//.
Qed.


Fixpoint C_to_dialogue (F : (forall i, O i) -> A) (CF : C F) : dialogue.
Proof.
  case: CF => [o|q k H].
  - exact (eta o).
  - exact (@beta q (fun a => (C_to_dialogue (k a) (H a)))).
Defined.

Proposition continuous_C_to_dialogue :
  forall F : (forall i, O i) -> A,
    C F -> continuous_via_dialogues F.
Proof.
  move=> F CF.
  exists (C_to_dialogue CF)=> f.
  elim: CF=> [o|q k H] //=.
Qed.


(* Definition 3: extensional dialogues *)
Notation result I A := (option (I + A))%type.
Notation ask i := (Some (inl i)).
Notation reject := None.
Notation output a := (Some (inr a)).

Record duo :=
  { qur : I ;
    ans : O qur
  }.

Definition ext_tree := list duo -> result I A.

Definition sub_ext_tree (τ : ext_tree) a := (fun l => τ (a::l)).

Record is_total_tree (τ : ext_tree) := {
  is_not_empty' : τ [] <> reject ;
  is_valid_nodes' : forall l,
    match τ l with
    | ask q => forall a, τ (rcons l a) <> reject
    | _ => forall a, τ (rcons l a) = reject
    end
}.

Fixpoint iota' (f : nat -> (forall i, O i)) (m n : nat) {struct n} : list duo.
Proof.
  refine (match n with
          | 0 => []
          | S k => _
          end).
  unshelve eapply cons.
  exists m.
  eapply f.
  exact m.
  exact (iota' f (S m) k).
Defined.


Definition well_founded (τ : ext_tree) :=
  forall f :  nat -> (forall i, O i), exists n o,
    τ ((iota' f 0 n)) = output o.

Fixpoint eval_ext_tree2 (τ : ext_tree) (f : forall i, O i) (n : nat)
  (l : list duo): result I A.
Proof.
  refine (match n with
          | 0 => τ l
          | S k => _
          end).
  - refine (match τ l with
            | ask q => eval_ext_tree2 τ f k (rcons l _)
            | output o => reject
            | reject => reject
            end).
    exists q.
    exact (f q).
Defined.

Fixpoint eval_ext_tree (τ : ext_tree) (f : forall i, O i) (n : nat) : result I A.
Proof.
  refine ( match τ [] with
  | ask q => _
  | output o => if n is S n' then reject else output o
  | reject => reject
           end).
  refine (if n is S n' then _ else ask q).
  refine (eval_ext_tree (sub_ext_tree τ _) f n').
  exists q.
  now apply f.
Defined.

Lemma eval_ext_tree_eq : forall τ f n,
    eval_ext_tree τ f n = eval_ext_tree2 τ f n [].
Proof.
  intros.
  revert τ f.
  induction n.
  - intros.
    cbn.
    induction (τ []) ; try reflexivity.
    induction a ; try reflexivity.
  - cbn.
    intros.
    induction (τ []) ; cbn.
    destruct a.
    2, 3: reflexivity.
    erewrite IHn.
    clear IHn.
    generalize ({| qur := i; ans := f i |}).
    assert (forall l d,
               eval_ext_tree2 (sub_ext_tree τ d) f n l =
                 eval_ext_tree2 τ f n (cons d l)).
    induction n ; intros.
    + cbn.
      reflexivity.
    + cbn.
      unfold sub_ext_tree in *.
      destruct (τ (d :: l)%SEQ).
      destruct s.
      2,3: reflexivity.
      now erewrite IHn.
      eapply H.
Qed.

(* I guess the last part would imply well_foundedness *)
Definition continuous_via_ext_tree_ex F τ :=
  is_total_tree τ /\
  (*well_founded τ /\*)
  forall f, exists n, eval_ext_tree2 τ f n [] = output (F f).

Definition continuous_via_ext_tree F :=
  exists τ, continuous_via_ext_tree_ex F τ.

Definition weak_continuous_via_ext_tree_ex F τ :=
  forall f, exists n, eval_ext_tree2 τ f n [] = output (F f).

Definition weak_continuous_via_ext_tree F :=
  exists τ, weak_continuous_via_ext_tree_ex F τ.

(* Conjecture : other direction *)

Fixpoint to_total (a : A) τ l :=
  match τ [], l with
  | reject, [] => output a
  | α, [] => α
  | ask q, o::l' => to_total a (sub_ext_tree τ o) l'
  | _, _ => reject
  end.

Lemma to_total_correct o0 τ : is_total_tree (to_total o0 τ) (*/\ (to_total o0 τ) = τ.*).
Proof.
  split.
    + unfold to_total.
      destruct (τ []).
      * now destruct s.
      * now trivial.
    + intros l.
      revert τ.
      induction l ; intros.
      * unfold to_total.
        remember (τ []).
        destruct o ; cbn.
        -- destruct s ; intros ; cbn.
           rewrite <- Heqo.
           unfold sub_ext_tree.
           destruct (τ [a]) ; try now auto.
           now destruct s.
           rewrite <- Heqo ; reflexivity.
           intros ; rewrite <- Heqo ; reflexivity.
      * remember (τ []).
        destruct o ; cbn.
        rewrite <- Heqo ; destruct s ; try now auto.
        exact (IHl (sub_ext_tree τ a)).
        now rewrite <- Heqo.
Qed.

Section to_well_founded_aux.

Variable eq_q : rel I.
Hypothesis eq_q_correct : forall q1 q2, reflect (q1 = q2) (eq_q q1 q2).
(*
Fixpoint to_well_founded_aux (f : forall i, option (O i)) τ l :=
  match τ [], l with
  | α, [] => α
  | ask q, a :: l' =>
    if f q is Some a' then
      to_well_founded_aux f (sub_ext_tree τ {| qur := q ; ans := a' |}) l'
    else
      to_well_founded_aux (fun q' => if eq_q q q' then Some a else None)
        (sub_ext_tree τ a) l'
  | _, _ => reject
  end.

Definition to_well_founded := to_well_founded_aux (fun=> None).

Lemma to_well_founded_correct τ : well_founded (to_well_founded τ) /\ eval_ext_tree τ =2 eval_ext_tree (to_well_founded τ).
Abort.

Theorem unweak_continuous_via_ext_tree_ex F :
  weak_continuous_via_ext_tree F -> continuous_via_ext_tree F.
Abort.
*)
End to_well_founded_aux.
(* End conjecture *)


Fixpoint dialogue_to_ext_tree2 (d : dialogue) (l : list duo) {struct l} : result I A.
Proof.
  refine ( match l, d with
           | [], eta o => output o
           | [], beta q _ => ask q
           | _, eta _ => reject
           | a::l, beta _ k => _
           end).
  Print PeanoNat.Nat.eq_dec.
  refine (match (PeanoNat.Nat.eq_dec i a.(qur)) with
          | left _ => _
          | right _ => _
          end).
  + subst.
    eapply dialogue_to_ext_tree2.
    eapply k.
    exact a.(ans).
    exact l.
  + refine (dialogue_to_ext_tree2 d l).
Defined.
Unset Printing Notations.
Print eq.

Lemma test2 : forall (x y : bool), x = y -> y = x.
Proof.
  intros x y e.
  pose (zef := @eq_rect bool x (fun y => eq y x) (Logic.eq_refl x) y e).
  cbn in *.
  induction e.
  reflexivity.
Defined.
Print test2. e).

Lemma test : true = false -> False.
Proof.
  intro e.
  Print True.
  Print bool_rect.
  pose (zfe := @eq_rect bool true (bool_rect (fun _ => Type) True False) Logic.I false).
  About Nat.eq_decidable.
  About Nat.compare_refl.
  change (bool_rect (fun _ => Type) True False false).
  induction e.
  now econstructor.
Defined.
Print test.

Fixpoint dialogue_to_ext_tree (d : dialogue) (l : list duo) {struct l} : result I A.
Proof.
  refine (match d with
          | eta a => _
          | beta i k => _
          end).
  + refine (match l with
            | [] => output a
            | _ => reject
            end).
  + refine (match l with
            | [] => ask i
            | cons o q => _
            end).
  + 
  
  refine ( match l, d with
           | [], eta o => output o
           | [], beta q _ => ask q
           | _, eta _ => reject
           | a::l, beta _ k => _
           end).
  refine (match (PeanoNat.Nat.eq_dec i a.(qur)) with
          | left _ => _
          | right _ => _
          end).
  + subst.
    eapply dialogue_to_ext_tree.
    eapply k.
    exact a.(ans).
    exact l.
  + refine (dialogue_to_ext_tree d l).
Defined.

(*  := fun l =>
  match l, d with
  | [], eta o => output o
  | [], beta q _ => ask q
  | _, eta _ => reject
  | a::l, beta _ k => dialogue_to_ext_tree (k a.(ans)) l
  end.*)

Lemma continous_dialogue_to_ext_tree : forall F : (forall i, O i) -> A,
    continuous_via_dialogues F ->
    continuous_via_ext_tree F.
Proof.
  intros F [d H].
  exists (dialogue_to_ext_tree d) ; split.
  split.
  - induction d ; done.
  - clear H.
    intros l.
    revert d.
    induction l ; intros.
    1:{ induction d ; cbn ; try now auto.
        intros [] ; cbn in *.
        destruct (Nat.eq_dec i (qur0)) ; try now auto.
        destruct e ; cbn.
        destruct (k ans0) ; now auto.
    }
    induction d ; try now auto.
    { cbn.
      destruct (Nat.eq_dec i (qur a)) ; cbn in *.
      2: now eapply IHl.
      destruct a ; cbn in *.
      destruct e ; cbn.
      now eapply IHl.
    }
    intros alpha.
    rewrite (H alpha) ; clear H.
    induction d as [ | * IH].
    + exists 0 ; done.
    + specialize (IH (alpha i)).
      destruct IH as [n H] ; cbn in *.
      rewrite <- H.
      exists (S n).
      cbn.
      assert (forall l,
                 eval_ext_tree2 (dialogue_to_ext_tree (beta k)) alpha n
                   (cons {| qur := i; ans := alpha i |} l) =
                   eval_ext_tree2 (dialogue_to_ext_tree (k (alpha i))) alpha n l).
      * clear.
        induction n ; intros.
        cbn.
        destruct (Nat.eq_dec i i).
        2:exfalso; done.
        cbn.
        destruct e.
        cbn.
        reflexivity.
      * cbn.
        destruct (Nat.eq_dec i i).
        2:exfalso; done.
        destruct e.
        cbn.
        destruct (dialogue_to_ext_tree (k (alpha i)) l).
        destruct s.
        2,3 : reflexivity.
        erewrite <- IHn. 
        reflexivity.
      * eapply H0.
Qed.      


(* Definition 4: modulus definition *)

(* Definition Forall T (P : T -> Prop) l := fold_right and True (map P l). *)

Definition modulus_on O' (F : (forall i, O i) -> O')
  (f : forall i, O i) (L : list I) :=
  forall g, Forall (fun q => f q = g q) L -> F f = F g.

Definition modulus_continuous O' (F : (forall i, O i) -> O') :=
  forall f, exists L, modulus_on F f L.

Definition modulus O' (F : (forall i, O i) -> O') (M : (forall i, O i) -> list I) :=
  forall f g, Forall (fun q => f q = g q) (M f) -> F f = F g.

Definition continuous_via_modulus O' (F : (forall i, O i) -> O') :=
  exists M, modulus F M.

Fixpoint eval_ext_tree_modulus2 (τ : ext_tree)
  (f : forall i, O i) (n : nat)
  (l : list duo): list I.
Proof.
  refine (match n with
          | 0 => []
          | S k => _
          end).
  - refine (match τ l with
            | ask q => cons q (eval_ext_tree_modulus2 τ f k (rcons l _))
            | output o => []
            | reject => []
            end).
    exists q.
    exact (f q).
Defined.

Fixpoint eval_ext_tree_modulus (τ : ext_tree) (f : forall i, O i) (n : nat) : list I :=
  match τ [], n with
  | ask q, S n' =>
      q :: (eval_ext_tree_modulus
              (fun l => τ ({| qur := q ; ans := (f q) |} :: l)) f n')
  | _, _ => []
  end.

Lemma unique_depth τ f n n' o o' l :
  eval_ext_tree2 τ f n l = output o ->
  eval_ext_tree2 τ f n' l = output o' ->
  n = n'.
Proof.
  revert l.
  elim: n n' τ => [|n H] [|n'] τ=> //=.
  all: intros l.
  1,2: case: (τ l)=> [|//] [] //.
  case: (τ l)=> [|//] [|//] ?.
  intros.
  unshelve erewrite (H n' τ _ H0 H1).
  reflexivity.
Qed.

Lemma continuous_ext_tree_to_modulus F :
  weak_continuous_via_ext_tree F -> modulus_continuous F.
Proof.
  intros [d H] alpha.
  destruct (H alpha) as [n Halpha].
  unshelve econstructor.
  - exact (eval_ext_tree_modulus2 d alpha n nil).
  - intros beta Hyp.
    destruct (H beta) as [m Hbeta].
    revert Halpha Hbeta Hyp.
    generalize (nil (T := duo)) ; intros.
    assert (n = m).
    { revert l m Halpha Hbeta Hyp.
      clear H.
      induction n ; intros.
      + induction m.
        * reflexivity.
        * cbn in *.
          rewrite Halpha in Hbeta.
          inversion Hbeta.
      + cbn in *.
        induction m.
        * cbn in *.
          rewrite Hbeta in Halpha.
          inversion Halpha.
        * cbn in *.
          destruct (d l).
          destruct s.
          cbn in *.
          unshelve erewrite (IHn _ m).
          2: reflexivity.
          3: eassumption.
          1,2: inversion Hyp ; subst ; rewrite <- H1.
          1,2: assumption.
          1,2: now inversion Halpha.
    }
    subst.
    assert (eval_ext_tree2 d alpha m l = eval_ext_tree2 d beta m l).
    { clear Halpha Hbeta.
      revert l Hyp.
      induction m ; intros.
      * cbn in *.
        reflexivity.
      * cbn in *.
        destruct (d l) ; cbn.
        destruct s.
        2,3 : reflexivity.
        inversion Hyp ; subst.
        rewrite <- H2.
        eapply IHm.
        exact H3.
    }
    rewrite Halpha in H0.
    rewrite Hbeta in H0.
    now injection H0.
Qed.        
        

    
Inductive interrogation (f : forall i, O i) :
  list A ->
  (list A -> option (Q + O)) ->
  Prop :=
  NoQuestions τ : interrogation f [] τ
| Ask l τ q a : f q = a ->
                interrogation f l τ ->
                τ l = Some (inl q) ->
                interrogation f (l ++ [a]) τ.

Definition continuous_via_interrogations_ex F τ :=
  forall f, exists ans, interrogation f ans τ /\ τ ans = output (F f).

Definition continuous_via_interrogations F :=
  exists τ, continuous_via_interrogations_ex F τ.

Lemma eval_ext_tree_ext τ τ' f n :
  (forall l, τ l = τ' l) ->
  eval_ext_tree τ f n = eval_ext_tree τ' f n.
Proof.
  intros Heq. induction n in τ, τ', Heq |- *.
  - cbn. rewrite Heq. reflexivity.
  - cbn. rewrite Heq. destruct τ' as [ [] | ]; eauto.
Qed.

Lemma interrogation_plus τ f n l  :
  interrogation f l τ ->
  eval_ext_tree (fun l' => τ (l ++ l')) f n = eval_ext_tree τ f (length l + n).
Proof.
  intros H. induction H in n |- *.
  - reflexivity.
  - rewrite app_length. cbn -[eval_ext_tree].
    replace ((length l + 1)%coq_nat + n)%Nrec with (length l + (S n)).
    2: admit.
    rewrite <- IHinterrogation.
    cbn. rewrite app_nil_r H1. eapply eval_ext_tree_ext.
    cbn. intros. now rewrite <- app_assoc, H. 
Admitted.

Lemma interrogation_cons q1 a1 a2 τ (f : Q -> A) :
  τ [] = Some (inl q1) ->
  f q1 = a1 ->
  interrogation f a2 (fun l => τ (a1 :: l)) ->
  interrogation f (a1 :: a2) τ.
Proof.
  intros H1 H2.
  induction a2 in a1, H1, H2 |- * using rev_ind.
  - inversion 1; subst.
    + eapply Ask with (l := []); eauto. constructor.
    + destruct l; cbn in *; congruence.
  - inversion 1.
    + destruct a2; cbn in *; congruence.
    + subst. eapply app_inj_tail in H0 as [<- <-].
      eapply Ask with (l := f q1 :: l); eauto.
Qed.

Lemma interrogation_ext τ τ' a f :
  (forall l, τ l = τ' l) ->
  interrogation f a τ <-> interrogation f a τ'.
Proof.
  intros. split; induction 1; econstructor; eauto; congruence.
Qed.

Lemma interrogation_app a1 a2 τ f :
  interrogation f a1 τ ->
  interrogation f a2 (fun l => τ (a1 ++ l)) ->
  interrogation f (a1 ++ a2) τ.
Proof.
  induction 1 in a2 |- *; cbn.
  - eauto.
  - intros. rewrite <- !app_assoc.
    eapply IHinterrogation.
    eapply interrogation_cons.
    + rewrite app_nil_r. eassumption.
    + eauto.
    + eapply interrogation_ext; eauto. cbn. intros. now rewrite <- app_assoc.
Qed.

Lemma eval_ext_tree_to_interrogation τ f n o :
  eval_ext_tree τ f n = output o ->
  exists ans, length ans <= n /\ interrogation f ans τ /\ τ ans = output o.
Proof.
  intros H.
  induction n in τ, H |- *.
  - cbn in *. exists []. repeat split. econstructor. destruct τ as [ [] | ]; eauto.
  - cbn in *. destruct τ as [ [] | ] eqn:E; try congruence.
    destruct (IHn (sub_ext_tree τ (f q))) as (ans & H3 & H1 & H2); eauto.
    exists (f q :: ans). repeat split.
    + admit.
    + eapply interrogation_cons; eauto. 
      now rewrite <- H2.
Admitted.

Lemma interrogation_equiv_eval_ext_tree τ f o :
  (exists (ans : list A), interrogation f ans τ /\ τ ans = output o) <->
    (exists n : nat, eval_ext_tree τ f n = output o).
Proof.
  split.
  + intros (ans & Hi & Hout).
    exists (length ans + 1). rewrite <- interrogation_plus; eauto.
    cbn. rewrite app_nil_r Hout. admit.
  + intros [n H]. eapply eval_ext_tree_to_interrogation in H as (? & ? & ? & ?); eauto.
Admitted.

Lemma continuous_via_interrogations_iff F τ :
  continuous_via_interrogations_ex F τ <-> weak_continuous_via_ext_tree_ex F τ.
Proof.
  split.
  - intros H f. eapply interrogation_equiv_eval_ext_tree. eapply H.
  - intros H f. eapply interrogation_equiv_eval_ext_tree. eapply H.
Qed.

End fix_types.

Section fix_types.

Definition Q := nat.

Variables (A O : Type).

Notation ask q := (Some (inl q)).
Notation reject := None.
Notation output o := (Some (inr o)).

(* Apparently F continuous for M and M continuous implies there (exist M', auto_modulus F M') (ref?) *)

Definition auto_modulus (F : (Q -> A) -> O) M :=
  modulus F M /\ modulus M M.

Definition extend (a0 : A) l := seq.nth a0 l.

Definition modulus_in (a0 : A) M l := all (fun i => i < size l) (M (extend a0 l)).
Definition modulus_exact (a0 : A) M l := has (fun i => i.+1 == size l) (M (extend a0 l)).

Lemma Forall_all T P (l : seq.seq T) : all P l <-> Forall P l.
Proof.
  elim: l=> // a l [] H1 H2 ; split=> [/=/andP[] ??|].
  - apply Forall_cons=> // ; apply H1 ; done.
  - move: {-1} l (erefl (a :: l))=> l' /[swap] ; case=> // a' l'' P_a' /[swap] [[]] _ <- /H2 ? ; apply/andP ; done.
Qed.

Lemma modulus_in_cat a0 M l a : modulus M M -> modulus_in a0 M l -> M (extend a0 l) = M (extend a0 (rcons l a)).
Proof.
  move=> mod_M mod_in ; apply mod_M.
  move: mod_in ; rewrite/modulus_in Forall_all.
  rewrite !Forall_forall => + i i_in ; move/(_ _ i_in).
  rewrite /extend nth_rcons ; case: ifP ; done.
Qed.

Definition modulus_to_ext_tree a0 (F : (Q -> A) -> O) M l :=
  let f := extend a0 l in
  let n := size l in
  if modulus_in a0 M l then
    if modulus_exact a0 M l then
      output (F f)
    else
      reject
  else
    ask n.


Lemma continuous_auto_modulus_to_ext_tree a0 F M:
  auto_modulus F M -> continuous_via_ext_tree_ex F (modulus_to_ext_tree a0 F M).
Proof.
Abort.

